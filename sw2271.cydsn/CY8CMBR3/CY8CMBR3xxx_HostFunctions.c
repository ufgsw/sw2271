/*****************************************************************************
* File Name: CY8CMBR3xxx_HostFunctions.c
*
* Version 1.00
*
* Description:
*   This file contains the definitions of the low-level APIs. You may need to 
*   modify the content of these APIs to suit your host processor’s I2C 
*   implementation.
*
* Note:
*   These host-dependent Low Level APIs are provided as an example of
*   low level I2C read and write functions. This set of low level APIs are 
*   written for PSoC 4200/4100 devices and hence should be re-written
*   with equivalent host-dependent APIs from the respective IDEs, if the 
*   host design does not include PSoC 4200/4100 device.
* 
*   To use these APIs, the host should implement a working I2C communication
*   interface. This interface will be used by these APIs to communicate to the
*   CY8CMBR3xxx device.
*
*   For PSoC 4200/4100 devices, please ensure that you have created an instance 
*   of SCB component with I2C Master configuration. The component should be
*   named "SCB".
*
* Owner:
*   SRVS
*
* Related Document:
*   MBR3 Design Guide
*   MBR3 Device Datasheet
*
* Hardware Dependency:
*   PSoC 4200 (Update this as per the host used)
*
* Code Tested With:
*   PSoC Creator 3.0 CP7
*   CY3280-MBR3 Evaluation Kit
*   CY8CKIT-042 Pioneer Kit
*
******************************************************************************
* Copyright (2014), Cypress Semiconductor Corporation.
******************************************************************************
* This software is owned by Cypress Semiconductor Corporation (Cypress) and is
* protected by and subject to worldwide patent protection (United States and
* foreign), United States copyright laws and international treaty provisions.
* Cypress hereby grants to licensee a personal, non-exclusive, non-transferable
* license to copy, use, modify, create derivative works of, and compile the
* Cypress Source Code and derivative works for the sole purpose of creating
* custom software in support of licensee product to be used only in conjunction
* with a Cypress integrated circuit as specified in the applicable agreement.
* Any reproduction, modification, translation, compilation, or representation of
* this software except as specified above is prohibited without the express
* written permission of Cypress.
*
* Disclaimer: CYPRESS MAKES NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, WITH
* REGARD TO THIS MATERIAL, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
* Cypress reserves the right to make changes without further notice to the
* materials described herein. Cypress does not assume any liability arising out
* of the application or use of any product or circuit described herein. Cypress
* does not authorize its products for use as critical components in life-support
* systems where a malfunction or failure may reasonably be expected to result in
* significant injury to the user. The inclusion of Cypress' product in a life-
* support systems application implies that the manufacturer assumes all risk of
* such use and in doing so indemnifies Cypress against all charges. Use may be
* limited by and subject to the applicable Cypress software license agreement.
*****************************************************************************/

/*******************************************************************************
* Included headers
*******************************************************************************/
#include "project.h"
#include "CY8CMBR3xxx_HostFunctions.h"


/*******************************************************************************
* API Constants
*******************************************************************************/
#define CY8CMBR3xxx_READ                    (1)
#define CY8CMBR3xxx_WRITE                   (0)
#define CY8CMBR3xxx_ACK                     (0)
#define CY8CMBR3xxx_NACK                    (1)
#define CY8CMBR3xxx_READ_BYTE_ERROR         (0x80000000)

/* The following macro defines the maximum number of times the low-level read
 * and write functions try to communicate with the CY8CMBR3xxx device, as
 * long as the I2C communication is unsuccessful.
 */
#define CY8CMBR3xxx_RETRY_TIMES             (10)    

/*******************************************************************************
*   Function Code
*******************************************************************************/

/*******************************************************************************
* Function Name: Host_LowLevelWrite
********************************************************************************
*
* Summary:
*  This API writes to the register map of the CY8CMBR3xxx device using the I2C 
*  communication protocol. The implementation is host processor dependent and 
*  you may need to update the API code to suit your host.
*
* Parameters:
*  uint8 slaveAddress:
*   The I2C address of the CY8CMBR3xxx device. Valid range: 8 - 119
*
*  uint8 *writeBuffer:
*   The buffer from which data is written to the device. 
*
*   The first element should always contain the location of the register 
*   of the device to write to. This value can be within 0 – 251.
*
*   Each successive element should contain the data to be written to that 
*   register and the successive registers. These elements can have a value 
*   between 0 – 255. The number of data bytes can be between 0 and 128.
*
*  uint8 numberOfBytes:
*   Number of bytes to be written, equal to the number of elements in the 
*   buffer (i.e. number of data bytes + 1)
*
* Pre:
*  The I2C interface should be enabled and working before this Low Level 
*  API can be called. Also make sure that the Global Interrupts are also
*  enabled (if required)
*
* Post:
*  N/A
*
* Return:
*  status
*    Value                Description
*    TRUE                 Write process was successful
*    FALSE                Write process was not successful
*
*******************************************************************************/
#define MASTER_ERROR_MASK  (CY_SCB_I2C_MASTER_DATA_NAK | CY_SCB_I2C_MASTER_ADDR_NAK    | \
                            CY_SCB_I2C_MASTER_ARB_LOST | CY_SCB_I2C_MASTER_ABORT_START | \
                            CY_SCB_I2C_MASTER_BUS_ERR)    


bool Host_LowLevelWrite(uint8 slaveAddress, uint8 *writeBuffer, uint8 numberOfBytes)
{
   cy_stc_scb_i2c_master_xfer_config_t masterTransferCfg;
   //uint32_t ret;
   uint32_t masterStatus;
   uint32_t masterStatusMask;
   uint32_t timeout = 1000;
   
   cy_en_scb_i2c_status_t  errorStatus;

   masterTransferCfg.slaveAddress = slaveAddress;
   masterTransferCfg.buffer       = writeBuffer;
   masterTransferCfg.bufferSize   = numberOfBytes;
   masterTransferCfg.xferPending  = false;

   /* Initiate write transaction */
   errorStatus = Cy_SCB_I2C_MasterWrite( Sc1_HW, &masterTransferCfg, &Sc1_context);
   if(errorStatus == CY_SCB_I2C_SUCCESS)
   {
      do 
      {
         masterStatus  = Cy_SCB_I2C_MasterGetStatus( Sc1_HW, &Sc1_context );
         if((masterStatus & CY_SCB_I2C_MASTER_BUSY) == 0 ) timeout = 0;
         else
         {
            CyDelay(1);
            if(--timeout == 0 ) 
            {
               Cy_SCB_I2C_Disable(Sc1_HW, &Sc1_context);
               Cy_SCB_I2C_Enable (Sc1_HW);
               return false;
            }
         }
      } while (timeout != 0 );

      /* Check transfer status */
      if (0u != (MASTER_ERROR_MASK & masterStatus))
      {
         /* Check packet structure and status */
         return false;
      }
   }
   else return false;   
   
   return true;
   
//   bool status = FALSE;                                       /* Default return is FALSE if anything goes wrong */
//    uint32 localBufStatus = Sc1_I2C_MSTR_NO_ERROR;             /* Error status variable */
//    uint32 localWrStatus  = Sc1_I2C_MSTR_NO_ERROR;             /* Error status variable */
//    uint8 retryCount      = CY8CMBR3xxx_RETRY_TIMES;           /* Retry count, in case of any issue */
//    
//    do
//    {
//        /* Clear the Write buffer to reset the read pointer */
//        Sc1_I2CMasterClearWriteBuf();
//        
//        /* Clear the Master status since this is a new transaction */
//        Sc1_I2CMasterClearStatus();
//        
//        /* Issue a non-blocking write */
//        localBufStatus = Sc1_I2CMasterWriteBuf(slaveAddress, writeBuffer, numberOfBytes, Sc1_I2C_MODE_COMPLETE_XFER);
//        
//        /* Proceed if there aren't any immediate issues */
//        if (Sc1_I2C_MSTR_NO_ERROR == localBufStatus)
//        {
//            do
//            {
//                /* Read the Master status */
//                localWrStatus = Sc1_I2CMasterStatus();
//            } while (!(Sc1_I2C_MSTAT_WR_CMPLT & localWrStatus)); /* Repeat until Write operation is complete */
//            
//            /* Proceed if the Write operation completed successfully */
//            if (!(Sc1_I2C_MSTAT_ERR_XFER & localWrStatus))
//            {
//                /* Wait till the Write buffer is empty */
//                while (!(numberOfBytes == Sc1_I2CMasterGetWriteBufSize()));
//            }
//        }
//        
//    } while(((Sc1_I2C_MSTAT_ERR_XFER & localWrStatus) || (Sc1_I2C_MSTR_NO_ERROR != localBufStatus)) && (0 != (--retryCount))); /* Repeat from beginning until all bytes are written successfully */
//
//    /* Check whether the transaction was successful or it timed-out */
//    status = (retryCount)? TRUE: FALSE;
//    
//    /* Return the status */
//    return status;
}

/*******************************************************************************
* Function Name: Host_LowLevelRead
********************************************************************************
*
* Summary:
*  This API reads from the register map of the CY8CMBR3xxx device using the 
*  I2C communication protocol. The implementation is host processor dependent 
*  and you may need to update the API code to suit your host.
*
* Parameters:
*  uint8 slaveAddress:
*   The I2C address of the CY8CMBR3xxx device. Valid range: 8 - 119
*
*  uint8 *readBuffer:
*   The buffer to be updated with the data read from the device.
*
*   The register location to read from should be set prior to calling 
*   this API. Each successive element to contain the data read from that 
*   register and the successive registers. These elements can have a value 
*   between 0 – 255.
*
*  uint8 numberOfBytes:
*   Number of data bytes to be read, equal to the number of elements in 
*   the buffer. Valid range: 1 – 252
*
* Pre:
*  The I2C interface should be enabled and working before this Low Level 
*  API can be called. Also make sure that the Global Interrupts are also
*  enabled (if required)
*
* Post:
*  N/A
*
* Return:
*  status
*    Value                Description
*    TRUE                 Read process was successful
*    FALSE                Read process was not successful
*
*******************************************************************************/
bool Host_LowLevelRead(uint8 slaveAddress, uint8 *readBuffer, uint8 numberOfBytes)
{
   cy_stc_scb_i2c_master_xfer_config_t masterTransferCfg;
   uint32_t masterStatus;
   uint32_t timeout = 1000;
   
   cy_en_scb_i2c_status_t  errorStatus;

   masterTransferCfg.slaveAddress = slaveAddress;
   masterTransferCfg.buffer       = readBuffer;
   masterTransferCfg.bufferSize   = numberOfBytes;
   masterTransferCfg.xferPending  = false;
   
   errorStatus = Cy_SCB_I2C_MasterRead( Sc1_HW, &masterTransferCfg, &Sc1_context );
   if(errorStatus == CY_SCB_I2C_SUCCESS)
   {
      do
      {
         masterStatus  = Cy_SCB_I2C_MasterGetStatus( Sc1_HW, &Sc1_context );
         if( (masterStatus & CY_SCB_I2C_MASTER_BUSY) == 0 ) timeout = 0;
         else
         {
            CyDelay(1);
            if( --timeout == 0 )
            {
               Cy_SCB_I2C_Disable( Sc1_HW, &Sc1_context );
               Cy_SCB_I2C_Enable ( Sc1_HW );
               return false;
            }
         }
      } while(timeout != 0);
        
      /* Check transfer status */
      if (0u != (MASTER_ERROR_MASK & masterStatus))
      {
         /* Check packet structure and status */
        return false;
      }   
   }
   else return false;
   return true;
}

/*******************************************************************************
* Function Name: Host_LowLevelDelay
********************************************************************************
*
* Summary:
*  This API implements a time-delay function to be used by the High-level APIs. 
*  The delay period is in milliseconds. This delay is achieved by a 
*  code-execution block for the required amount of time.
*
*  The implementation is host processor dependent and you need to update the 
*  API code to suit your host.
*
* Parameters:
*  uint16 milliseconds:
*   The amount of time in milliseconds for which a wait is required. 
*   Valid range: 0 – 65535
*
* Return:
*  None
*
*******************************************************************************/
void Host_LowLevelDelay(uint16 milliseconds)
{
    // Call the host-specific delay implementation
    // Replace this with the correct host delay routine for introducing delays in milliseconds
    CyDelay((uint32) milliseconds);
}

/****************************End of File***************************************/
