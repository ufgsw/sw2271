/*******************************************************************************
* File Name: MIO_WDOG.c
* Version 1.10
*
* Description:
*  This file provides the source code to the API for the MIO_WDOG
*  component.
*
********************************************************************************
* Copyright 2016, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "MIO_WDOG_PDL.h"


/** MIO_WDOG_initVar indicates whether the MIO_WDOG  component
*  has been initialized. The variable is initialized to 0 and set to 1 the first
*  time MIO_WDOG_Start() is called.
*  This allows the component to restart without reinitialization after the first 
*  call to the MIO_WDOG_Start() routine.
*
*  If re-initialization of the component is required, then the 
*  MIO_WDOG_Init() function can be called before the 
*  MIO_WDOG_Start() or MIO_WDOG_Enable() function.
*/
uint8 MIO_WDOG_initVar = 0u;

/** The instance-specific configuration structure. This should be used in the 
*  associated MIO_WDOG_Init() function.
*/ 
const cy_stc_mcwdt_config_t MIO_WDOG_config =
{
    .c0Match     = MIO_WDOG_C0_MATCH,
    .c1Match     = MIO_WDOG_C1_MATCH,
    .c0Mode      = MIO_WDOG_C0_MODE,
    .c1Mode      = MIO_WDOG_C1_MODE,
    .c2ToggleBit = MIO_WDOG_C2_PERIOD,
    .c2Mode      = MIO_WDOG_C2_MODE,
    .c0ClearOnMatch = (bool)MIO_WDOG_C0_CLEAR_ON_MATCH,
    .c1ClearOnMatch = (bool)MIO_WDOG_C1_CLEAR_ON_MATCH,
    .c0c1Cascade = (bool)MIO_WDOG_CASCADE_C0C1,
    .c1c2Cascade = (bool)MIO_WDOG_CASCADE_C1C2
};


/*******************************************************************************
* Function Name: MIO_WDOG_Start
****************************************************************************//**
*
*  Sets the initVar variable, calls the Init() function, unmasks the 
*  corresponding counter interrupts and then calls the Enable() function 
*  to enable the counters.
*
* \globalvars
*  \ref MIO_WDOG_initVar
*
*  \note
*  When this API is called, the counter starts counting after two lf_clk cycles 
*  pass. It is the user's responsibility to check whether the selected counters
*  were enabled immediately after the function call. This can be done by the 
*  MIO_WDOG_GetEnabledStatus() API.
*
*******************************************************************************/
void MIO_WDOG_Start(void)
{
    if (0u == MIO_WDOG_initVar)
    {
        (void)MIO_WDOG_Init(&MIO_WDOG_config);
        MIO_WDOG_initVar = 1u; /* Component was initialized */
    }

	/* Set interrupt masks for the counters */
	MIO_WDOG_SetInterruptMask(MIO_WDOG_CTRS_INT_MASK);

	/* Enable the counters that are enabled in the customizer */
    MIO_WDOG_Enable(MIO_WDOG_ENABLED_CTRS_MASK, 0u);
}


/*******************************************************************************
* Function Name: MIO_WDOG_Stop
****************************************************************************//**
*
*  Calls the Disable() function to disable all counters.
*
*******************************************************************************/
void MIO_WDOG_Stop(void)
{
    MIO_WDOG_Disable(CY_MCWDT_CTR_Msk, MIO_WDOG_TWO_LF_CLK_CYCLES_DELAY);
    MIO_WDOG_DeInit();
}


/* [] END OF FILE */
