/***************************************************************************//**
* \file Sc1.c
* \version 2.0
*
*  This file provides the source code to the API for the I2C Component.
*
********************************************************************************
* \copyright
* Copyright 2016-2017, Cypress Semiconductor Corporation. All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "Sc1.h"
#include "sysint/cy_sysint.h"
#include "cyfitter_sysint.h"
#include "cyfitter_sysint_cfg.h"


#if defined(__cplusplus)
extern "C" {
#endif

/***************************************
*     Global variables
***************************************/

/** Sc1_initVar indicates whether the Sc1
*  component has been initialized. The variable is initialized to 0
*  and set to 1 the first time Sc1_Start() is called.
*  This allows  the component to restart without reinitialization
*  after the first call to the Sc1_Start() routine.
*
*  If re-initialization of the component is required, then the
*  Sc1_Init() function can be called before the
*  Sc1_Start() or Sc1_Enable() function.
*/
uint8_t Sc1_initVar = 0U;

/** The instance-specific configuration structure.
* The pointer to this structure should be passed to Cy_SCB_I2C_Init function
* to initialize component with GUI selected settings.
*/
cy_stc_scb_i2c_config_t const Sc1_config =
{
    .i2cMode    = CY_SCB_I2C_MASTER,

    .useRxFifo = true,
    .useTxFifo = true,

    .slaveAddress        = 0U,
    .slaveAddressMask    = 0U,
    .acceptAddrInFifo    = false,
    .ackGeneralAddr      = false,

    .enableWakeFromSleep = false
};

/** The instance-specific context structure.
* It is used while the driver operation for internal configuration and
* data keeping for the I2C. The user should not modify anything in this
* structure.
*/
cy_stc_scb_i2c_context_t Sc1_context;


/*******************************************************************************
* Function Name: Sc1_Start
****************************************************************************//**
*
* Invokes Sc1_Init() and Sc1_Enable().
* Also configures interrupt and low and high oversampling phases.
* After this function call the component is enabled and ready for operation.
* This is the preferred method to begin component operation.
*
* \globalvars
* \ref Sc1_initVar - used to check initial configuration,
* modified  on first function call.
*
*******************************************************************************/
void Sc1_Start(void)
{
    if (0U == Sc1_initVar)
    {
        /* Configure component */
        (void) Cy_SCB_I2C_Init(Sc1_HW, &Sc1_config, &Sc1_context);

    #if (Sc1_ENABLE_MASTER)
        /* Configure desired data rate */
        (void) Cy_SCB_I2C_SetDataRate(Sc1_HW, Sc1_DATA_RATE_HZ, Sc1_CLK_FREQ_HZ);

        #if (Sc1_MANUAL_SCL_CONTROL)
            Cy_SCB_I2C_MasterSetLowPhaseDutyCycle (Sc1_HW, Sc1_LOW_PHASE_DUTY_CYCLE);
            Cy_SCB_I2C_MasterSetHighPhaseDutyCycle(Sc1_HW, Sc1_HIGH_PHASE_DUTY_CYCLE);
        #endif /* (Sc1_MANUAL_SCL_CONTROL) */
    #endif /* (Sc1_ENABLE_MASTER) */

        /* Hook interrupt service routine */
    #if defined(Sc1_SCB_IRQ__INTC_ASSIGNED)
        (void) Cy_SysInt_Init(&Sc1_SCB_IRQ_cfg, &Sc1_Interrupt);
    #endif /* (Sc1_SCB_IRQ__INTC_ASSIGNED) */

        Sc1_initVar = 1U;
    }

    /* Enable interrupt in NVIC */
#if defined(Sc1_SCB_IRQ__INTC_ASSIGNED)
    NVIC_EnableIRQ((IRQn_Type) Sc1_SCB_IRQ_cfg.intrSrc);
#endif /* (Sc1_SCB_IRQ__INTC_ASSIGNED) */

    Cy_SCB_I2C_Enable(Sc1_HW);
}

#if defined(__cplusplus)
}
#endif


/* [] END OF FILE */
