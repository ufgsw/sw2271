/***************************************************************************//**
* \file com_debug.h
* \version 2.0
*
*  This file provides constants and parameter values for the UART component.
*
********************************************************************************
* \copyright
* Copyright 2016-2017, Cypress Semiconductor Corporation. All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(com_debug_CY_SCB_UART_PDL_H)
#define com_debug_CY_SCB_UART_PDL_H

#include "cyfitter.h"
#include "scb/cy_scb_uart.h"

#if defined(__cplusplus)
extern "C" {
#endif

/***************************************
*   Initial Parameter Constants
****************************************/

#define com_debug_DIRECTION  (3U)
#define com_debug_ENABLE_RTS (0U)
#define com_debug_ENABLE_CTS (0U)

/* UART direction enum */
#define com_debug_RX    (0x1U)
#define com_debug_TX    (0x2U)

#define com_debug_ENABLE_RX  (0UL != (com_debug_DIRECTION & com_debug_RX))
#define com_debug_ENABLE_TX  (0UL != (com_debug_DIRECTION & com_debug_TX))


/***************************************
*        Function Prototypes
***************************************/
/**
* \addtogroup group_general
* @{
*/
/* Component specific functions. */
void com_debug_Start(void);

/* Basic functions */
__STATIC_INLINE cy_en_scb_uart_status_t com_debug_Init(cy_stc_scb_uart_config_t const *config);
__STATIC_INLINE void com_debug_DeInit(void);
__STATIC_INLINE void com_debug_Enable(void);
__STATIC_INLINE void com_debug_Disable(void);

/* Register callback. */
__STATIC_INLINE void com_debug_RegisterCallback(cy_cb_scb_uart_handle_events_t callback);

/* Configuration change. */
#if (com_debug_ENABLE_CTS)
__STATIC_INLINE void com_debug_EnableCts(void);
__STATIC_INLINE void com_debug_DisableCts(void);
#endif /* (com_debug_ENABLE_CTS) */

#if (com_debug_ENABLE_RTS)
__STATIC_INLINE void     com_debug_SetRtsFifoLevel(uint32_t level);
__STATIC_INLINE uint32_t com_debug_GetRtsFifoLevel(void);
#endif /* (com_debug_ENABLE_RTS) */

__STATIC_INLINE void com_debug_EnableSkipStart(void);
__STATIC_INLINE void com_debug_DisableSkipStart(void);

#if (com_debug_ENABLE_RX)
/* Low level: Receive direction. */
__STATIC_INLINE uint32_t com_debug_Get(void);
__STATIC_INLINE uint32_t com_debug_GetArray(void *buffer, uint32_t size);
__STATIC_INLINE void     com_debug_GetArrayBlocking(void *buffer, uint32_t size);
__STATIC_INLINE uint32_t com_debug_GetRxFifoStatus(void);
__STATIC_INLINE void     com_debug_ClearRxFifoStatus(uint32_t clearMask);
__STATIC_INLINE uint32_t com_debug_GetNumInRxFifo(void);
__STATIC_INLINE void     com_debug_ClearRxFifo(void);
#endif /* (com_debug_ENABLE_RX) */

#if (com_debug_ENABLE_TX)
/* Low level: Transmit direction. */
__STATIC_INLINE uint32_t com_debug_Put(uint32_t data);
__STATIC_INLINE uint32_t com_debug_PutArray(void *buffer, uint32_t size);
__STATIC_INLINE void     com_debug_PutArrayBlocking(void *buffer, uint32_t size);
__STATIC_INLINE void     com_debug_PutString(char_t const string[]);
__STATIC_INLINE void     com_debug_SendBreakBlocking(uint32_t breakWidth);
__STATIC_INLINE uint32_t com_debug_GetTxFifoStatus(void);
__STATIC_INLINE void     com_debug_ClearTxFifoStatus(uint32_t clearMask);
__STATIC_INLINE uint32_t com_debug_GetNumInTxFifo(void);
__STATIC_INLINE bool     com_debug_IsTxComplete(void);
__STATIC_INLINE void     com_debug_ClearTxFifo(void);
#endif /* (com_debug_ENABLE_TX) */

#if (com_debug_ENABLE_RX)
/* High level: Ring buffer functions. */
__STATIC_INLINE void     com_debug_StartRingBuffer(void *buffer, uint32_t size);
__STATIC_INLINE void     com_debug_StopRingBuffer(void);
__STATIC_INLINE void     com_debug_ClearRingBuffer(void);
__STATIC_INLINE uint32_t com_debug_GetNumInRingBuffer(void);

/* High level: Receive direction functions. */
__STATIC_INLINE cy_en_scb_uart_status_t com_debug_Receive(void *buffer, uint32_t size);
__STATIC_INLINE void     com_debug_AbortReceive(void);
__STATIC_INLINE uint32_t com_debug_GetReceiveStatus(void);
__STATIC_INLINE uint32_t com_debug_GetNumReceived(void);
#endif /* (com_debug_ENABLE_RX) */

#if (com_debug_ENABLE_TX)
/* High level: Transmit direction functions. */
__STATIC_INLINE cy_en_scb_uart_status_t com_debug_Transmit(void *buffer, uint32_t size);
__STATIC_INLINE void     com_debug_AbortTransmit(void);
__STATIC_INLINE uint32_t com_debug_GetTransmitStatus(void);
__STATIC_INLINE uint32_t com_debug_GetNumLeftToTransmit(void);
#endif /* (com_debug_ENABLE_TX) */

/* Interrupt handler */
__STATIC_INLINE void com_debug_Interrupt(void);
/** @} group_general */


/***************************************
*    Variables with External Linkage
***************************************/
/**
* \addtogroup group_globals
* @{
*/
extern uint8_t com_debug_initVar;
extern cy_stc_scb_uart_config_t const com_debug_config;
extern cy_stc_scb_uart_context_t com_debug_context;
/** @} group_globals */


/***************************************
*         Preprocessor Macros
***************************************/
/**
* \addtogroup group_macros
* @{
*/
/** The pointer to the base address of the hardware */
#define com_debug_HW     ((CySCB_Type *) com_debug_SCB__HW)
/** @} group_macros */


/***************************************
*    In-line Function Implementation
***************************************/

/*******************************************************************************
* Function Name: com_debug_Init
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_Init() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE cy_en_scb_uart_status_t com_debug_Init(cy_stc_scb_uart_config_t const *config)
{
   return Cy_SCB_UART_Init(com_debug_HW, config, &com_debug_context);
}


/*******************************************************************************
* Function Name: com_debug_DeInit
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_DeInit() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void com_debug_DeInit(void)
{
    Cy_SCB_UART_DeInit(com_debug_HW);
}


/*******************************************************************************
* Function Name: com_debug_Enable
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_Enable() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void com_debug_Enable(void)
{
    Cy_SCB_UART_Enable(com_debug_HW);
}


/*******************************************************************************
* Function Name: com_debug_Disable
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_Disable() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void com_debug_Disable(void)
{
    Cy_SCB_UART_Disable(com_debug_HW, &com_debug_context);
}


/*******************************************************************************
* Function Name: com_debug_RegisterCallback
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_RegisterCallback() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void com_debug_RegisterCallback(cy_cb_scb_uart_handle_events_t callback)
{
    Cy_SCB_UART_RegisterCallback(com_debug_HW, callback, &com_debug_context);
}


#if (com_debug_ENABLE_CTS)
/*******************************************************************************
* Function Name: com_debug_EnableCts
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_EnableCts() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void com_debug_EnableCts(void)
{
    Cy_SCB_UART_EnableCts(com_debug_HW);
}


/*******************************************************************************
* Function Name: Cy_SCB_UART_DisableCts
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_DisableCts() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void com_debug_DisableCts(void)
{
    Cy_SCB_UART_DisableCts(com_debug_HW);
}
#endif /* (com_debug_ENABLE_CTS) */


#if (com_debug_ENABLE_RTS)
/*******************************************************************************
* Function Name: com_debug_SetRtsFifoLevel
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_SetRtsFifoLevel() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void com_debug_SetRtsFifoLevel(uint32_t level)
{
    Cy_SCB_UART_SetRtsFifoLevel(com_debug_HW, level);
}


/*******************************************************************************
* Function Name: com_debug_GetRtsFifoLevel
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetRtsFifoLevel() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t com_debug_GetRtsFifoLevel(void)
{
    return Cy_SCB_UART_GetRtsFifoLevel(com_debug_HW);
}
#endif /* (com_debug_ENABLE_RTS) */


/*******************************************************************************
* Function Name: com_debug_EnableSkipStart
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_EnableSkipStart() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void com_debug_EnableSkipStart(void)
{
    Cy_SCB_UART_EnableSkipStart(com_debug_HW);
}


/*******************************************************************************
* Function Name: com_debug_DisableSkipStart
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_DisableSkipStart() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void com_debug_DisableSkipStart(void)
{
    Cy_SCB_UART_DisableSkipStart(com_debug_HW);
}


#if (com_debug_ENABLE_RX)
/*******************************************************************************
* Function Name: com_debug_Get
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_Get() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t com_debug_Get(void)
{
    return Cy_SCB_UART_Get(com_debug_HW);
}


/*******************************************************************************
* Function Name: com_debug_GetArray
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetArray() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t com_debug_GetArray(void *buffer, uint32_t size)
{
    return Cy_SCB_UART_GetArray(com_debug_HW, buffer, size);
}


/*******************************************************************************
* Function Name: com_debug_GetArrayBlocking
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetArrayBlocking() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void com_debug_GetArrayBlocking(void *buffer, uint32_t size)
{
    Cy_SCB_UART_GetArrayBlocking(com_debug_HW, buffer, size);
}


/*******************************************************************************
* Function Name: com_debug_GetRxFifoStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetRxFifoStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t com_debug_GetRxFifoStatus(void)
{
    return Cy_SCB_UART_GetRxFifoStatus(com_debug_HW);
}


/*******************************************************************************
* Function Name: com_debug_ClearRxFifoStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_ClearRxFifoStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void com_debug_ClearRxFifoStatus(uint32_t clearMask)
{
    Cy_SCB_UART_ClearRxFifoStatus(com_debug_HW, clearMask);
}


/*******************************************************************************
* Function Name: com_debug_GetNumInRxFifo
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetNumInRxFifo() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t com_debug_GetNumInRxFifo(void)
{
    return Cy_SCB_UART_GetNumInRxFifo(com_debug_HW);
}


/*******************************************************************************
* Function Name: com_debug_ClearRxFifo
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_ClearRxFifo() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void com_debug_ClearRxFifo(void)
{
    Cy_SCB_UART_ClearRxFifo(com_debug_HW);
}
#endif /* (com_debug_ENABLE_RX) */


#if (com_debug_ENABLE_TX)
/*******************************************************************************
* Function Name: com_debug_Put
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_Put() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t com_debug_Put(uint32_t data)
{
    return Cy_SCB_UART_Put(com_debug_HW,data);
}


/*******************************************************************************
* Function Name: com_debug_PutArray
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_PutArray() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t com_debug_PutArray(void *buffer, uint32_t size)
{
    return Cy_SCB_UART_PutArray(com_debug_HW, buffer, size);
}


/*******************************************************************************
* Function Name: com_debug_PutArrayBlocking
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_PutArrayBlocking() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void com_debug_PutArrayBlocking(void *buffer, uint32_t size)
{
    Cy_SCB_UART_PutArrayBlocking(com_debug_HW, buffer, size);
}


/*******************************************************************************
* Function Name: com_debug_PutString
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_PutString() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void com_debug_PutString(char_t const string[])
{
    Cy_SCB_UART_PutString(com_debug_HW, string);
}


/*******************************************************************************
* Function Name: com_debug_SendBreakBlocking
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_SendBreakBlocking() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void com_debug_SendBreakBlocking(uint32_t breakWidth)
{
    Cy_SCB_UART_SendBreakBlocking(com_debug_HW, breakWidth);
}


/*******************************************************************************
* Function Name: com_debug_GetTxFifoStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetTxFifoStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t com_debug_GetTxFifoStatus(void)
{
    return Cy_SCB_UART_GetTxFifoStatus(com_debug_HW);
}


/*******************************************************************************
* Function Name: com_debug_ClearTxFifoStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_ClearTxFifoStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void com_debug_ClearTxFifoStatus(uint32_t clearMask)
{
    Cy_SCB_UART_ClearTxFifoStatus(com_debug_HW, clearMask);
}


/*******************************************************************************
* Function Name: com_debug_GetNumInTxFifo
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetNumInTxFifo() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t com_debug_GetNumInTxFifo(void)
{
    return Cy_SCB_UART_GetNumInTxFifo(com_debug_HW);
}


/*******************************************************************************
* Function Name: com_debug_IsTxComplete
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_IsTxComplete() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE bool com_debug_IsTxComplete(void)
{
    return Cy_SCB_UART_IsTxComplete(com_debug_HW);
}


/*******************************************************************************
* Function Name: com_debug_ClearTxFifo
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_ClearTxFifo() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void com_debug_ClearTxFifo(void)
{
    Cy_SCB_UART_ClearTxFifo(com_debug_HW);
}
#endif /* (com_debug_ENABLE_TX) */


#if (com_debug_ENABLE_RX)
/*******************************************************************************
* Function Name: com_debug_StartRingBuffer
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_StartRingBuffer() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void com_debug_StartRingBuffer(void *buffer, uint32_t size)
{
    Cy_SCB_UART_StartRingBuffer(com_debug_HW, buffer, size, &com_debug_context);
}


/*******************************************************************************
* Function Name: com_debug_StopRingBuffer
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_StopRingBuffer() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void com_debug_StopRingBuffer(void)
{
    Cy_SCB_UART_StopRingBuffer(com_debug_HW, &com_debug_context);
}


/*******************************************************************************
* Function Name: com_debug_ClearRingBuffer
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_ClearRingBuffer() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void com_debug_ClearRingBuffer(void)
{
    Cy_SCB_UART_ClearRingBuffer(com_debug_HW, &com_debug_context);
}


/*******************************************************************************
* Function Name: com_debug_GetNumInRingBuffer
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetNumInRingBuffer() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t com_debug_GetNumInRingBuffer(void)
{
    return Cy_SCB_UART_GetNumInRingBuffer(com_debug_HW, &com_debug_context);
}


/*******************************************************************************
* Function Name: com_debug_Receive
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_Receive() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE cy_en_scb_uart_status_t com_debug_Receive(void *buffer, uint32_t size)
{
    return Cy_SCB_UART_Receive(com_debug_HW, buffer, size, &com_debug_context);
}


/*******************************************************************************
* Function Name: com_debug_GetReceiveStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetReceiveStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t com_debug_GetReceiveStatus(void)
{
    return Cy_SCB_UART_GetReceiveStatus(com_debug_HW, &com_debug_context);
}


/*******************************************************************************
* Function Name: com_debug_AbortReceive
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_AbortReceive() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void com_debug_AbortReceive(void)
{
    Cy_SCB_UART_AbortReceive(com_debug_HW, &com_debug_context);
}


/*******************************************************************************
* Function Name: com_debug_GetNumReceived
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetNumReceived() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t com_debug_GetNumReceived(void)
{
    return Cy_SCB_UART_GetNumReceived(com_debug_HW, &com_debug_context);
}
#endif /* (com_debug_ENABLE_RX) */


#if (com_debug_ENABLE_TX)
/*******************************************************************************
* Function Name: com_debug_Transmit
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_Transmit() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE cy_en_scb_uart_status_t com_debug_Transmit(void *buffer, uint32_t size)
{
    return Cy_SCB_UART_Transmit(com_debug_HW, buffer, size, &com_debug_context);
}


/*******************************************************************************
* Function Name: com_debug_GetTransmitStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetTransmitStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t com_debug_GetTransmitStatus(void)
{
    return Cy_SCB_UART_GetTransmitStatus(com_debug_HW, &com_debug_context);
}


/*******************************************************************************
* Function Name: com_debug_AbortTransmit
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_AbortTransmit() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void com_debug_AbortTransmit(void)
{
    Cy_SCB_UART_AbortTransmit(com_debug_HW, &com_debug_context);
}


/*******************************************************************************
* Function Name: com_debug_GetNumLeftToTransmit
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetNumLeftToTransmit() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t com_debug_GetNumLeftToTransmit(void)
{
    return Cy_SCB_UART_GetNumLeftToTransmit(com_debug_HW, &com_debug_context);
}
#endif /* (com_debug_ENABLE_TX) */


/*******************************************************************************
* Function Name: com_debug_Interrupt
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_Interrupt() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void com_debug_Interrupt(void)
{
    Cy_SCB_UART_Interrupt(com_debug_HW, &com_debug_context);
}

#if defined(__cplusplus)
}
#endif

#endif /* com_debug_CY_SCB_UART_PDL_H */


/* [] END OF FILE */
