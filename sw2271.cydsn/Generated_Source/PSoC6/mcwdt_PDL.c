/*******************************************************************************
* File Name: mcwdt.c
* Version 1.10
*
* Description:
*  This file provides the source code to the API for the mcwdt
*  component.
*
********************************************************************************
* Copyright 2016, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "mcwdt_PDL.h"


/** mcwdt_initVar indicates whether the mcwdt  component
*  has been initialized. The variable is initialized to 0 and set to 1 the first
*  time mcwdt_Start() is called.
*  This allows the component to restart without reinitialization after the first 
*  call to the mcwdt_Start() routine.
*
*  If re-initialization of the component is required, then the 
*  mcwdt_Init() function can be called before the 
*  mcwdt_Start() or mcwdt_Enable() function.
*/
uint8 mcwdt_initVar = 0u;

/** The instance-specific configuration structure. This should be used in the 
*  associated mcwdt_Init() function.
*/ 
const cy_stc_mcwdt_config_t mcwdt_config =
{
    .c0Match     = mcwdt_C0_MATCH,
    .c1Match     = mcwdt_C1_MATCH,
    .c0Mode      = mcwdt_C0_MODE,
    .c1Mode      = mcwdt_C1_MODE,
    .c2ToggleBit = mcwdt_C2_PERIOD,
    .c2Mode      = mcwdt_C2_MODE,
    .c0ClearOnMatch = (bool)mcwdt_C0_CLEAR_ON_MATCH,
    .c1ClearOnMatch = (bool)mcwdt_C1_CLEAR_ON_MATCH,
    .c0c1Cascade = (bool)mcwdt_CASCADE_C0C1,
    .c1c2Cascade = (bool)mcwdt_CASCADE_C1C2
};


/*******************************************************************************
* Function Name: mcwdt_Start
****************************************************************************//**
*
*  Sets the initVar variable, calls the Init() function, unmasks the 
*  corresponding counter interrupts and then calls the Enable() function 
*  to enable the counters.
*
* \globalvars
*  \ref mcwdt_initVar
*
*  \note
*  When this API is called, the counter starts counting after two lf_clk cycles 
*  pass. It is the user's responsibility to check whether the selected counters
*  were enabled immediately after the function call. This can be done by the 
*  mcwdt_GetEnabledStatus() API.
*
*******************************************************************************/
void mcwdt_Start(void)
{
    if (0u == mcwdt_initVar)
    {
        (void)mcwdt_Init(&mcwdt_config);
        mcwdt_initVar = 1u; /* Component was initialized */
    }

	/* Set interrupt masks for the counters */
	mcwdt_SetInterruptMask(mcwdt_CTRS_INT_MASK);

	/* Enable the counters that are enabled in the customizer */
    mcwdt_Enable(mcwdt_ENABLED_CTRS_MASK, 0u);
}


/*******************************************************************************
* Function Name: mcwdt_Stop
****************************************************************************//**
*
*  Calls the Disable() function to disable all counters.
*
*******************************************************************************/
void mcwdt_Stop(void)
{
    mcwdt_Disable(CY_MCWDT_CTR_Msk, mcwdt_TWO_LF_CLK_CYCLES_DELAY);
    mcwdt_DeInit();
}


/* [] END OF FILE */
