/*******************************************************************************
* File Name: mcwdt.h
* Version 1.10
*
* Description:
*  This file provides constants and parameter values for the mcwdt
*  component.
*
********************************************************************************
* Copyright 2016, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(mcwdt_CY_MCWDT_PDL_H)
#define mcwdt_CY_MCWDT_PDL_H

#if defined(__cplusplus)
extern "C" {
#endif

#include "cyfitter.h"
#include "mcwdt/cy_mcwdt.h"

/*******************************************************************************
*   Variables
*******************************************************************************/
/**
* \addtogroup group_globals
* @{
*/
extern uint8_t  mcwdt_initVar;
extern const cy_stc_mcwdt_config_t mcwdt_config;
/** @} group_globals */

/***************************************
*   Conditional Compilation Parameters
****************************************/
#define mcwdt_C0_CLEAR_ON_MATCH  (1U)
#define mcwdt_C1_CLEAR_ON_MATCH  (0U)
#define mcwdt_CASCADE_C0C1       (0U)
#define mcwdt_CASCADE_C1C2       (0U)
#define mcwdt_C0_MATCH           (10000U)
#define mcwdt_C0_MODE            (1U)
#define mcwdt_C1_MATCH           (32768U)
#define mcwdt_C1_MODE            (0U)
#define mcwdt_C2_PERIOD          (16U)
#define mcwdt_C2_MODE            (0U)

#if (0u == 1U)
    #define mcwdt_CTR0_EN_MASK   0UL
#else
    #define mcwdt_CTR0_EN_MASK   CY_MCWDT_CTR0
#endif
#if (0u == 0U)
    #define mcwdt_CTR1_EN_MASK   0UL
#else
    #define mcwdt_CTR1_EN_MASK   CY_MCWDT_CTR1
#endif
#if (0u == 0U)
    #define mcwdt_CTR2_EN_MASK   0UL
#else
    #define mcwdt_CTR2_EN_MASK   CY_MCWDT_CTR2
#endif

#define mcwdt_ENABLED_CTRS_MASK  (mcwdt_CTR0_EN_MASK |\
                                             mcwdt_CTR1_EN_MASK |\
                                             mcwdt_CTR2_EN_MASK)
											 
#if (1U == mcwdt_C0_MODE) || (3U == mcwdt_C0_MODE)
    #define mcwdt_CTR0_INT_MASK   CY_MCWDT_CTR0
#else
    #define mcwdt_CTR0_INT_MASK   0UL
#endif
#if (1U == mcwdt_C1_MODE) || (3U == mcwdt_C1_MODE)
    #define mcwdt_CTR1_INT_MASK   CY_MCWDT_CTR1
#else
    #define mcwdt_CTR1_INT_MASK   0UL
#endif
#if (1U == mcwdt_C2_MODE)
    #define mcwdt_CTR2_INT_MASK   CY_MCWDT_CTR2
#else
    #define mcwdt_CTR2_INT_MASK   0UL
#endif 

#define mcwdt_CTRS_INT_MASK      (mcwdt_CTR0_INT_MASK |\
                                             mcwdt_CTR1_INT_MASK |\
                                             mcwdt_CTR2_INT_MASK)										 

/***************************************
*        Registers Constants
***************************************/

/* This is a ptr to the base address of the MCWDT instance. */
#define mcwdt_HW                 (mcwdt_MCWDT__HW)

#if (0u == mcwdt_MCWDT__IDX)
    #define mcwdt_RESET_REASON   CY_SYSLIB_RESET_SWWDT0
#else
    #define mcwdt_RESET_REASON   CY_SYSLIB_RESET_SWWDT1
#endif 

#define mcwdt_TWO_LF_CLK_CYCLES_DELAY (62u)


/*******************************************************************************
*        Function Prototypes
*******************************************************************************/
/**
* \addtogroup group_general
* @{
*/
                void     mcwdt_Start(void);
                void     mcwdt_Stop(void);
__STATIC_INLINE cy_en_mcwdt_status_t mcwdt_Init(const cy_stc_mcwdt_config_t *config);
__STATIC_INLINE void     mcwdt_DeInit(void);
__STATIC_INLINE void     mcwdt_Enable(uint32_t counters, uint16_t waitUs);
__STATIC_INLINE void     mcwdt_Disable(uint32_t counters, uint16_t waitUs);
__STATIC_INLINE uint32_t mcwdt_GetEnabledStatus(cy_en_mcwdtctr_t counter);
__STATIC_INLINE void     mcwdt_Lock(void);
__STATIC_INLINE void     mcwdt_Unlock(void);
__STATIC_INLINE uint32_t mcwdt_GetLockedStatus(void);
__STATIC_INLINE void     mcwdt_SetMode(cy_en_mcwdtctr_t counter, cy_en_mcwdtmode_t mode);
__STATIC_INLINE cy_en_mcwdtmode_t mcwdt_GetMode(cy_en_mcwdtctr_t counter);
__STATIC_INLINE void     mcwdt_SetClearOnMatch(cy_en_mcwdtctr_t counter, uint32_t enable);
__STATIC_INLINE uint32_t mcwdt_GetClearOnMatch(cy_en_mcwdtctr_t counter);
__STATIC_INLINE void     mcwdt_SetCascade(cy_en_mcwdtcascade_t cascade);
__STATIC_INLINE cy_en_mcwdtcascade_t mcwdt_GetCascade(void);
__STATIC_INLINE void     mcwdt_SetMatch(cy_en_mcwdtctr_t counter, uint32_t match, uint16_t waitUs);
__STATIC_INLINE uint32_t mcwdt_GetMatch(cy_en_mcwdtctr_t counter);
__STATIC_INLINE void     mcwdt_SetToggleBit(uint32_t bit);
__STATIC_INLINE uint32_t mcwdt_GetToggleBit(void);
__STATIC_INLINE uint32_t mcwdt_GetCount(cy_en_mcwdtctr_t counter);
__STATIC_INLINE void     mcwdt_ResetCounters(uint32_t counters, uint16_t waitUs);
__STATIC_INLINE uint32_t mcwdt_GetInterruptStatus(void);
__STATIC_INLINE void     mcwdt_ClearInterrupt(uint32_t counters);
__STATIC_INLINE void     mcwdt_SetInterrupt(uint32_t counters);
__STATIC_INLINE uint32_t mcwdt_GetInterruptMask(void);
__STATIC_INLINE void     mcwdt_SetInterruptMask(uint32_t counters);
__STATIC_INLINE uint32_t mcwdt_GetInterruptStatusMasked(void);
__STATIC_INLINE uint32_t mcwdt_GetCountCascaded(void);
/** @} general */


/*******************************************************************************
* Function Name: mcwdt_Init
****************************************************************************//**
*
* Invokes the Cy_MCWDT_Init() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE cy_en_mcwdt_status_t mcwdt_Init(const cy_stc_mcwdt_config_t *config)
{
    return (Cy_MCWDT_Init(mcwdt_HW, config));
}


/*******************************************************************************
* Function Name: mcwdt_DeInit
****************************************************************************//**
*
* Invokes the Cy_MCWDT_DeInit() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void mcwdt_DeInit(void)
{
    Cy_MCWDT_DeInit(mcwdt_HW);
}


/*******************************************************************************
* Function Name: mcwdt_Enable
****************************************************************************//**
*
* Invokes the Cy_MCWDT_Enable() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void mcwdt_Enable(uint32_t counters, uint16_t waitUs)
{
    Cy_MCWDT_Enable(mcwdt_HW, counters, waitUs);
}


/*******************************************************************************
* Function Name: mcwdt_Disable
****************************************************************************//**
*
* Invokes the Cy_MCWDT_Disable() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void mcwdt_Disable(uint32_t counters, uint16_t waitUs)
{
    Cy_MCWDT_Disable(mcwdt_HW, counters, waitUs);
}


/*******************************************************************************
* Function Name: mcwdt_GetEnabledStatus
****************************************************************************//**
*
* Invokes the Cy_MCWDT_GetEnabledStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t mcwdt_GetEnabledStatus(cy_en_mcwdtctr_t counter)
{
    return (Cy_MCWDT_GetEnabledStatus(mcwdt_HW, counter));
}


/*******************************************************************************
* Function Name: mcwdt_Lock
****************************************************************************//**
*
* Invokes the Cy_MCWDT_Lock() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void mcwdt_Lock(void)
{
    Cy_MCWDT_Lock(mcwdt_HW);
}


/*******************************************************************************
* Function Name: mcwdt_Unlock
****************************************************************************//**
*
* Invokes the Cy_MCWDT_Unlock() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void mcwdt_Unlock(void)
{
   Cy_MCWDT_Unlock(mcwdt_HW);
}


/*******************************************************************************
* Function Name: mcwdt_GetLockStatus
****************************************************************************//**
*
* Invokes the Cy_MCWDT_GetLockedStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t mcwdt_GetLockedStatus(void)
{
    return (Cy_MCWDT_GetLockedStatus(mcwdt_HW));
}


/*******************************************************************************
* Function Name: mcwdt_SetMode
****************************************************************************//**
*
* Invokes the Cy_MCWDT_SetMode() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void mcwdt_SetMode(cy_en_mcwdtctr_t counter, cy_en_mcwdtmode_t mode)
{
    Cy_MCWDT_SetMode(mcwdt_HW, counter, mode);
}


/*******************************************************************************
* Function Name: mcwdt_GetMode
****************************************************************************//**
*
* Invokes the Cy_MCWDT_GetMode() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE cy_en_mcwdtmode_t mcwdt_GetMode(cy_en_mcwdtctr_t counter)
{
    return (Cy_MCWDT_GetMode(mcwdt_HW, counter));
}


/*******************************************************************************
* Function Name: mcwdt_SetClearOnMatch
****************************************************************************//**
*
* Invokes the Cy_MCWDT_SetClearOnMatch() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void mcwdt_SetClearOnMatch(cy_en_mcwdtctr_t counter, uint32_t enable)
{
    Cy_MCWDT_SetClearOnMatch(mcwdt_HW, counter, enable);
}


/*******************************************************************************
* Function Name: mcwdt_GetClearOnMatch
****************************************************************************//**
*
* Invokes the Cy_MCWDT_GetClearOnMatch() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t mcwdt_GetClearOnMatch(cy_en_mcwdtctr_t counter)
{
    return (Cy_MCWDT_GetClearOnMatch(mcwdt_HW, counter));
}


/*******************************************************************************
* Function Name: mcwdt_SetCascade
****************************************************************************//**
*
* Invokes the Cy_MCWDT_SetCascade() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void mcwdt_SetCascade(cy_en_mcwdtcascade_t cascade)
{
    Cy_MCWDT_SetCascade(mcwdt_HW, cascade);
}


/*******************************************************************************
* Function Name: mcwdt_GetCascade
****************************************************************************//**
*
* Invokes the Cy_MCWDT_GetCascade() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE cy_en_mcwdtcascade_t mcwdt_GetCascade(void)
{
    return (Cy_MCWDT_GetCascade(mcwdt_HW));
}


/*******************************************************************************
* Function Name: mcwdt_SetMatch
****************************************************************************//**
*
* Invokes the Cy_MCWDT_SetMatch() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void mcwdt_SetMatch(cy_en_mcwdtctr_t counter, uint32_t match, uint16_t waitUs)
{
    Cy_MCWDT_SetMatch(mcwdt_HW, counter, match, waitUs);
}


/*******************************************************************************
* Function Name: mcwdt_GetMatch
****************************************************************************//**
*
* Invokes the Cy_MCWDT_GetMatch() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t mcwdt_GetMatch(cy_en_mcwdtctr_t counter)
{
    return (Cy_MCWDT_GetMatch(mcwdt_HW, counter));
}


/*******************************************************************************
* Function Name: mcwdt_SetToggleBit
****************************************************************************//**
*
* Invokes the Cy_MCWDT_SetToggleBit() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void mcwdt_SetToggleBit(uint32_t bit)
{
    Cy_MCWDT_SetToggleBit(mcwdt_HW, bit);
}

/*******************************************************************************
* Function Name: mcwdt_GetToggleBit
****************************************************************************//**
*
* Invokes the Cy_MCWDT_GetToggleBit() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t mcwdt_GetToggleBit(void)
{
    return (Cy_MCWDT_GetToggleBit(mcwdt_HW));
}


/*******************************************************************************
* Function Name: mcwdt_GetCount
****************************************************************************//**
*
* Invokes the Cy_MCWDT_GetCount() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t mcwdt_GetCount(cy_en_mcwdtctr_t counter)
{
    return (Cy_MCWDT_GetCount(mcwdt_HW, counter));
}


/*******************************************************************************
* Function Name: mcwdt_ResetCounters
****************************************************************************//**
*
* Invokes the Cy_MCWDT_ResetCounters() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void mcwdt_ResetCounters(uint32_t counters, uint16_t waitUs)
{
    Cy_MCWDT_ResetCounters(mcwdt_HW, counters, waitUs);
}


/*******************************************************************************
* Function Name: mcwdt_GetInterruptStatus
****************************************************************************//**
*
* Invokes the Cy_MCWDT_GetInterruptStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t mcwdt_GetInterruptStatus(void)
{
    return (Cy_MCWDT_GetInterruptStatus(mcwdt_HW));
}


/*******************************************************************************
* Function Name: mcwdt_ClearInterrupt
****************************************************************************//**
*
* Invokes the Cy_MCWDT_ClearInterrupt() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void mcwdt_ClearInterrupt(uint32_t counters)
{
    Cy_MCWDT_ClearInterrupt(mcwdt_HW, counters);
}


/*******************************************************************************
* Function Name: mcwdt_SetInterrupt
****************************************************************************//**
*
* Invokes the Cy_MCWDT_SetInterrupt() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void mcwdt_SetInterrupt(uint32_t counters)
{
    Cy_MCWDT_SetInterrupt(mcwdt_HW, counters);
}


/*******************************************************************************
* Function Name: mcwdt_GetInterruptMask
****************************************************************************//**
*
* Invokes the Cy_MCWDT_GetInterruptMask() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t mcwdt_GetInterruptMask(void)
{
    return (Cy_MCWDT_GetInterruptMask(mcwdt_HW));
}


/*******************************************************************************
* Function Name: mcwdt_SetInterruptMask
****************************************************************************//**
*
* Invokes the Cy_MCWDT_SetInterruptMask() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void mcwdt_SetInterruptMask(uint32_t counters)
{
    Cy_MCWDT_SetInterruptMask(mcwdt_HW, counters);
}


/*******************************************************************************
* Function Name: mcwdt_GetInterruptStatusMasked
****************************************************************************//**
* Invokes the Cy_MCWDT_GetInterruptStatusMasked() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t mcwdt_GetInterruptStatusMasked(void)
{
    return (Cy_MCWDT_GetInterruptStatusMasked(mcwdt_HW));
}


/*******************************************************************************
* Function Name: mcwdt_GetCountCascaded
****************************************************************************//**
* Invokes the Cy_MCWDT_GetCountCascaded() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t mcwdt_GetCountCascaded(void)
{
    return (Cy_MCWDT_GetCountCascaded(mcwdt_HW));
}

#if defined(__cplusplus)
}
#endif

#endif /* mcwdt_CY_MCWDT_PDL_H */


/* [] END OF FILE */
