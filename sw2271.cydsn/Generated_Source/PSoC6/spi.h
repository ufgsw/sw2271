/***************************************************************************//**
* \file spi.h
* \version 2.0
*
*  This file provides constants and parameter values for the SPI component.
*
********************************************************************************
* \copyright
* Copyright 2016-2017, Cypress Semiconductor Corporation. All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(spi_CY_SCB_SPI_PDL_H)
#define spi_CY_SCB_SPI_PDL_H

#include "cyfitter.h"
#include "scb/cy_scb_spi.h"

#if defined(__cplusplus)
extern "C" {
#endif

/***************************************
*        Function Prototypes
***************************************/
/**
* \addtogroup group_general
* @{
*/
/* Component specific functions. */
void spi_Start(void);

/* Basic functions. */
__STATIC_INLINE cy_en_scb_spi_status_t spi_Init(cy_stc_scb_spi_config_t const *config);
__STATIC_INLINE void spi_DeInit(void);
__STATIC_INLINE void spi_Enable(void);
__STATIC_INLINE void spi_Disable(void);

/* Register callback. */
__STATIC_INLINE void spi_RegisterCallback(cy_cb_scb_spi_handle_events_t callback);

/* Bus state. */
__STATIC_INLINE bool spi_IsBusBusy(void);

/* Slave select control. */
__STATIC_INLINE void spi_SetActiveSlaveSelect(cy_en_scb_spi_slave_select_t slaveSelect);
__STATIC_INLINE void spi_SetActiveSlaveSelectPolarity(cy_en_scb_spi_slave_select_t slaveSelect, 
                                                                   cy_en_scb_spi_polarity_t polarity);

/* Low level: read. */
__STATIC_INLINE uint32_t spi_Read(void);
__STATIC_INLINE uint32_t spi_ReadArray(void *buffer, uint32_t size);
__STATIC_INLINE uint32_t spi_GetRxFifoStatus(void);
__STATIC_INLINE void     spi_ClearRxFifoStatus(uint32_t clearMask);
__STATIC_INLINE uint32_t spi_GetNumInRxFifo(void);
__STATIC_INLINE void     spi_ClearRxFifo(void);

/* Low level: write. */
__STATIC_INLINE uint32_t spi_Write(uint32_t data);
__STATIC_INLINE uint32_t spi_WriteArray(void *buffer, uint32_t size);
__STATIC_INLINE void     spi_WriteArrayBlocking(void *buffer, uint32_t size);
__STATIC_INLINE uint32_t spi_GetTxFifoStatus(void);
__STATIC_INLINE void     spi_ClearTxFifoStatus(uint32_t clearMask);
__STATIC_INLINE uint32_t spi_GetNumInTxFifo(void);
__STATIC_INLINE bool     spi_IsTxComplete(void);
__STATIC_INLINE void     spi_ClearTxFifo(void);

/* Master/slave specific status. */
__STATIC_INLINE uint32_t spi_GetSlaveMasterStatus(void);
__STATIC_INLINE void     spi_ClearSlaveMasterStatus(uint32_t clearMask);

/* High level: transfer functions. */
__STATIC_INLINE cy_en_scb_spi_status_t spi_Transfer(void *txBuffer, void *rxBuffer, uint32_t size);
__STATIC_INLINE void     spi_AbortTransfer(void);
__STATIC_INLINE uint32_t spi_GetTransferStatus(void);
__STATIC_INLINE uint32_t spi_GetNumTransfered(void);

/* Interrupt handler */
__STATIC_INLINE void spi_Interrupt(void);
/** @} group_general */


/***************************************
*    Variables with External Linkage
***************************************/
/**
* \addtogroup group_globals
* @{
*/
extern uint8_t spi_initVar;
extern cy_stc_scb_spi_config_t const spi_config;
extern cy_stc_scb_spi_context_t spi_context;
/** @} group_globals */


/***************************************
*         Preprocessor Macros
***************************************/
/**
* \addtogroup group_macros
* @{
*/
/** The pointer to the base address of the hardware */
#define spi_HW     ((CySCB_Type *) spi_SCB__HW)

/** The slave select line 0 constant which takes into account pin placement */
#define spi_SPI_SLAVE_SELECT0    ( (cy_en_scb_spi_slave_select_t) spi_SCB__SS0_POSITION)

/** The slave select line 1 constant which takes into account pin placement */
#define spi_SPI_SLAVE_SELECT1    ( (cy_en_scb_spi_slave_select_t) spi_SCB__SS1_POSITION)

/** The slave select line 2 constant which takes into account pin placement */
#define spi_SPI_SLAVE_SELECT2    ( (cy_en_scb_spi_slave_select_t) spi_SCB__SS2_POSITION)

/** The slave select line 3 constant which takes into account pin placement */
#define spi_SPI_SLAVE_SELECT3    ((cy_en_scb_spi_slave_select_t) spi_SCB__SS3_POSITION)
/** @} group_macros */


/***************************************
*    In-line Function Implementation
***************************************/

/*******************************************************************************
* Function Name: spi_Init
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_Init() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE cy_en_scb_spi_status_t spi_Init(cy_stc_scb_spi_config_t const *config)
{
    return Cy_SCB_SPI_Init(spi_HW, config, &spi_context);
}


/*******************************************************************************
* Function Name: spi_DeInit
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_DeInit() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void spi_DeInit(void)
{
    Cy_SCB_SPI_DeInit(spi_HW);
}


/*******************************************************************************
* Function Name: spi_Enable
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_Enable() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void spi_Enable(void)
{
    Cy_SCB_SPI_Enable(spi_HW);
}


/*******************************************************************************
* Function Name: spi_Disable
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_Disable() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void spi_Disable(void)
{
    Cy_SCB_SPI_Disable(spi_HW, &spi_context);
}


/*******************************************************************************
* Function Name: spi_RegisterCallback
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_RegisterCallback() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void spi_RegisterCallback(cy_cb_scb_spi_handle_events_t callback)
{
    Cy_SCB_SPI_RegisterCallback(spi_HW, callback, &spi_context);
}


/*******************************************************************************
* Function Name: spi_IsBusBusy
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_IsBusBusy() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE bool spi_IsBusBusy(void)
{
    return Cy_SCB_SPI_IsBusBusy(spi_HW);
}


/*******************************************************************************
* Function Name: spi_SetActiveSlaveSelect
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_SetActiveSlaveSelect() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void spi_SetActiveSlaveSelect(cy_en_scb_spi_slave_select_t slaveSelect)
{
    Cy_SCB_SPI_SetActiveSlaveSelect(spi_HW, slaveSelect);
}


/*******************************************************************************
* Function Name: spi_SetActiveSlaveSelectPolarity
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_SetActiveSlaveSelectPolarity() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void spi_SetActiveSlaveSelectPolarity(cy_en_scb_spi_slave_select_t slaveSelect, 
                                                                   cy_en_scb_spi_polarity_t polarity)
{
    Cy_SCB_SPI_SetActiveSlaveSelectPolarity(spi_HW, slaveSelect, polarity);
}


/*******************************************************************************
* Function Name: spi_Read
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_Read() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t spi_Read(void)
{
    return Cy_SCB_SPI_Read(spi_HW);
}


/*******************************************************************************
* Function Name: spi_ReadArray
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_ReadArray() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t spi_ReadArray(void *buffer, uint32_t size)
{
    return Cy_SCB_SPI_ReadArray(spi_HW, buffer, size);
}


/*******************************************************************************
* Function Name: spi_GetRxFifoStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_GetRxFifoStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t spi_GetRxFifoStatus(void)
{
    return Cy_SCB_SPI_GetRxFifoStatus(spi_HW);
}


/*******************************************************************************
* Function Name: spi_ClearRxFifoStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_ClearRxFifoStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void spi_ClearRxFifoStatus(uint32_t clearMask)
{
    Cy_SCB_SPI_ClearRxFifoStatus(spi_HW, clearMask);
}


/*******************************************************************************
* Function Name: spi_GetNumInRxFifo
****************************************************************************//**
*
* Invokes the Cy_SCB_GetNumInRxFifo() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t spi_GetNumInRxFifo(void)
{
    return Cy_SCB_GetNumInRxFifo(spi_HW);
}


/*******************************************************************************
* Function Name: spi_ClearRxFifo
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_ClearRxFifo() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void spi_ClearRxFifo(void)
{
    Cy_SCB_SPI_ClearRxFifo(spi_HW);
}


/*******************************************************************************
* Function Name: spi_Write
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_Write() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t spi_Write(uint32_t data)
{
    return Cy_SCB_SPI_Write(spi_HW, data);
}


/*******************************************************************************
* Function Name: spi_WriteArray
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_WriteArray() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t spi_WriteArray(void *buffer, uint32_t size)
{
    return Cy_SCB_SPI_WriteArray(spi_HW, buffer, size);
}


/*******************************************************************************
* Function Name: spi_WriteArrayBlocking
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_WriteArrayBlocking() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void spi_WriteArrayBlocking(void *buffer, uint32_t size)
{
    Cy_SCB_SPI_WriteArrayBlocking(spi_HW, buffer, size);
}


/*******************************************************************************
* Function Name: spi_GetTxFifoStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_GetTxFifoStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t spi_GetTxFifoStatus(void)
{
    return Cy_SCB_SPI_GetTxFifoStatus(spi_HW);
}


/*******************************************************************************
* Function Name: spi_ClearTxFifoStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_ClearTxFifoStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void spi_ClearTxFifoStatus(uint32_t clearMask)
{
    Cy_SCB_SPI_ClearTxFifoStatus(spi_HW, clearMask);
}


/*******************************************************************************
* Function Name: spi_GetNumInTxFifo
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_GetNumInTxFifo() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t spi_GetNumInTxFifo(void)
{
    return Cy_SCB_SPI_GetNumInTxFifo(spi_HW);
}


/*******************************************************************************
* Function Name: spi_IsTxComplete
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_IsTxComplete() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE bool spi_IsTxComplete(void)
{
    return Cy_SCB_SPI_IsTxComplete(spi_HW);
}


/*******************************************************************************
* Function Name: spi_ClearTxFifo
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_ClearTxFifo() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void spi_ClearTxFifo(void)
{
    Cy_SCB_SPI_ClearTxFifo(spi_HW);
}


/*******************************************************************************
* Function Name: spi_GetSlaveMasterStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_GetSlaveMasterStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t spi_GetSlaveMasterStatus(void)
{
    return Cy_SCB_SPI_GetSlaveMasterStatus(spi_HW);
}


/*******************************************************************************
* Function Name: spi_ClearSlaveMasterStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_ClearSlaveMasterStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void spi_ClearSlaveMasterStatus(uint32_t clearMask)
{
    Cy_SCB_SPI_ClearSlaveMasterStatus(spi_HW, clearMask);
}


/*******************************************************************************
* Function Name: spi_Transfer
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_Transfer() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE cy_en_scb_spi_status_t spi_Transfer(void *txBuffer, void *rxBuffer, uint32_t size)
{
    return Cy_SCB_SPI_Transfer(spi_HW, txBuffer, rxBuffer, size, &spi_context);
}

/*******************************************************************************
* Function Name: spi_AbortTransfer
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_AbortTransfer() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void spi_AbortTransfer(void)
{
    Cy_SCB_SPI_AbortTransfer(spi_HW, &spi_context);
}


/*******************************************************************************
* Function Name: spi_GetTransferStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_GetTransferStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t spi_GetTransferStatus(void)
{
    return Cy_SCB_SPI_GetTransferStatus(spi_HW, &spi_context);
}


/*******************************************************************************
* Function Name: spi_GetNumTransfered
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_GetNumTransfered() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t spi_GetNumTransfered(void)
{
    return Cy_SCB_SPI_GetNumTransfered(spi_HW, &spi_context);
}


/*******************************************************************************
* Function Name: spi_Interrupt
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_Interrupt() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void spi_Interrupt(void)
{
    Cy_SCB_SPI_Interrupt(spi_HW, &spi_context);
}


#if defined(__cplusplus)
}
#endif

#endif /* spi_CY_SCB_SPI_PDL_H */


/* [] END OF FILE */
