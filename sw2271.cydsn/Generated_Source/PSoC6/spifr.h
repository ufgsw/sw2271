/***************************************************************************//**
* \file spifr.h
* \version 2.0
*
*  This file provides constants and parameter values for the SPI component.
*
********************************************************************************
* \copyright
* Copyright 2016-2017, Cypress Semiconductor Corporation. All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(spifr_CY_SCB_SPI_PDL_H)
#define spifr_CY_SCB_SPI_PDL_H

#include "cyfitter.h"
#include "scb/cy_scb_spi.h"

#if defined(__cplusplus)
extern "C" {
#endif

/***************************************
*        Function Prototypes
***************************************/
/**
* \addtogroup group_general
* @{
*/
/* Component specific functions. */
void spifr_Start(void);

/* Basic functions. */
__STATIC_INLINE cy_en_scb_spi_status_t spifr_Init(cy_stc_scb_spi_config_t const *config);
__STATIC_INLINE void spifr_DeInit(void);
__STATIC_INLINE void spifr_Enable(void);
__STATIC_INLINE void spifr_Disable(void);

/* Register callback. */
__STATIC_INLINE void spifr_RegisterCallback(cy_cb_scb_spi_handle_events_t callback);

/* Bus state. */
__STATIC_INLINE bool spifr_IsBusBusy(void);

/* Slave select control. */
__STATIC_INLINE void spifr_SetActiveSlaveSelect(cy_en_scb_spi_slave_select_t slaveSelect);
__STATIC_INLINE void spifr_SetActiveSlaveSelectPolarity(cy_en_scb_spi_slave_select_t slaveSelect, 
                                                                   cy_en_scb_spi_polarity_t polarity);

/* Low level: read. */
__STATIC_INLINE uint32_t spifr_Read(void);
__STATIC_INLINE uint32_t spifr_ReadArray(void *buffer, uint32_t size);
__STATIC_INLINE uint32_t spifr_GetRxFifoStatus(void);
__STATIC_INLINE void     spifr_ClearRxFifoStatus(uint32_t clearMask);
__STATIC_INLINE uint32_t spifr_GetNumInRxFifo(void);
__STATIC_INLINE void     spifr_ClearRxFifo(void);

/* Low level: write. */
__STATIC_INLINE uint32_t spifr_Write(uint32_t data);
__STATIC_INLINE uint32_t spifr_WriteArray(void *buffer, uint32_t size);
__STATIC_INLINE void     spifr_WriteArrayBlocking(void *buffer, uint32_t size);
__STATIC_INLINE uint32_t spifr_GetTxFifoStatus(void);
__STATIC_INLINE void     spifr_ClearTxFifoStatus(uint32_t clearMask);
__STATIC_INLINE uint32_t spifr_GetNumInTxFifo(void);
__STATIC_INLINE bool     spifr_IsTxComplete(void);
__STATIC_INLINE void     spifr_ClearTxFifo(void);

/* Master/slave specific status. */
__STATIC_INLINE uint32_t spifr_GetSlaveMasterStatus(void);
__STATIC_INLINE void     spifr_ClearSlaveMasterStatus(uint32_t clearMask);

/* High level: transfer functions. */
__STATIC_INLINE cy_en_scb_spi_status_t spifr_Transfer(void *txBuffer, void *rxBuffer, uint32_t size);
__STATIC_INLINE void     spifr_AbortTransfer(void);
__STATIC_INLINE uint32_t spifr_GetTransferStatus(void);
__STATIC_INLINE uint32_t spifr_GetNumTransfered(void);

/* Interrupt handler */
__STATIC_INLINE void spifr_Interrupt(void);
/** @} group_general */


/***************************************
*    Variables with External Linkage
***************************************/
/**
* \addtogroup group_globals
* @{
*/
extern uint8_t spifr_initVar;
extern cy_stc_scb_spi_config_t const spifr_config;
extern cy_stc_scb_spi_context_t spifr_context;
/** @} group_globals */


/***************************************
*         Preprocessor Macros
***************************************/
/**
* \addtogroup group_macros
* @{
*/
/** The pointer to the base address of the hardware */
#define spifr_HW     ((CySCB_Type *) spifr_SCB__HW)

/** The slave select line 0 constant which takes into account pin placement */
#define spifr_SPI_SLAVE_SELECT0    ( (cy_en_scb_spi_slave_select_t) spifr_SCB__SS0_POSITION)

/** The slave select line 1 constant which takes into account pin placement */
#define spifr_SPI_SLAVE_SELECT1    ( (cy_en_scb_spi_slave_select_t) spifr_SCB__SS1_POSITION)

/** The slave select line 2 constant which takes into account pin placement */
#define spifr_SPI_SLAVE_SELECT2    ( (cy_en_scb_spi_slave_select_t) spifr_SCB__SS2_POSITION)

/** The slave select line 3 constant which takes into account pin placement */
#define spifr_SPI_SLAVE_SELECT3    ((cy_en_scb_spi_slave_select_t) spifr_SCB__SS3_POSITION)
/** @} group_macros */


/***************************************
*    In-line Function Implementation
***************************************/

/*******************************************************************************
* Function Name: spifr_Init
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_Init() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE cy_en_scb_spi_status_t spifr_Init(cy_stc_scb_spi_config_t const *config)
{
    return Cy_SCB_SPI_Init(spifr_HW, config, &spifr_context);
}


/*******************************************************************************
* Function Name: spifr_DeInit
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_DeInit() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void spifr_DeInit(void)
{
    Cy_SCB_SPI_DeInit(spifr_HW);
}


/*******************************************************************************
* Function Name: spifr_Enable
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_Enable() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void spifr_Enable(void)
{
    Cy_SCB_SPI_Enable(spifr_HW);
}


/*******************************************************************************
* Function Name: spifr_Disable
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_Disable() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void spifr_Disable(void)
{
    Cy_SCB_SPI_Disable(spifr_HW, &spifr_context);
}


/*******************************************************************************
* Function Name: spifr_RegisterCallback
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_RegisterCallback() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void spifr_RegisterCallback(cy_cb_scb_spi_handle_events_t callback)
{
    Cy_SCB_SPI_RegisterCallback(spifr_HW, callback, &spifr_context);
}


/*******************************************************************************
* Function Name: spifr_IsBusBusy
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_IsBusBusy() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE bool spifr_IsBusBusy(void)
{
    return Cy_SCB_SPI_IsBusBusy(spifr_HW);
}


/*******************************************************************************
* Function Name: spifr_SetActiveSlaveSelect
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_SetActiveSlaveSelect() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void spifr_SetActiveSlaveSelect(cy_en_scb_spi_slave_select_t slaveSelect)
{
    Cy_SCB_SPI_SetActiveSlaveSelect(spifr_HW, slaveSelect);
}


/*******************************************************************************
* Function Name: spifr_SetActiveSlaveSelectPolarity
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_SetActiveSlaveSelectPolarity() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void spifr_SetActiveSlaveSelectPolarity(cy_en_scb_spi_slave_select_t slaveSelect, 
                                                                   cy_en_scb_spi_polarity_t polarity)
{
    Cy_SCB_SPI_SetActiveSlaveSelectPolarity(spifr_HW, slaveSelect, polarity);
}


/*******************************************************************************
* Function Name: spifr_Read
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_Read() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t spifr_Read(void)
{
    return Cy_SCB_SPI_Read(spifr_HW);
}


/*******************************************************************************
* Function Name: spifr_ReadArray
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_ReadArray() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t spifr_ReadArray(void *buffer, uint32_t size)
{
    return Cy_SCB_SPI_ReadArray(spifr_HW, buffer, size);
}


/*******************************************************************************
* Function Name: spifr_GetRxFifoStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_GetRxFifoStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t spifr_GetRxFifoStatus(void)
{
    return Cy_SCB_SPI_GetRxFifoStatus(spifr_HW);
}


/*******************************************************************************
* Function Name: spifr_ClearRxFifoStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_ClearRxFifoStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void spifr_ClearRxFifoStatus(uint32_t clearMask)
{
    Cy_SCB_SPI_ClearRxFifoStatus(spifr_HW, clearMask);
}


/*******************************************************************************
* Function Name: spifr_GetNumInRxFifo
****************************************************************************//**
*
* Invokes the Cy_SCB_GetNumInRxFifo() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t spifr_GetNumInRxFifo(void)
{
    return Cy_SCB_GetNumInRxFifo(spifr_HW);
}


/*******************************************************************************
* Function Name: spifr_ClearRxFifo
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_ClearRxFifo() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void spifr_ClearRxFifo(void)
{
    Cy_SCB_SPI_ClearRxFifo(spifr_HW);
}


/*******************************************************************************
* Function Name: spifr_Write
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_Write() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t spifr_Write(uint32_t data)
{
    return Cy_SCB_SPI_Write(spifr_HW, data);
}


/*******************************************************************************
* Function Name: spifr_WriteArray
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_WriteArray() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t spifr_WriteArray(void *buffer, uint32_t size)
{
    return Cy_SCB_SPI_WriteArray(spifr_HW, buffer, size);
}


/*******************************************************************************
* Function Name: spifr_WriteArrayBlocking
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_WriteArrayBlocking() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void spifr_WriteArrayBlocking(void *buffer, uint32_t size)
{
    Cy_SCB_SPI_WriteArrayBlocking(spifr_HW, buffer, size);
}


/*******************************************************************************
* Function Name: spifr_GetTxFifoStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_GetTxFifoStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t spifr_GetTxFifoStatus(void)
{
    return Cy_SCB_SPI_GetTxFifoStatus(spifr_HW);
}


/*******************************************************************************
* Function Name: spifr_ClearTxFifoStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_ClearTxFifoStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void spifr_ClearTxFifoStatus(uint32_t clearMask)
{
    Cy_SCB_SPI_ClearTxFifoStatus(spifr_HW, clearMask);
}


/*******************************************************************************
* Function Name: spifr_GetNumInTxFifo
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_GetNumInTxFifo() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t spifr_GetNumInTxFifo(void)
{
    return Cy_SCB_SPI_GetNumInTxFifo(spifr_HW);
}


/*******************************************************************************
* Function Name: spifr_IsTxComplete
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_IsTxComplete() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE bool spifr_IsTxComplete(void)
{
    return Cy_SCB_SPI_IsTxComplete(spifr_HW);
}


/*******************************************************************************
* Function Name: spifr_ClearTxFifo
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_ClearTxFifo() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void spifr_ClearTxFifo(void)
{
    Cy_SCB_SPI_ClearTxFifo(spifr_HW);
}


/*******************************************************************************
* Function Name: spifr_GetSlaveMasterStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_GetSlaveMasterStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t spifr_GetSlaveMasterStatus(void)
{
    return Cy_SCB_SPI_GetSlaveMasterStatus(spifr_HW);
}


/*******************************************************************************
* Function Name: spifr_ClearSlaveMasterStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_ClearSlaveMasterStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void spifr_ClearSlaveMasterStatus(uint32_t clearMask)
{
    Cy_SCB_SPI_ClearSlaveMasterStatus(spifr_HW, clearMask);
}


/*******************************************************************************
* Function Name: spifr_Transfer
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_Transfer() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE cy_en_scb_spi_status_t spifr_Transfer(void *txBuffer, void *rxBuffer, uint32_t size)
{
    return Cy_SCB_SPI_Transfer(spifr_HW, txBuffer, rxBuffer, size, &spifr_context);
}

/*******************************************************************************
* Function Name: spifr_AbortTransfer
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_AbortTransfer() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void spifr_AbortTransfer(void)
{
    Cy_SCB_SPI_AbortTransfer(spifr_HW, &spifr_context);
}


/*******************************************************************************
* Function Name: spifr_GetTransferStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_GetTransferStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t spifr_GetTransferStatus(void)
{
    return Cy_SCB_SPI_GetTransferStatus(spifr_HW, &spifr_context);
}


/*******************************************************************************
* Function Name: spifr_GetNumTransfered
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_GetNumTransfered() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t spifr_GetNumTransfered(void)
{
    return Cy_SCB_SPI_GetNumTransfered(spifr_HW, &spifr_context);
}


/*******************************************************************************
* Function Name: spifr_Interrupt
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_Interrupt() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void spifr_Interrupt(void)
{
    Cy_SCB_SPI_Interrupt(spifr_HW, &spifr_context);
}


#if defined(__cplusplus)
}
#endif

#endif /* spifr_CY_SCB_SPI_PDL_H */


/* [] END OF FILE */
