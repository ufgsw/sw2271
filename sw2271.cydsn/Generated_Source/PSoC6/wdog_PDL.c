/*******************************************************************************
* File Name: WDOG.c
* Version 1.10
*
* Description:
*  This file provides the source code to the API for the WDOG
*  component.
*
********************************************************************************
* Copyright 2016, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "WDOG_PDL.h"


/** WDOG_initVar indicates whether the WDOG  component
*  has been initialized. The variable is initialized to 0 and set to 1 the first
*  time WDOG_Start() is called.
*  This allows the component to restart without reinitialization after the first 
*  call to the WDOG_Start() routine.
*
*  If re-initialization of the component is required, then the 
*  WDOG_Init() function can be called before the 
*  WDOG_Start() or WDOG_Enable() function.
*/
uint8 WDOG_initVar = 0u;

/** The instance-specific configuration structure. This should be used in the 
*  associated WDOG_Init() function.
*/ 
const cy_stc_mcwdt_config_t WDOG_config =
{
    .c0Match     = WDOG_C0_MATCH,
    .c1Match     = WDOG_C1_MATCH,
    .c0Mode      = WDOG_C0_MODE,
    .c1Mode      = WDOG_C1_MODE,
    .c2ToggleBit = WDOG_C2_PERIOD,
    .c2Mode      = WDOG_C2_MODE,
    .c0ClearOnMatch = (bool)WDOG_C0_CLEAR_ON_MATCH,
    .c1ClearOnMatch = (bool)WDOG_C1_CLEAR_ON_MATCH,
    .c0c1Cascade = (bool)WDOG_CASCADE_C0C1,
    .c1c2Cascade = (bool)WDOG_CASCADE_C1C2
};


/*******************************************************************************
* Function Name: WDOG_Start
****************************************************************************//**
*
*  Sets the initVar variable, calls the Init() function, unmasks the 
*  corresponding counter interrupts and then calls the Enable() function 
*  to enable the counters.
*
* \globalvars
*  \ref WDOG_initVar
*
*  \note
*  When this API is called, the counter starts counting after two lf_clk cycles 
*  pass. It is the user's responsibility to check whether the selected counters
*  were enabled immediately after the function call. This can be done by the 
*  WDOG_GetEnabledStatus() API.
*
*******************************************************************************/
void WDOG_Start(void)
{
    if (0u == WDOG_initVar)
    {
        (void)WDOG_Init(&WDOG_config);
        WDOG_initVar = 1u; /* Component was initialized */
    }

	/* Set interrupt masks for the counters */
	WDOG_SetInterruptMask(WDOG_CTRS_INT_MASK);

	/* Enable the counters that are enabled in the customizer */
    WDOG_Enable(WDOG_ENABLED_CTRS_MASK, 0u);
}


/*******************************************************************************
* Function Name: WDOG_Stop
****************************************************************************//**
*
*  Calls the Disable() function to disable all counters.
*
*******************************************************************************/
void WDOG_Stop(void)
{
    WDOG_Disable(CY_MCWDT_CTR_Msk, WDOG_TWO_LF_CLK_CYCLES_DELAY);
    WDOG_DeInit();
}


/* [] END OF FILE */
