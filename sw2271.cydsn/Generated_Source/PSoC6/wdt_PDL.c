/*******************************************************************************
* File Name: wdt.c
* Version 1.10
*
* Description:
*  This file provides the source code to the API for the wdt
*  component.
*
********************************************************************************
* Copyright 2016, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "wdt_PDL.h"


/** wdt_initVar indicates whether the wdt  component
*  has been initialized. The variable is initialized to 0 and set to 1 the first
*  time wdt_Start() is called.
*  This allows the component to restart without reinitialization after the first 
*  call to the wdt_Start() routine.
*
*  If re-initialization of the component is required, then the 
*  wdt_Init() function can be called before the 
*  wdt_Start() or wdt_Enable() function.
*/
uint8 wdt_initVar = 0u;

/** The instance-specific configuration structure. This should be used in the 
*  associated wdt_Init() function.
*/ 
const cy_stc_mcwdt_config_t wdt_config =
{
    .c0Match     = wdt_C0_MATCH,
    .c1Match     = wdt_C1_MATCH,
    .c0Mode      = wdt_C0_MODE,
    .c1Mode      = wdt_C1_MODE,
    .c2ToggleBit = wdt_C2_PERIOD,
    .c2Mode      = wdt_C2_MODE,
    .c0ClearOnMatch = (bool)wdt_C0_CLEAR_ON_MATCH,
    .c1ClearOnMatch = (bool)wdt_C1_CLEAR_ON_MATCH,
    .c0c1Cascade = (bool)wdt_CASCADE_C0C1,
    .c1c2Cascade = (bool)wdt_CASCADE_C1C2
};


/*******************************************************************************
* Function Name: wdt_Start
****************************************************************************//**
*
*  Sets the initVar variable, calls the Init() function, unmasks the 
*  corresponding counter interrupts and then calls the Enable() function 
*  to enable the counters.
*
* \globalvars
*  \ref wdt_initVar
*
*  \note
*  When this API is called, the counter starts counting after two lf_clk cycles 
*  pass. It is the user's responsibility to check whether the selected counters
*  were enabled immediately after the function call. This can be done by the 
*  wdt_GetEnabledStatus() API.
*
*******************************************************************************/
void wdt_Start(void)
{
    if (0u == wdt_initVar)
    {
        (void)wdt_Init(&wdt_config);
        wdt_initVar = 1u; /* Component was initialized */
    }

	/* Set interrupt masks for the counters */
	wdt_SetInterruptMask(wdt_CTRS_INT_MASK);

	/* Enable the counters that are enabled in the customizer */
    wdt_Enable(wdt_ENABLED_CTRS_MASK, 0u);
}


/*******************************************************************************
* Function Name: wdt_Stop
****************************************************************************//**
*
*  Calls the Disable() function to disable all counters.
*
*******************************************************************************/
void wdt_Stop(void)
{
    wdt_Disable(CY_MCWDT_CTR_Msk, wdt_TWO_LF_CLK_CYCLES_DELAY);
    wdt_DeInit();
}


/* [] END OF FILE */
