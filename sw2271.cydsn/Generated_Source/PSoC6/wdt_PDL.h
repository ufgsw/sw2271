/*******************************************************************************
* File Name: wdt.h
* Version 1.10
*
* Description:
*  This file provides constants and parameter values for the wdt
*  component.
*
********************************************************************************
* Copyright 2016, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(wdt_CY_MCWDT_PDL_H)
#define wdt_CY_MCWDT_PDL_H

#if defined(__cplusplus)
extern "C" {
#endif

#include "cyfitter.h"
#include "mcwdt/cy_mcwdt.h"

/*******************************************************************************
*   Variables
*******************************************************************************/
/**
* \addtogroup group_globals
* @{
*/
extern uint8_t  wdt_initVar;
extern const cy_stc_mcwdt_config_t wdt_config;
/** @} group_globals */

/***************************************
*   Conditional Compilation Parameters
****************************************/
#define wdt_C0_CLEAR_ON_MATCH  (1U)
#define wdt_C1_CLEAR_ON_MATCH  (1U)
#define wdt_CASCADE_C0C1       (0U)
#define wdt_CASCADE_C1C2       (0U)
#define wdt_C0_MATCH           (32000U)
#define wdt_C0_MODE            (1U)
#define wdt_C1_MATCH           (60000U)
#define wdt_C1_MODE            (1U)
#define wdt_C2_PERIOD          (16U)
#define wdt_C2_MODE            (0U)

#if (0u == 1U)
    #define wdt_CTR0_EN_MASK   0UL
#else
    #define wdt_CTR0_EN_MASK   CY_MCWDT_CTR0
#endif
#if (0u == 0U)
    #define wdt_CTR1_EN_MASK   0UL
#else
    #define wdt_CTR1_EN_MASK   CY_MCWDT_CTR1
#endif
#if (0u == 0U)
    #define wdt_CTR2_EN_MASK   0UL
#else
    #define wdt_CTR2_EN_MASK   CY_MCWDT_CTR2
#endif

#define wdt_ENABLED_CTRS_MASK  (wdt_CTR0_EN_MASK |\
                                             wdt_CTR1_EN_MASK |\
                                             wdt_CTR2_EN_MASK)
											 
#if (1U == wdt_C0_MODE) || (3U == wdt_C0_MODE)
    #define wdt_CTR0_INT_MASK   CY_MCWDT_CTR0
#else
    #define wdt_CTR0_INT_MASK   0UL
#endif
#if (1U == wdt_C1_MODE) || (3U == wdt_C1_MODE)
    #define wdt_CTR1_INT_MASK   CY_MCWDT_CTR1
#else
    #define wdt_CTR1_INT_MASK   0UL
#endif
#if (1U == wdt_C2_MODE)
    #define wdt_CTR2_INT_MASK   CY_MCWDT_CTR2
#else
    #define wdt_CTR2_INT_MASK   0UL
#endif 

#define wdt_CTRS_INT_MASK      (wdt_CTR0_INT_MASK |\
                                             wdt_CTR1_INT_MASK |\
                                             wdt_CTR2_INT_MASK)										 

/***************************************
*        Registers Constants
***************************************/

/* This is a ptr to the base address of the MCWDT instance. */
#define wdt_HW                 (wdt_MCWDT__HW)

#if (0u == wdt_MCWDT__IDX)
    #define wdt_RESET_REASON   CY_SYSLIB_RESET_SWWDT0
#else
    #define wdt_RESET_REASON   CY_SYSLIB_RESET_SWWDT1
#endif 

#define wdt_TWO_LF_CLK_CYCLES_DELAY (62u)


/*******************************************************************************
*        Function Prototypes
*******************************************************************************/
/**
* \addtogroup group_general
* @{
*/
                void     wdt_Start(void);
                void     wdt_Stop(void);
__STATIC_INLINE cy_en_mcwdt_status_t wdt_Init(const cy_stc_mcwdt_config_t *config);
__STATIC_INLINE void     wdt_DeInit(void);
__STATIC_INLINE void     wdt_Enable(uint32_t counters, uint16_t waitUs);
__STATIC_INLINE void     wdt_Disable(uint32_t counters, uint16_t waitUs);
__STATIC_INLINE uint32_t wdt_GetEnabledStatus(cy_en_mcwdtctr_t counter);
__STATIC_INLINE void     wdt_Lock(void);
__STATIC_INLINE void     wdt_Unlock(void);
__STATIC_INLINE uint32_t wdt_GetLockedStatus(void);
__STATIC_INLINE void     wdt_SetMode(cy_en_mcwdtctr_t counter, cy_en_mcwdtmode_t mode);
__STATIC_INLINE cy_en_mcwdtmode_t wdt_GetMode(cy_en_mcwdtctr_t counter);
__STATIC_INLINE void     wdt_SetClearOnMatch(cy_en_mcwdtctr_t counter, uint32_t enable);
__STATIC_INLINE uint32_t wdt_GetClearOnMatch(cy_en_mcwdtctr_t counter);
__STATIC_INLINE void     wdt_SetCascade(cy_en_mcwdtcascade_t cascade);
__STATIC_INLINE cy_en_mcwdtcascade_t wdt_GetCascade(void);
__STATIC_INLINE void     wdt_SetMatch(cy_en_mcwdtctr_t counter, uint32_t match, uint16_t waitUs);
__STATIC_INLINE uint32_t wdt_GetMatch(cy_en_mcwdtctr_t counter);
__STATIC_INLINE void     wdt_SetToggleBit(uint32_t bit);
__STATIC_INLINE uint32_t wdt_GetToggleBit(void);
__STATIC_INLINE uint32_t wdt_GetCount(cy_en_mcwdtctr_t counter);
__STATIC_INLINE void     wdt_ResetCounters(uint32_t counters, uint16_t waitUs);
__STATIC_INLINE uint32_t wdt_GetInterruptStatus(void);
__STATIC_INLINE void     wdt_ClearInterrupt(uint32_t counters);
__STATIC_INLINE void     wdt_SetInterrupt(uint32_t counters);
__STATIC_INLINE uint32_t wdt_GetInterruptMask(void);
__STATIC_INLINE void     wdt_SetInterruptMask(uint32_t counters);
__STATIC_INLINE uint32_t wdt_GetInterruptStatusMasked(void);
__STATIC_INLINE uint32_t wdt_GetCountCascaded(void);
/** @} general */


/*******************************************************************************
* Function Name: wdt_Init
****************************************************************************//**
*
* Invokes the Cy_MCWDT_Init() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE cy_en_mcwdt_status_t wdt_Init(const cy_stc_mcwdt_config_t *config)
{
    return (Cy_MCWDT_Init(wdt_HW, config));
}


/*******************************************************************************
* Function Name: wdt_DeInit
****************************************************************************//**
*
* Invokes the Cy_MCWDT_DeInit() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void wdt_DeInit(void)
{
    Cy_MCWDT_DeInit(wdt_HW);
}


/*******************************************************************************
* Function Name: wdt_Enable
****************************************************************************//**
*
* Invokes the Cy_MCWDT_Enable() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void wdt_Enable(uint32_t counters, uint16_t waitUs)
{
    Cy_MCWDT_Enable(wdt_HW, counters, waitUs);
}


/*******************************************************************************
* Function Name: wdt_Disable
****************************************************************************//**
*
* Invokes the Cy_MCWDT_Disable() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void wdt_Disable(uint32_t counters, uint16_t waitUs)
{
    Cy_MCWDT_Disable(wdt_HW, counters, waitUs);
}


/*******************************************************************************
* Function Name: wdt_GetEnabledStatus
****************************************************************************//**
*
* Invokes the Cy_MCWDT_GetEnabledStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t wdt_GetEnabledStatus(cy_en_mcwdtctr_t counter)
{
    return (Cy_MCWDT_GetEnabledStatus(wdt_HW, counter));
}


/*******************************************************************************
* Function Name: wdt_Lock
****************************************************************************//**
*
* Invokes the Cy_MCWDT_Lock() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void wdt_Lock(void)
{
    Cy_MCWDT_Lock(wdt_HW);
}


/*******************************************************************************
* Function Name: wdt_Unlock
****************************************************************************//**
*
* Invokes the Cy_MCWDT_Unlock() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void wdt_Unlock(void)
{
   Cy_MCWDT_Unlock(wdt_HW);
}


/*******************************************************************************
* Function Name: wdt_GetLockStatus
****************************************************************************//**
*
* Invokes the Cy_MCWDT_GetLockedStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t wdt_GetLockedStatus(void)
{
    return (Cy_MCWDT_GetLockedStatus(wdt_HW));
}


/*******************************************************************************
* Function Name: wdt_SetMode
****************************************************************************//**
*
* Invokes the Cy_MCWDT_SetMode() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void wdt_SetMode(cy_en_mcwdtctr_t counter, cy_en_mcwdtmode_t mode)
{
    Cy_MCWDT_SetMode(wdt_HW, counter, mode);
}


/*******************************************************************************
* Function Name: wdt_GetMode
****************************************************************************//**
*
* Invokes the Cy_MCWDT_GetMode() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE cy_en_mcwdtmode_t wdt_GetMode(cy_en_mcwdtctr_t counter)
{
    return (Cy_MCWDT_GetMode(wdt_HW, counter));
}


/*******************************************************************************
* Function Name: wdt_SetClearOnMatch
****************************************************************************//**
*
* Invokes the Cy_MCWDT_SetClearOnMatch() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void wdt_SetClearOnMatch(cy_en_mcwdtctr_t counter, uint32_t enable)
{
    Cy_MCWDT_SetClearOnMatch(wdt_HW, counter, enable);
}


/*******************************************************************************
* Function Name: wdt_GetClearOnMatch
****************************************************************************//**
*
* Invokes the Cy_MCWDT_GetClearOnMatch() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t wdt_GetClearOnMatch(cy_en_mcwdtctr_t counter)
{
    return (Cy_MCWDT_GetClearOnMatch(wdt_HW, counter));
}


/*******************************************************************************
* Function Name: wdt_SetCascade
****************************************************************************//**
*
* Invokes the Cy_MCWDT_SetCascade() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void wdt_SetCascade(cy_en_mcwdtcascade_t cascade)
{
    Cy_MCWDT_SetCascade(wdt_HW, cascade);
}


/*******************************************************************************
* Function Name: wdt_GetCascade
****************************************************************************//**
*
* Invokes the Cy_MCWDT_GetCascade() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE cy_en_mcwdtcascade_t wdt_GetCascade(void)
{
    return (Cy_MCWDT_GetCascade(wdt_HW));
}


/*******************************************************************************
* Function Name: wdt_SetMatch
****************************************************************************//**
*
* Invokes the Cy_MCWDT_SetMatch() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void wdt_SetMatch(cy_en_mcwdtctr_t counter, uint32_t match, uint16_t waitUs)
{
    Cy_MCWDT_SetMatch(wdt_HW, counter, match, waitUs);
}


/*******************************************************************************
* Function Name: wdt_GetMatch
****************************************************************************//**
*
* Invokes the Cy_MCWDT_GetMatch() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t wdt_GetMatch(cy_en_mcwdtctr_t counter)
{
    return (Cy_MCWDT_GetMatch(wdt_HW, counter));
}


/*******************************************************************************
* Function Name: wdt_SetToggleBit
****************************************************************************//**
*
* Invokes the Cy_MCWDT_SetToggleBit() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void wdt_SetToggleBit(uint32_t bit)
{
    Cy_MCWDT_SetToggleBit(wdt_HW, bit);
}

/*******************************************************************************
* Function Name: wdt_GetToggleBit
****************************************************************************//**
*
* Invokes the Cy_MCWDT_GetToggleBit() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t wdt_GetToggleBit(void)
{
    return (Cy_MCWDT_GetToggleBit(wdt_HW));
}


/*******************************************************************************
* Function Name: wdt_GetCount
****************************************************************************//**
*
* Invokes the Cy_MCWDT_GetCount() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t wdt_GetCount(cy_en_mcwdtctr_t counter)
{
    return (Cy_MCWDT_GetCount(wdt_HW, counter));
}


/*******************************************************************************
* Function Name: wdt_ResetCounters
****************************************************************************//**
*
* Invokes the Cy_MCWDT_ResetCounters() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void wdt_ResetCounters(uint32_t counters, uint16_t waitUs)
{
    Cy_MCWDT_ResetCounters(wdt_HW, counters, waitUs);
}


/*******************************************************************************
* Function Name: wdt_GetInterruptStatus
****************************************************************************//**
*
* Invokes the Cy_MCWDT_GetInterruptStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t wdt_GetInterruptStatus(void)
{
    return (Cy_MCWDT_GetInterruptStatus(wdt_HW));
}


/*******************************************************************************
* Function Name: wdt_ClearInterrupt
****************************************************************************//**
*
* Invokes the Cy_MCWDT_ClearInterrupt() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void wdt_ClearInterrupt(uint32_t counters)
{
    Cy_MCWDT_ClearInterrupt(wdt_HW, counters);
}


/*******************************************************************************
* Function Name: wdt_SetInterrupt
****************************************************************************//**
*
* Invokes the Cy_MCWDT_SetInterrupt() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void wdt_SetInterrupt(uint32_t counters)
{
    Cy_MCWDT_SetInterrupt(wdt_HW, counters);
}


/*******************************************************************************
* Function Name: wdt_GetInterruptMask
****************************************************************************//**
*
* Invokes the Cy_MCWDT_GetInterruptMask() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t wdt_GetInterruptMask(void)
{
    return (Cy_MCWDT_GetInterruptMask(wdt_HW));
}


/*******************************************************************************
* Function Name: wdt_SetInterruptMask
****************************************************************************//**
*
* Invokes the Cy_MCWDT_SetInterruptMask() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void wdt_SetInterruptMask(uint32_t counters)
{
    Cy_MCWDT_SetInterruptMask(wdt_HW, counters);
}


/*******************************************************************************
* Function Name: wdt_GetInterruptStatusMasked
****************************************************************************//**
* Invokes the Cy_MCWDT_GetInterruptStatusMasked() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t wdt_GetInterruptStatusMasked(void)
{
    return (Cy_MCWDT_GetInterruptStatusMasked(wdt_HW));
}


/*******************************************************************************
* Function Name: wdt_GetCountCascaded
****************************************************************************//**
* Invokes the Cy_MCWDT_GetCountCascaded() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t wdt_GetCountCascaded(void)
{
    return (Cy_MCWDT_GetCountCascaded(wdt_HW));
}

#if defined(__cplusplus)
}
#endif

#endif /* wdt_CY_MCWDT_PDL_H */


/* [] END OF FILE */
