/*******************************************************************************
* File Name: MIO_WDOG.h
* Version 1.10
*
* Description:
*  This file provides constants and parameter values for the MIO_WDOG
*  component.
*
********************************************************************************
* Copyright 2016, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(MIO_WDOG_CY_MCWDT_PDL_H)
#define MIO_WDOG_CY_MCWDT_PDL_H

#if defined(__cplusplus)
extern "C" {
#endif

#include "cyfitter.h"
#include "mcwdt/cy_mcwdt.h"

/*******************************************************************************
*   Variables
*******************************************************************************/
/**
* \addtogroup group_globals
* @{
*/
extern uint8_t  MIO_WDOG_initVar;
extern const cy_stc_mcwdt_config_t MIO_WDOG_config;
/** @} group_globals */

/***************************************
*   Conditional Compilation Parameters
****************************************/
#define MIO_WDOG_C0_CLEAR_ON_MATCH  (0U)
#define MIO_WDOG_C1_CLEAR_ON_MATCH  (1U)
#define MIO_WDOG_CASCADE_C0C1       (1U)
#define MIO_WDOG_CASCADE_C1C2       (0U)
#define MIO_WDOG_C0_MATCH           (32768U)
#define MIO_WDOG_C0_MODE            (0U)
#define MIO_WDOG_C1_MATCH           (4U)
#define MIO_WDOG_C1_MODE            (2U)
#define MIO_WDOG_C2_PERIOD          (16U)
#define MIO_WDOG_C2_MODE            (0U)

#if (0u == 1U)
    #define MIO_WDOG_CTR0_EN_MASK   0UL
#else
    #define MIO_WDOG_CTR0_EN_MASK   CY_MCWDT_CTR0
#endif
#if (0u == 1U)
    #define MIO_WDOG_CTR1_EN_MASK   0UL
#else
    #define MIO_WDOG_CTR1_EN_MASK   CY_MCWDT_CTR1
#endif
#if (0u == 0U)
    #define MIO_WDOG_CTR2_EN_MASK   0UL
#else
    #define MIO_WDOG_CTR2_EN_MASK   CY_MCWDT_CTR2
#endif

#define MIO_WDOG_ENABLED_CTRS_MASK  (MIO_WDOG_CTR0_EN_MASK |\
                                             MIO_WDOG_CTR1_EN_MASK |\
                                             MIO_WDOG_CTR2_EN_MASK)
											 
#if (1U == MIO_WDOG_C0_MODE) || (3U == MIO_WDOG_C0_MODE)
    #define MIO_WDOG_CTR0_INT_MASK   CY_MCWDT_CTR0
#else
    #define MIO_WDOG_CTR0_INT_MASK   0UL
#endif
#if (1U == MIO_WDOG_C1_MODE) || (3U == MIO_WDOG_C1_MODE)
    #define MIO_WDOG_CTR1_INT_MASK   CY_MCWDT_CTR1
#else
    #define MIO_WDOG_CTR1_INT_MASK   0UL
#endif
#if (1U == MIO_WDOG_C2_MODE)
    #define MIO_WDOG_CTR2_INT_MASK   CY_MCWDT_CTR2
#else
    #define MIO_WDOG_CTR2_INT_MASK   0UL
#endif 

#define MIO_WDOG_CTRS_INT_MASK      (MIO_WDOG_CTR0_INT_MASK |\
                                             MIO_WDOG_CTR1_INT_MASK |\
                                             MIO_WDOG_CTR2_INT_MASK)										 

/***************************************
*        Registers Constants
***************************************/

/* This is a ptr to the base address of the MCWDT instance. */
#define MIO_WDOG_HW                 (MIO_WDOG_MCWDT__HW)

#if (0u == MIO_WDOG_MCWDT__IDX)
    #define MIO_WDOG_RESET_REASON   CY_SYSLIB_RESET_SWWDT0
#else
    #define MIO_WDOG_RESET_REASON   CY_SYSLIB_RESET_SWWDT1
#endif 

#define MIO_WDOG_TWO_LF_CLK_CYCLES_DELAY (62u)


/*******************************************************************************
*        Function Prototypes
*******************************************************************************/
/**
* \addtogroup group_general
* @{
*/
                void     MIO_WDOG_Start(void);
                void     MIO_WDOG_Stop(void);
__STATIC_INLINE cy_en_mcwdt_status_t MIO_WDOG_Init(const cy_stc_mcwdt_config_t *config);
__STATIC_INLINE void     MIO_WDOG_DeInit(void);
__STATIC_INLINE void     MIO_WDOG_Enable(uint32_t counters, uint16_t waitUs);
__STATIC_INLINE void     MIO_WDOG_Disable(uint32_t counters, uint16_t waitUs);
__STATIC_INLINE uint32_t MIO_WDOG_GetEnabledStatus(cy_en_mcwdtctr_t counter);
__STATIC_INLINE void     MIO_WDOG_Lock(void);
__STATIC_INLINE void     MIO_WDOG_Unlock(void);
__STATIC_INLINE uint32_t MIO_WDOG_GetLockedStatus(void);
__STATIC_INLINE void     MIO_WDOG_SetMode(cy_en_mcwdtctr_t counter, cy_en_mcwdtmode_t mode);
__STATIC_INLINE cy_en_mcwdtmode_t MIO_WDOG_GetMode(cy_en_mcwdtctr_t counter);
__STATIC_INLINE void     MIO_WDOG_SetClearOnMatch(cy_en_mcwdtctr_t counter, uint32_t enable);
__STATIC_INLINE uint32_t MIO_WDOG_GetClearOnMatch(cy_en_mcwdtctr_t counter);
__STATIC_INLINE void     MIO_WDOG_SetCascade(cy_en_mcwdtcascade_t cascade);
__STATIC_INLINE cy_en_mcwdtcascade_t MIO_WDOG_GetCascade(void);
__STATIC_INLINE void     MIO_WDOG_SetMatch(cy_en_mcwdtctr_t counter, uint32_t match, uint16_t waitUs);
__STATIC_INLINE uint32_t MIO_WDOG_GetMatch(cy_en_mcwdtctr_t counter);
__STATIC_INLINE void     MIO_WDOG_SetToggleBit(uint32_t bit);
__STATIC_INLINE uint32_t MIO_WDOG_GetToggleBit(void);
__STATIC_INLINE uint32_t MIO_WDOG_GetCount(cy_en_mcwdtctr_t counter);
__STATIC_INLINE void     MIO_WDOG_ResetCounters(uint32_t counters, uint16_t waitUs);
__STATIC_INLINE uint32_t MIO_WDOG_GetInterruptStatus(void);
__STATIC_INLINE void     MIO_WDOG_ClearInterrupt(uint32_t counters);
__STATIC_INLINE void     MIO_WDOG_SetInterrupt(uint32_t counters);
__STATIC_INLINE uint32_t MIO_WDOG_GetInterruptMask(void);
__STATIC_INLINE void     MIO_WDOG_SetInterruptMask(uint32_t counters);
__STATIC_INLINE uint32_t MIO_WDOG_GetInterruptStatusMasked(void);
__STATIC_INLINE uint32_t MIO_WDOG_GetCountCascaded(void);
/** @} general */


/*******************************************************************************
* Function Name: MIO_WDOG_Init
****************************************************************************//**
*
* Invokes the Cy_MCWDT_Init() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE cy_en_mcwdt_status_t MIO_WDOG_Init(const cy_stc_mcwdt_config_t *config)
{
    return (Cy_MCWDT_Init(MIO_WDOG_HW, config));
}


/*******************************************************************************
* Function Name: MIO_WDOG_DeInit
****************************************************************************//**
*
* Invokes the Cy_MCWDT_DeInit() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void MIO_WDOG_DeInit(void)
{
    Cy_MCWDT_DeInit(MIO_WDOG_HW);
}


/*******************************************************************************
* Function Name: MIO_WDOG_Enable
****************************************************************************//**
*
* Invokes the Cy_MCWDT_Enable() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void MIO_WDOG_Enable(uint32_t counters, uint16_t waitUs)
{
    Cy_MCWDT_Enable(MIO_WDOG_HW, counters, waitUs);
}


/*******************************************************************************
* Function Name: MIO_WDOG_Disable
****************************************************************************//**
*
* Invokes the Cy_MCWDT_Disable() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void MIO_WDOG_Disable(uint32_t counters, uint16_t waitUs)
{
    Cy_MCWDT_Disable(MIO_WDOG_HW, counters, waitUs);
}


/*******************************************************************************
* Function Name: MIO_WDOG_GetEnabledStatus
****************************************************************************//**
*
* Invokes the Cy_MCWDT_GetEnabledStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t MIO_WDOG_GetEnabledStatus(cy_en_mcwdtctr_t counter)
{
    return (Cy_MCWDT_GetEnabledStatus(MIO_WDOG_HW, counter));
}


/*******************************************************************************
* Function Name: MIO_WDOG_Lock
****************************************************************************//**
*
* Invokes the Cy_MCWDT_Lock() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void MIO_WDOG_Lock(void)
{
    Cy_MCWDT_Lock(MIO_WDOG_HW);
}


/*******************************************************************************
* Function Name: MIO_WDOG_Unlock
****************************************************************************//**
*
* Invokes the Cy_MCWDT_Unlock() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void MIO_WDOG_Unlock(void)
{
   Cy_MCWDT_Unlock(MIO_WDOG_HW);
}


/*******************************************************************************
* Function Name: MIO_WDOG_GetLockStatus
****************************************************************************//**
*
* Invokes the Cy_MCWDT_GetLockedStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t MIO_WDOG_GetLockedStatus(void)
{
    return (Cy_MCWDT_GetLockedStatus(MIO_WDOG_HW));
}


/*******************************************************************************
* Function Name: MIO_WDOG_SetMode
****************************************************************************//**
*
* Invokes the Cy_MCWDT_SetMode() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void MIO_WDOG_SetMode(cy_en_mcwdtctr_t counter, cy_en_mcwdtmode_t mode)
{
    Cy_MCWDT_SetMode(MIO_WDOG_HW, counter, mode);
}


/*******************************************************************************
* Function Name: MIO_WDOG_GetMode
****************************************************************************//**
*
* Invokes the Cy_MCWDT_GetMode() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE cy_en_mcwdtmode_t MIO_WDOG_GetMode(cy_en_mcwdtctr_t counter)
{
    return (Cy_MCWDT_GetMode(MIO_WDOG_HW, counter));
}


/*******************************************************************************
* Function Name: MIO_WDOG_SetClearOnMatch
****************************************************************************//**
*
* Invokes the Cy_MCWDT_SetClearOnMatch() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void MIO_WDOG_SetClearOnMatch(cy_en_mcwdtctr_t counter, uint32_t enable)
{
    Cy_MCWDT_SetClearOnMatch(MIO_WDOG_HW, counter, enable);
}


/*******************************************************************************
* Function Name: MIO_WDOG_GetClearOnMatch
****************************************************************************//**
*
* Invokes the Cy_MCWDT_GetClearOnMatch() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t MIO_WDOG_GetClearOnMatch(cy_en_mcwdtctr_t counter)
{
    return (Cy_MCWDT_GetClearOnMatch(MIO_WDOG_HW, counter));
}


/*******************************************************************************
* Function Name: MIO_WDOG_SetCascade
****************************************************************************//**
*
* Invokes the Cy_MCWDT_SetCascade() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void MIO_WDOG_SetCascade(cy_en_mcwdtcascade_t cascade)
{
    Cy_MCWDT_SetCascade(MIO_WDOG_HW, cascade);
}


/*******************************************************************************
* Function Name: MIO_WDOG_GetCascade
****************************************************************************//**
*
* Invokes the Cy_MCWDT_GetCascade() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE cy_en_mcwdtcascade_t MIO_WDOG_GetCascade(void)
{
    return (Cy_MCWDT_GetCascade(MIO_WDOG_HW));
}


/*******************************************************************************
* Function Name: MIO_WDOG_SetMatch
****************************************************************************//**
*
* Invokes the Cy_MCWDT_SetMatch() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void MIO_WDOG_SetMatch(cy_en_mcwdtctr_t counter, uint32_t match, uint16_t waitUs)
{
    Cy_MCWDT_SetMatch(MIO_WDOG_HW, counter, match, waitUs);
}


/*******************************************************************************
* Function Name: MIO_WDOG_GetMatch
****************************************************************************//**
*
* Invokes the Cy_MCWDT_GetMatch() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t MIO_WDOG_GetMatch(cy_en_mcwdtctr_t counter)
{
    return (Cy_MCWDT_GetMatch(MIO_WDOG_HW, counter));
}


/*******************************************************************************
* Function Name: MIO_WDOG_SetToggleBit
****************************************************************************//**
*
* Invokes the Cy_MCWDT_SetToggleBit() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void MIO_WDOG_SetToggleBit(uint32_t bit)
{
    Cy_MCWDT_SetToggleBit(MIO_WDOG_HW, bit);
}

/*******************************************************************************
* Function Name: MIO_WDOG_GetToggleBit
****************************************************************************//**
*
* Invokes the Cy_MCWDT_GetToggleBit() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t MIO_WDOG_GetToggleBit(void)
{
    return (Cy_MCWDT_GetToggleBit(MIO_WDOG_HW));
}


/*******************************************************************************
* Function Name: MIO_WDOG_GetCount
****************************************************************************//**
*
* Invokes the Cy_MCWDT_GetCount() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t MIO_WDOG_GetCount(cy_en_mcwdtctr_t counter)
{
    return (Cy_MCWDT_GetCount(MIO_WDOG_HW, counter));
}


/*******************************************************************************
* Function Name: MIO_WDOG_ResetCounters
****************************************************************************//**
*
* Invokes the Cy_MCWDT_ResetCounters() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void MIO_WDOG_ResetCounters(uint32_t counters, uint16_t waitUs)
{
    Cy_MCWDT_ResetCounters(MIO_WDOG_HW, counters, waitUs);
}


/*******************************************************************************
* Function Name: MIO_WDOG_GetInterruptStatus
****************************************************************************//**
*
* Invokes the Cy_MCWDT_GetInterruptStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t MIO_WDOG_GetInterruptStatus(void)
{
    return (Cy_MCWDT_GetInterruptStatus(MIO_WDOG_HW));
}


/*******************************************************************************
* Function Name: MIO_WDOG_ClearInterrupt
****************************************************************************//**
*
* Invokes the Cy_MCWDT_ClearInterrupt() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void MIO_WDOG_ClearInterrupt(uint32_t counters)
{
    Cy_MCWDT_ClearInterrupt(MIO_WDOG_HW, counters);
}


/*******************************************************************************
* Function Name: MIO_WDOG_SetInterrupt
****************************************************************************//**
*
* Invokes the Cy_MCWDT_SetInterrupt() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void MIO_WDOG_SetInterrupt(uint32_t counters)
{
    Cy_MCWDT_SetInterrupt(MIO_WDOG_HW, counters);
}


/*******************************************************************************
* Function Name: MIO_WDOG_GetInterruptMask
****************************************************************************//**
*
* Invokes the Cy_MCWDT_GetInterruptMask() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t MIO_WDOG_GetInterruptMask(void)
{
    return (Cy_MCWDT_GetInterruptMask(MIO_WDOG_HW));
}


/*******************************************************************************
* Function Name: MIO_WDOG_SetInterruptMask
****************************************************************************//**
*
* Invokes the Cy_MCWDT_SetInterruptMask() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void MIO_WDOG_SetInterruptMask(uint32_t counters)
{
    Cy_MCWDT_SetInterruptMask(MIO_WDOG_HW, counters);
}


/*******************************************************************************
* Function Name: MIO_WDOG_GetInterruptStatusMasked
****************************************************************************//**
* Invokes the Cy_MCWDT_GetInterruptStatusMasked() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t MIO_WDOG_GetInterruptStatusMasked(void)
{
    return (Cy_MCWDT_GetInterruptStatusMasked(MIO_WDOG_HW));
}


/*******************************************************************************
* Function Name: MIO_WDOG_GetCountCascaded
****************************************************************************//**
* Invokes the Cy_MCWDT_GetCountCascaded() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t MIO_WDOG_GetCountCascaded(void)
{
    return (Cy_MCWDT_GetCountCascaded(MIO_WDOG_HW));
}

#if defined(__cplusplus)
}
#endif

#endif /* MIO_WDOG_CY_MCWDT_PDL_H */


/* [] END OF FILE */
