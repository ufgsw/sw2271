/***************************************************************************//**
* \file com.h
* \version 2.0
*
*  This file provides constants and parameter values for the UART component.
*
********************************************************************************
* \copyright
* Copyright 2016-2017, Cypress Semiconductor Corporation. All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(com_CY_SCB_UART_PDL_H)
#define com_CY_SCB_UART_PDL_H

#include "cyfitter.h"
#include "scb/cy_scb_uart.h"

#if defined(__cplusplus)
extern "C" {
#endif

/***************************************
*   Initial Parameter Constants
****************************************/

#define com_DIRECTION  (3U)
#define com_ENABLE_RTS (0U)
#define com_ENABLE_CTS (0U)

/* UART direction enum */
#define com_RX    (0x1U)
#define com_TX    (0x2U)

#define com_ENABLE_RX  (0UL != (com_DIRECTION & com_RX))
#define com_ENABLE_TX  (0UL != (com_DIRECTION & com_TX))


/***************************************
*        Function Prototypes
***************************************/
/**
* \addtogroup group_general
* @{
*/
/* Component specific functions. */
void com_Start(void);

/* Basic functions */
__STATIC_INLINE cy_en_scb_uart_status_t com_Init(cy_stc_scb_uart_config_t const *config);
__STATIC_INLINE void com_DeInit(void);
__STATIC_INLINE void com_Enable(void);
__STATIC_INLINE void com_Disable(void);

/* Register callback. */
__STATIC_INLINE void com_RegisterCallback(cy_cb_scb_uart_handle_events_t callback);

/* Configuration change. */
#if (com_ENABLE_CTS)
__STATIC_INLINE void com_EnableCts(void);
__STATIC_INLINE void com_DisableCts(void);
#endif /* (com_ENABLE_CTS) */

#if (com_ENABLE_RTS)
__STATIC_INLINE void     com_SetRtsFifoLevel(uint32_t level);
__STATIC_INLINE uint32_t com_GetRtsFifoLevel(void);
#endif /* (com_ENABLE_RTS) */

__STATIC_INLINE void com_EnableSkipStart(void);
__STATIC_INLINE void com_DisableSkipStart(void);

#if (com_ENABLE_RX)
/* Low level: Receive direction. */
__STATIC_INLINE uint32_t com_Get(void);
__STATIC_INLINE uint32_t com_GetArray(void *buffer, uint32_t size);
__STATIC_INLINE void     com_GetArrayBlocking(void *buffer, uint32_t size);
__STATIC_INLINE uint32_t com_GetRxFifoStatus(void);
__STATIC_INLINE void     com_ClearRxFifoStatus(uint32_t clearMask);
__STATIC_INLINE uint32_t com_GetNumInRxFifo(void);
__STATIC_INLINE void     com_ClearRxFifo(void);
#endif /* (com_ENABLE_RX) */

#if (com_ENABLE_TX)
/* Low level: Transmit direction. */
__STATIC_INLINE uint32_t com_Put(uint32_t data);
__STATIC_INLINE uint32_t com_PutArray(void *buffer, uint32_t size);
__STATIC_INLINE void     com_PutArrayBlocking(void *buffer, uint32_t size);
__STATIC_INLINE void     com_PutString(char_t const string[]);
__STATIC_INLINE void     com_SendBreakBlocking(uint32_t breakWidth);
__STATIC_INLINE uint32_t com_GetTxFifoStatus(void);
__STATIC_INLINE void     com_ClearTxFifoStatus(uint32_t clearMask);
__STATIC_INLINE uint32_t com_GetNumInTxFifo(void);
__STATIC_INLINE bool     com_IsTxComplete(void);
__STATIC_INLINE void     com_ClearTxFifo(void);
#endif /* (com_ENABLE_TX) */

#if (com_ENABLE_RX)
/* High level: Ring buffer functions. */
__STATIC_INLINE void     com_StartRingBuffer(void *buffer, uint32_t size);
__STATIC_INLINE void     com_StopRingBuffer(void);
__STATIC_INLINE void     com_ClearRingBuffer(void);
__STATIC_INLINE uint32_t com_GetNumInRingBuffer(void);

/* High level: Receive direction functions. */
__STATIC_INLINE cy_en_scb_uart_status_t com_Receive(void *buffer, uint32_t size);
__STATIC_INLINE void     com_AbortReceive(void);
__STATIC_INLINE uint32_t com_GetReceiveStatus(void);
__STATIC_INLINE uint32_t com_GetNumReceived(void);
#endif /* (com_ENABLE_RX) */

#if (com_ENABLE_TX)
/* High level: Transmit direction functions. */
__STATIC_INLINE cy_en_scb_uart_status_t com_Transmit(void *buffer, uint32_t size);
__STATIC_INLINE void     com_AbortTransmit(void);
__STATIC_INLINE uint32_t com_GetTransmitStatus(void);
__STATIC_INLINE uint32_t com_GetNumLeftToTransmit(void);
#endif /* (com_ENABLE_TX) */

/* Interrupt handler */
__STATIC_INLINE void com_Interrupt(void);
/** @} group_general */


/***************************************
*    Variables with External Linkage
***************************************/
/**
* \addtogroup group_globals
* @{
*/
extern uint8_t com_initVar;
extern cy_stc_scb_uart_config_t const com_config;
extern cy_stc_scb_uart_context_t com_context;
/** @} group_globals */


/***************************************
*         Preprocessor Macros
***************************************/
/**
* \addtogroup group_macros
* @{
*/
/** The pointer to the base address of the hardware */
#define com_HW     ((CySCB_Type *) com_SCB__HW)
/** @} group_macros */


/***************************************
*    In-line Function Implementation
***************************************/

/*******************************************************************************
* Function Name: com_Init
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_Init() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE cy_en_scb_uart_status_t com_Init(cy_stc_scb_uart_config_t const *config)
{
   return Cy_SCB_UART_Init(com_HW, config, &com_context);
}


/*******************************************************************************
* Function Name: com_DeInit
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_DeInit() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void com_DeInit(void)
{
    Cy_SCB_UART_DeInit(com_HW);
}


/*******************************************************************************
* Function Name: com_Enable
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_Enable() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void com_Enable(void)
{
    Cy_SCB_UART_Enable(com_HW);
}


/*******************************************************************************
* Function Name: com_Disable
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_Disable() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void com_Disable(void)
{
    Cy_SCB_UART_Disable(com_HW, &com_context);
}


/*******************************************************************************
* Function Name: com_RegisterCallback
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_RegisterCallback() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void com_RegisterCallback(cy_cb_scb_uart_handle_events_t callback)
{
    Cy_SCB_UART_RegisterCallback(com_HW, callback, &com_context);
}


#if (com_ENABLE_CTS)
/*******************************************************************************
* Function Name: com_EnableCts
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_EnableCts() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void com_EnableCts(void)
{
    Cy_SCB_UART_EnableCts(com_HW);
}


/*******************************************************************************
* Function Name: Cy_SCB_UART_DisableCts
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_DisableCts() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void com_DisableCts(void)
{
    Cy_SCB_UART_DisableCts(com_HW);
}
#endif /* (com_ENABLE_CTS) */


#if (com_ENABLE_RTS)
/*******************************************************************************
* Function Name: com_SetRtsFifoLevel
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_SetRtsFifoLevel() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void com_SetRtsFifoLevel(uint32_t level)
{
    Cy_SCB_UART_SetRtsFifoLevel(com_HW, level);
}


/*******************************************************************************
* Function Name: com_GetRtsFifoLevel
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetRtsFifoLevel() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t com_GetRtsFifoLevel(void)
{
    return Cy_SCB_UART_GetRtsFifoLevel(com_HW);
}
#endif /* (com_ENABLE_RTS) */


/*******************************************************************************
* Function Name: com_EnableSkipStart
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_EnableSkipStart() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void com_EnableSkipStart(void)
{
    Cy_SCB_UART_EnableSkipStart(com_HW);
}


/*******************************************************************************
* Function Name: com_DisableSkipStart
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_DisableSkipStart() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void com_DisableSkipStart(void)
{
    Cy_SCB_UART_DisableSkipStart(com_HW);
}


#if (com_ENABLE_RX)
/*******************************************************************************
* Function Name: com_Get
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_Get() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t com_Get(void)
{
    return Cy_SCB_UART_Get(com_HW);
}


/*******************************************************************************
* Function Name: com_GetArray
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetArray() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t com_GetArray(void *buffer, uint32_t size)
{
    return Cy_SCB_UART_GetArray(com_HW, buffer, size);
}


/*******************************************************************************
* Function Name: com_GetArrayBlocking
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetArrayBlocking() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void com_GetArrayBlocking(void *buffer, uint32_t size)
{
    Cy_SCB_UART_GetArrayBlocking(com_HW, buffer, size);
}


/*******************************************************************************
* Function Name: com_GetRxFifoStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetRxFifoStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t com_GetRxFifoStatus(void)
{
    return Cy_SCB_UART_GetRxFifoStatus(com_HW);
}


/*******************************************************************************
* Function Name: com_ClearRxFifoStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_ClearRxFifoStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void com_ClearRxFifoStatus(uint32_t clearMask)
{
    Cy_SCB_UART_ClearRxFifoStatus(com_HW, clearMask);
}


/*******************************************************************************
* Function Name: com_GetNumInRxFifo
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetNumInRxFifo() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t com_GetNumInRxFifo(void)
{
    return Cy_SCB_UART_GetNumInRxFifo(com_HW);
}


/*******************************************************************************
* Function Name: com_ClearRxFifo
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_ClearRxFifo() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void com_ClearRxFifo(void)
{
    Cy_SCB_UART_ClearRxFifo(com_HW);
}
#endif /* (com_ENABLE_RX) */


#if (com_ENABLE_TX)
/*******************************************************************************
* Function Name: com_Put
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_Put() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t com_Put(uint32_t data)
{
    return Cy_SCB_UART_Put(com_HW,data);
}


/*******************************************************************************
* Function Name: com_PutArray
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_PutArray() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t com_PutArray(void *buffer, uint32_t size)
{
    return Cy_SCB_UART_PutArray(com_HW, buffer, size);
}


/*******************************************************************************
* Function Name: com_PutArrayBlocking
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_PutArrayBlocking() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void com_PutArrayBlocking(void *buffer, uint32_t size)
{
    Cy_SCB_UART_PutArrayBlocking(com_HW, buffer, size);
}


/*******************************************************************************
* Function Name: com_PutString
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_PutString() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void com_PutString(char_t const string[])
{
    Cy_SCB_UART_PutString(com_HW, string);
}


/*******************************************************************************
* Function Name: com_SendBreakBlocking
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_SendBreakBlocking() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void com_SendBreakBlocking(uint32_t breakWidth)
{
    Cy_SCB_UART_SendBreakBlocking(com_HW, breakWidth);
}


/*******************************************************************************
* Function Name: com_GetTxFifoStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetTxFifoStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t com_GetTxFifoStatus(void)
{
    return Cy_SCB_UART_GetTxFifoStatus(com_HW);
}


/*******************************************************************************
* Function Name: com_ClearTxFifoStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_ClearTxFifoStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void com_ClearTxFifoStatus(uint32_t clearMask)
{
    Cy_SCB_UART_ClearTxFifoStatus(com_HW, clearMask);
}


/*******************************************************************************
* Function Name: com_GetNumInTxFifo
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetNumInTxFifo() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t com_GetNumInTxFifo(void)
{
    return Cy_SCB_UART_GetNumInTxFifo(com_HW);
}


/*******************************************************************************
* Function Name: com_IsTxComplete
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_IsTxComplete() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE bool com_IsTxComplete(void)
{
    return Cy_SCB_UART_IsTxComplete(com_HW);
}


/*******************************************************************************
* Function Name: com_ClearTxFifo
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_ClearTxFifo() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void com_ClearTxFifo(void)
{
    Cy_SCB_UART_ClearTxFifo(com_HW);
}
#endif /* (com_ENABLE_TX) */


#if (com_ENABLE_RX)
/*******************************************************************************
* Function Name: com_StartRingBuffer
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_StartRingBuffer() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void com_StartRingBuffer(void *buffer, uint32_t size)
{
    Cy_SCB_UART_StartRingBuffer(com_HW, buffer, size, &com_context);
}


/*******************************************************************************
* Function Name: com_StopRingBuffer
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_StopRingBuffer() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void com_StopRingBuffer(void)
{
    Cy_SCB_UART_StopRingBuffer(com_HW, &com_context);
}


/*******************************************************************************
* Function Name: com_ClearRingBuffer
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_ClearRingBuffer() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void com_ClearRingBuffer(void)
{
    Cy_SCB_UART_ClearRingBuffer(com_HW, &com_context);
}


/*******************************************************************************
* Function Name: com_GetNumInRingBuffer
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetNumInRingBuffer() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t com_GetNumInRingBuffer(void)
{
    return Cy_SCB_UART_GetNumInRingBuffer(com_HW, &com_context);
}


/*******************************************************************************
* Function Name: com_Receive
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_Receive() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE cy_en_scb_uart_status_t com_Receive(void *buffer, uint32_t size)
{
    return Cy_SCB_UART_Receive(com_HW, buffer, size, &com_context);
}


/*******************************************************************************
* Function Name: com_GetReceiveStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetReceiveStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t com_GetReceiveStatus(void)
{
    return Cy_SCB_UART_GetReceiveStatus(com_HW, &com_context);
}


/*******************************************************************************
* Function Name: com_AbortReceive
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_AbortReceive() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void com_AbortReceive(void)
{
    Cy_SCB_UART_AbortReceive(com_HW, &com_context);
}


/*******************************************************************************
* Function Name: com_GetNumReceived
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetNumReceived() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t com_GetNumReceived(void)
{
    return Cy_SCB_UART_GetNumReceived(com_HW, &com_context);
}
#endif /* (com_ENABLE_RX) */


#if (com_ENABLE_TX)
/*******************************************************************************
* Function Name: com_Transmit
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_Transmit() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE cy_en_scb_uart_status_t com_Transmit(void *buffer, uint32_t size)
{
    return Cy_SCB_UART_Transmit(com_HW, buffer, size, &com_context);
}


/*******************************************************************************
* Function Name: com_GetTransmitStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetTransmitStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t com_GetTransmitStatus(void)
{
    return Cy_SCB_UART_GetTransmitStatus(com_HW, &com_context);
}


/*******************************************************************************
* Function Name: com_AbortTransmit
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_AbortTransmit() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void com_AbortTransmit(void)
{
    Cy_SCB_UART_AbortTransmit(com_HW, &com_context);
}


/*******************************************************************************
* Function Name: com_GetNumLeftToTransmit
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetNumLeftToTransmit() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t com_GetNumLeftToTransmit(void)
{
    return Cy_SCB_UART_GetNumLeftToTransmit(com_HW, &com_context);
}
#endif /* (com_ENABLE_TX) */


/*******************************************************************************
* Function Name: com_Interrupt
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_Interrupt() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void com_Interrupt(void)
{
    Cy_SCB_UART_Interrupt(com_HW, &com_context);
}

#if defined(__cplusplus)
}
#endif

#endif /* com_CY_SCB_UART_PDL_H */


/* [] END OF FILE */
