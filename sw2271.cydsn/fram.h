/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"

// indirizzamento in FRam ( max 65536 byte )
#define ADDRESS_TEST        0x0000    // size = 0x00fc
#define ADDRESS_PAR         0x0002    // size = 0x00fc
#define ADDRESS_LOG         0x0200

#define cs_on()   Cy_GPIO_Write( cs_fram_PORT, cs_fram_NUM, 0 );
#define cs_off()  Cy_GPIO_Write( cs_fram_PORT, cs_fram_NUM, 1 );

typedef enum
{
   cmd_wrsr  = 0x01,
   cmd_read  = 0x03,
   cmd_write = 0x02,
   cmd_wrdi  = 0x04,
   cmd_rdsr  = 0x05,
   cmd_wren  = 0x06,
   cmd_rdid  = 0x9f,
   cmd_fstrd = 0x0b,
   cmd_sleep = 0xb9
}eCmd;

typedef struct
{
    unsigned WIP : 1;
    unsigned WEL : 1;
    unsigned BP0 : 1;
    unsigned BP1 : 1;
    unsigned RESERVED : 3;
    unsigned WPEN     : 1;
}sFramStatusReg;

typedef union
{
    sFramStatusReg  bits;
    uint8_t         val;
}uFramStatusReg;


bool fram_init();
bool fram_test();
bool fram_readId();
bool fram_writeByte( uint16_t address, uint8_t  dato );
bool fram_readByte(  uint16_t address, uint8_t* dato );
bool fram_writeArray(uint16_t address, uint8_t* arr  ,int count);
bool fram_readArray( uint16_t address, uint8_t* arr  ,int count);
bool fram_sleep();
bool fram_wakeup();
/* [] END OF FILE */
