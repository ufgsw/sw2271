/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "main_cm0p.h"


int main(void)
{
   cy_en_ble_api_result_t apiResult;
   
   uint32_t reset = Cy_SysLib_GetResetReason();
   Cy_SysLib_ClearResetReason();
   
   apiResult = Cy_BLE_Start(NULL);
   if(apiResult == CY_BLE_SUCCESS)
   {
      /* Enable CM4 only if BLE Controller started successfully.
      * CY_CORTEX_M4_APPL_ADDR must be updated if CM4 memory layout
      * is changed. */
      
      //ad = CY_CORTEX_M4_APPL_ADDR;
      //Cy_SysEnableCM4( 0x10080000UL);

#ifdef NDEBUG      
      // Per bootloader
      Cy_SysEnableCM4( (uint32_t) (&__cy_app_core1_start_addr) );
#else
      // Per debug
      Cy_SysEnableCM4( CY_CORTEX_M4_APPL_ADDR );
#endif

   }
   else
   {
      /* Halt CPU */
      //CY_ASSERT(0u != 0u);
      while(1)
      {
         //Cy_GPIO_Inv( led_rosso_PORT, led_rosso_NUM );
         CyDelay( 150 );
      }      
   }
   
   __enable_irq();
      
   for(;;)
   {
      Cy_BLE_ProcessEvents();
      /* Place your application code here. */
      Cy_SysPm_DeepSleep(CY_SYSPM_WAIT_FOR_INTERRUPT); 
   }
}

//
///*******************************************************************************
//* Function Name: Clock_EnterUltraLowPowerCallback
//****************************************************************************//**
//*
//* Enter Low Power Mode callback implementation. It reduces the FLL frequency by
//* half.
//*
//*******************************************************************************/
//cy_en_syspm_status_t Clock_EnterUltraLowPowerCallback( cy_stc_syspm_callback_params_t *callbackParams, cy_en_syspm_callback_mode_t mode)
//{
//    cy_en_syspm_status_t retVal = CY_SYSPM_FAIL;
//
//	switch (mode)
//	{
//		case CY_SYSPM_BEFORE_TRANSITION:
//			/* Disable the FLL */
//			Cy_SysClk_FllDisable();
//
//			/* Reconfigure the FLL to be half of the original frequency */
//			Cy_SysClk_FllConfigure(IMO_CLOCK, FLL_CLOCK_50_MHZ, CY_SYSCLK_FLLPLL_OUTPUT_AUTO);
//
//			/* Re-enable the FLL */
//			Cy_SysClk_FllEnable(FLL_CLOCK_TIMEOUT);
//
//			/* Set Peri Clock Divider to 0u, to keep at 50 MHz */
//			Cy_SysClk_ClkPeriSetDivider(0u);
//
//			retVal = CY_SYSPM_SUCCESS;
//			break;
//
//		default:
//			/* Don't do anything in the other modes */
//			retVal = CY_SYSPM_SUCCESS;
//			break;
//	}
//
//    return retVal;
//}
//
///*******************************************************************************
//* Function Name: Clock_ExitUltraLowPowerCallback
//****************************************************************************//**
//*
//* Exit Low Power Mode callback implementation. It sets the original FLL
//* frequency for the device.
//*
//*******************************************************************************/
//cy_en_syspm_status_t Clock_ExitUltraLowPowerCallback( cy_stc_syspm_callback_params_t *callbackParams, cy_en_syspm_callback_mode_t mode)
//{
//    cy_en_syspm_status_t retVal = CY_SYSPM_FAIL;
//
//	switch (mode)
//	{
//		case CY_SYSPM_AFTER_TRANSITION:
//			/* Set Peri Clock Divider to 1u, to keep at 50 MHz */
//			Cy_SysClk_ClkPeriSetDivider(1u);
//
//			/* Disable the FLL */
//			Cy_SysClk_FllDisable();
//
//			/* Reconfigure the FLL to be the original frequency */
//			Cy_SysClk_FllConfigure(IMO_CLOCK, FLL_CLOCK_100_MHZ, CY_SYSCLK_FLLPLL_OUTPUT_AUTO);
//
//			/* Re-enable the FLL */
//			Cy_SysClk_FllEnable(FLL_CLOCK_TIMEOUT);
//
//			retVal = CY_SYSPM_SUCCESS;
//			break;
//
//		default:
//			/* Don't do anything in the other modes */
//			retVal = CY_SYSPM_SUCCESS;
//			break;
//	}
//
//    return retVal;
//}
/* [] END OF FILE */
