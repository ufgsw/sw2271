/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "main_cm4.h"
#include "fram.h"
#include "crypto.h"
#include "ble/cy_ble_stack.h"

#include <stdio.h>
#include <stdlib.h>

CY_SECTION(".cy_app_signature") __USED const uint32_t cy_bootload_appSignature[1];

void HandleBleProcessing(void);
void StackEventHandler(uint32 event, void* eventParam);

void BleAssert(void);
void attrHandleInit(void);
void InitiateConnection(void);
void BleProcessing(void);
void tick_1ms(void);
void rxcom_isr(void);

void refresh_timeout_device(cy_stc_ble_gapc_adv_report_param_t* adv);

void procRxNotification(cy_stc_ble_gattc_handle_value_ntf_param_t* param);
int  wait_writeCharacteristic();
void EnableNotification(void);
//int read_com();
int proc_rxcom();
int verifica_bdaddress(message_s* msg);
void ISR_UART(void);


#define ENABLE_LOG 1
void print_log(char*);

#define RTC_INTR_TIME_SECONDS   (2u) 

#define NUM_SEC_IN_MINUTE       (60u)
#define ALARM1_ENABLED          (1u)
#define ALARM1_DISABLED         (0u)

/* The configurable GATT parameters */
#define GATT_CCD_HANDLE             (28u) 
#define GATT_CHAR_HANDLE            (27u) 
#define CY_BLE_CCCD_LEN             (0x02u) 

#define SUCCESS                     (  0u)

#define MAX_MTU_SIZE                (128u)
#define DEFAULT_MTU_SIZE            ( 23u)    
#define GATT_MTU                    (MAX_MTU_SIZE)

static INFO_EXCHANGE_STATE_T    infoExchangeState = INFO_EXCHANGE_START;

/* UUID of the custom BLE UART service */
uint8 bleUartServiceUuid[16] = { 0x31, 0x01, 0x9b, 0x5f, 0x80, 0x00, 0x00,0x80, 0x00, 0x10, 0x00, 0x00, 0xd0, 0xcd, 0x03, 0x00 };
/* UUID of the TX attribute of the custom BLE UART service */
uint8 uartTxAttrUuid[16] = { 0x31, 0x01, 0x9b, 0x5f, 0x80, 0x00, 0x00,0x80, 0x00, 0x10, 0x00, 0x00, 0xd1, 0xcd, 0x03, 0x00 };
/* UUID of the RX attribute of the custom BLE UART service */
uint8 uartRxAttrUuid[16] = { 0x31, 0x01, 0x9b, 0x5f, 0x80, 0x00, 0x00,0x80,  0x00, 0x10, 0x00, 0x00, 0xd2, 0xcd, 0x03, 0x00 };

cy_en_ble_api_result_t       apiResult = CY_BLE_SUCCESS;
cy_stc_ble_gap_bd_addr_t     local_addr;

/* GAP Related Information */
uint8   bdHandle;
bool    targetAddrFound = false;
bool    ble_shutdown    = true;
/* GATT Related Information */
/* Char handle for which notification will be received. */
const uint16  char_handle = GATT_CHAR_HANDLE;

/* CCD handle to enable notification. */
const uint16  desc_handle = GATT_CCD_HANDLE;

/* CCD value to enable notification*/
uint8   desc_val[CY_BLE_CCCD_LEN] = { 0x01, 0x00 };

cy_stc_ble_conn_handle_t            conn_handle;
cy_stc_ble_gatt_attr_handle_range_t range_service_handle;
cy_stc_ble_gatt_attr_handle_range_t range_char_uartrx_handle;

cy_stc_ble_gatt_write_param_t    send_param;
cy_stc_ble_gap_disconnect_info_t disconnect_info;
cy_stc_ble_gattc_read_by_type_rsp_param_t* read_param;
cy_stc_ble_gattc_read_by_type_rsp_param_t*characteristic_handle;
cy_stc_ble_gatt_write_param_t* write_param;
cy_stc_ble_gattc_find_by_type_rsp_param_t* find_param;
cy_stc_ble_gapc_adv_report_param_t* adv_report;
cy_stc_ble_gatt_err_param_t* err_param;
cy_stc_ble_gattc_handle_value_ntf_param_t* ntf_param;

cy_stc_ble_gattc_read_by_group_req_t      ReadByGroupReq;
cy_stc_ble_gattc_find_by_type_value_req_t param;
cy_stc_ble_gattc_read_by_type_req_t       gattc_c;
cy_stc_ble_gattc_find_info_req_t          FindInfoReq;
cy_stc_ble_gattc_find_info_rsp_param_t*   descriptor_handle;
cy_stc_ble_disc_descr_info_t              locDiscDescrInfo;
cy_stc_ble_disc_descr_info_t              charDiscDescrInfo;

uint16 txCharHandle = 0;                /* Handle for the TX data characteristic */
uint16 rxCharHandle = 0;                /* Handle for the RX data characteristic */
uint16 txCharDescHandle = 0;                /* Handle for the TX data characteristic descriptor */
uint16 bleUartServiceHandle = 0;                /* Handle for the BLE UART service */
uint16 bleUartServiceEndHandle = 0;                /* End handle for the BLE UART service */
uint16 mtuSize = CY_BLE_GATT_MTU;  /* MTU size to be used by Client and Server after MTU exchange */

/* Number of notification received */
uint32 notify_count;
uint8_t currentProxData;

uErr  error;

sParametri par;
sLogUfg    logUfg;

uint16_t timeout = 0;
uint16_t timeout_com = 0;
uint32_t timeout_send_adv = 0;
uint32_t timeout_rx_pic = 0;

bool sendReq = false;
bool sendCode = false;
bool isConnected = false;
bool lowPower = false;
bool writeCCCD = false;
bool writeOk = false;
bool writeErr = false;
bool scanErr = false;
bool getCrypto = false;

uint16_t tasto_premuto;

uint32_t key_press;
uint8_t  key = 0;
uint8_t  key_old = 0;
eFaseApp faseApp = inizio;

eBle     fase_ble       = init;
eBle     fase_ble_old   = init;
eBle     fase_ble_error = init;

uint8_t  buftx[100];
uint8_t  bufrx[100];
uint8_t  chiper[80];
uint8_t  revision = 0;
uint8_t  buflog[255];

uint8_t fl_rx_pic = 0;

uint8_t  fl_savelog = 0;

cy_stc_ble_timer_info_t  timerParam = { .timeout = 10 };

#define MAX_BUFF 300
char com_buflog[MAX_BUFF];
char com_buftx[MAX_BUFF];
char com_bufrx[MAX_BUFF];
int  com_lenrx;
int  com_lentx;

uint8_t tentativi_connessione = 0;
uint8_t tentativi_memorizzazzione_impronta = 0;
uint8_t tentativi_invio;
uint8_t tentativi_cccd;

#define MAX_DEVICE  20

device_s  devices[20];

message_s message_to_ble;
message_s messages_toble[20];
int       pSendToBle = 0;
int       pSendBle = 0;

extern uint8_t com_initVar;

int pSend = 0;
int pOld = MAX_DEVICE - 1;

int pMess = 0;
int pSendMes = 0;

const uint8_t emEeprom[Em_EEPROM_PHYSICAL_SIZE] __ALIGNED(CY_FLASH_SIZEOF_ROW) = { 0u };

#define MAX_MESSAGGI     200
#define MAX_LEN_MESSAGGI 200

uint8_t messaggi[MAX_MESSAGGI][MAX_LEN_MESSAGGI];
int pMessaggi = 0;
int sMessaggi = 0;

int size = 0;
int nsend;
int refr = 0;

int tentativi_falliti;

char buf_release[9];

char fl_send_release = 0;

int main(void)
{
    error.val = 0;

    revision = Cy_SysLib_GetDeviceRevision();
    error.bit.wco = !Cy_SysClk_WcoOkay();

    Cy_SysTick_Init(CY_SYSTICK_CLOCK_SOURCE_CLK_CPU, 100000 );
    Cy_SysTick_SetCallback(0, tick_1ms);
    Cy_SysTick_Enable();
    
    //Cy_SysInt_Init( &SysInt_rxcom_cfg, rxcom_isr);
    //NVIC_EnableIRQ( SysInt_rxcom_cfg.intrSrc );
    //NVIC_ClearPendingIRQ(SysInt_rxcom_cfg);

    // MIO_WDOG_Start();
    
    __enable_irq(); /* Enable global interrupts. */

    faseApp = inizio;

    com_Start();    
    com_debug_Start();
    
    com_HW->INTR_RX_MASK = SCB_INTR_RX_MASK_NOT_EMPTY_Msk;
    
    /* Interrupt Settings for UART */    
    Cy_SysInt_Init( &com_SCB_IRQ_cfg , ISR_UART);
    
    /* Enable the interrupt */
    NVIC_EnableIRQ(com_SCB_IRQ_cfg.intrSrc);

    if (crypto_init())
    {
        memcpy(crypto.aes, par.aes, 16);
        crypto_aes_init();
    }
    
    CyDelay(1000);

#if ENABLE_LOG            
    com_debug_PutString( "APPLICAZIONE INIZIATA : " );
    com_debug_PutString( "\r\n");
#endif   

    // Invio la release al PIC il quale la invierà al cloud
    buf_release[0] = '#';
    buf_release[1] = '0';
    buf_release[2] = '6';
    buf_release[3] = ';';
    buf_release[4] = '1';
    buf_release[5] = '2';
    buf_release[6] = '0';
    buf_release[7] = 0x0d;
    buf_release[8] = 0;

//    nsend = 0;
//    size  = 8;
//    while (nsend != size)
//    {
//        nsend += com_PutArray(buf_release, size - nsend);
//
//        while (com_IsTxComplete() == false);
//    }
                
    
    //Cy_WDT_Enable();
    
    for (;;)
    {      
        // MIO_WDOG_ResetCounters( CY_MCWDT_CTR0 | CY_MCWDT_CTR1 ,62);         
        
        Cy_BLE_ProcessEvents();
        BleProcessing();
        Cy_BLE_ProcessEvents();

        if(  fl_rx_pic ) 
        {
            proc_rxcom();
#if ENABLE_LOG            
            com_debug_PutString( "RICEVUTO DAL PIC : " );
            com_debug_PutArray( com_bufrx, com_lenrx );
            com_debug_PutString( "\r\n");
#endif            
            memset( com_bufrx, 0, com_lenrx );
            fl_rx_pic = 0;
        }
        
        if( fl_send_release == 1 )
        {
            nsend = 0;
            size  = 8;
            while (nsend != size)
            {
                nsend += com_PutArray(buf_release, size - nsend);

                while (com_IsTxComplete() == false);
            }        
            fl_send_release = 0;
        }                

        if (timeout_send_adv == 0)
        {
            // Ho dei messaggi da consegnare ?
            if (pMessaggi != sMessaggi)
            {
                // I messaggi sono bufferizzati, perchè al cloud non posso parlarci piu' frequentemente di un secondo
                size = strlen((const char*)&messaggi[sMessaggi][0]);
                nsend = 0;
#if ENABLE_LOG            
            com_debug_PutString( "INVIO AL PIC : " );
            com_debug_PutArray( &messaggi[sMessaggi][0] , size );
            com_debug_PutString( "\r\n");
#endif            
                while (nsend != size)
                {
                    nsend += com_PutArray(&messaggi[sMessaggi][nsend], size - nsend);

                    while (com_IsTxComplete() == false);
                }

                if (++sMessaggi == MAX_MESSAGGI) sMessaggi = 0;
                timeout_send_adv = 1000;
            }
            // Altrimenti invio gli advertising
            else
            {
                // Il prossimo ?            
                while (pSend < MAX_DEVICE)
                {
                    if (devices[pSend].timeout > 0)
                    {
                        // Invio l'ADV al PIC
                        com_lentx = sprintf(com_buftx, "#01;%02X%02X%02X%02X%02X%02X;%02X;%02X;%02X\r",
                            devices[pSend].BDaddress[0],
                            devices[pSend].BDaddress[1],
                            devices[pSend].BDaddress[2],
                            devices[pSend].BDaddress[3],
                            devices[pSend].BDaddress[4],
                            devices[pSend].BDaddress[5],
                            devices[pSend].a,
                            devices[pSend].b,
                            devices[pSend].c.val);
#if ENABLE_LOG            
                        com_debug_PutString( "INVIO AL PIC : " );
                        com_debug_PutArray( com_buftx , com_lentx );
                        com_debug_PutString( "\r\n");
#endif            
                        com_PutArray(com_buftx, com_lentx);
                        timeout_send_adv = 1000;
                        ++pSend;
                        break;
                    }
                    else
                        ++pSend;
                }
                if (pSend == MAX_DEVICE)
                {
                    //timeout_send_adv = 300000;
                    timeout_send_adv = 10000;
                    pSend = 0;
                }
            }
        }
    }
}

//int read_com()
//{
//    int ret = 0;
//    char rx;
//    //cy_en_scb_uart_status_t status;
//
//    while (com_GetNumInRxFifo())
//    {
//        com_Receive(&rx, 1);
//
//        if (rx == '#')
//        {
//            com_bufrx[0] = rx;
//            com_lenrx = 1;
//        }
//        else if (rx == 0x0d)
//        {
//            com_bufrx[com_lenrx] = 0;
//            ret = 1;
//        }
//        else if (com_lenrx < MAX_BUFF)
//        {
//            com_bufrx[com_lenrx] = rx;
//            ++com_lenrx;
//        }
//    }
//    return ret;
//}

void refresh_timeout_device(cy_stc_ble_gapc_adv_report_param_t* adv)
{
    int r;
    for (r = 0; r < MAX_DEVICE; r++)
    {
        if (memcmp(devices[r].BDaddress, adv->peerBdAddr, 6) == 0)
        {
            // Se l'advertising contiene dati diversi li incìvio subito al cloud
            
            if( devices[r].a     != adv->data[25] ) 
                timeout_send_adv = 1000;
            if( devices[r].b     != adv->data[26] ) 
                timeout_send_adv = 1000;
            if( devices[r].c.val != adv->data[27] ) 
                timeout_send_adv = 1000;
            
            devices[r].a      = adv->data[25];
            devices[r].b      = adv->data[26];
            devices[r].c.val  = adv->data[27];

#if ENABLE_LOG
            if( devices[r].c.type.abbina_bridge )
            {
                com_debug_PutString( "TROVATO ABBINA BRIDGE : \r\n" );   
            }
#endif
            devices[r].timeout = 60000;
            break;
        }
    }

    if (r == MAX_DEVICE)
    {
        // è la prima volta che vedo questo BDAddress
        
#if ENABLE_LOG
    sprintf( com_buflog, "BLE TROVATO ADV :%02x %02x %02x %02x %02x %02x :%02X\r\n" ,
        adv->peerBdAddr[0],
        adv->peerBdAddr[1],
        adv->peerBdAddr[2],
        adv->peerBdAddr[3],
        adv->peerBdAddr[4],
        adv->peerBdAddr[5],
        adv->data[27]);
    
    print_log( com_buflog );
#endif   

        for (r = 0; r < MAX_DEVICE; r++)
        {
            if (devices[r].timeout == 0)
            {
                devices[r].BDaddress[0] = adv->peerBdAddr[0];
                devices[r].BDaddress[1] = adv->peerBdAddr[1];
                devices[r].BDaddress[2] = adv->peerBdAddr[2];
                devices[r].BDaddress[3] = adv->peerBdAddr[3];
                devices[r].BDaddress[4] = adv->peerBdAddr[4];
                devices[r].BDaddress[5] = adv->peerBdAddr[5];
                devices[r].a = adv->data[25];
                devices[r].b = adv->data[26];
                devices[r].c.val = adv->data[27];
                devices[r].timeout = 60000;
                break;
            }
        }        
    }
}

//void store_checksum( int len )
//{
//    int a;
//    uint16_t c = 0;
//    
//    for( a=0; a < len; a++ )
//    {
//        c += (uint16_t)com_buftx[a];    
//    }
//    
//    com_buftx[len]   = (char)(c >> 8);
//    com_buftx[len+1] = (char)(c & 0xff);
//}

//int read_com(int timeout)
//{
//    static int fase_rx = 0;
//    
//    char rx;
//    cy_en_scb_uart_status_t status;
//    timeout_com = timeout;
//    int confirmation_code = -1;
//    
//    while( timeout_com )
//    {    
//        if(com_GetNumInRxFifo() )
//        {
//            status = com_Receive( &rx,1);      
//            // La trasmissione inizia e finisce con 0xc0
//            // se ricevo 0xdb controllo il dato seguente
//            // se il seguente è 0xdc salvo 0xc0
//            // se il seguente è 0xdd salvo 0xdb
//            switch(fase_rx)
//            {
//                case 0:
//                    com_lenrx = 0;
//                    if( rx == 0xc0 ) fase_rx = 1;
//                break;
//                case 1 :                
//                    if( rx == 0xdb ) fase_rx = 2;
//                    else if( rx == 0xc0 ) 
//                    {
//                        confirmation_code = proc_rxcom();
//                        fase_rx     = 0;
//                        timeout_com = 0;
//                    }
//                    else if( com_lenrx < MAX_BUFF )
//                    {
//                        com_bufrx[ com_lenrx++ ] = rx;            
//                    }
//                    else fase_rx = 0;
//                        
//                break;
//                case 2:
//                    if(      rx == 0xdc ) com_bufrx[ com_lenrx++ ] = 0xc0;
//                    else if( rx == 0xdd ) com_bufrx[ com_lenrx++ ] = 0xdb;
//                    fase_rx = 1;
//                break;
//            }
//        }
//    }
//    return confirmation_code;
//}

int proc_rxcom()
{
    int ini;
    int p;
    uint8_t b[3];

    message_s* msg_to_ble = &messages_toble[pSendToBle];

    // 
    if (com_bufrx[1] == '0' && com_bufrx[2] == '1')
    {
        // Arrivato un messaggio da inviare alla porta
        if (++pSendToBle == 20) pSendToBle = 0;

        memset(msg_to_ble, 0, sizeof(message_s));

        b[0] = com_bufrx[4];
        b[1] = com_bufrx[5];
        b[2] = 0;

        // BDAddress
        msg_to_ble->BDAddress[0] = (uint8_t)strtol((char*)b, 0, 16);
        b[0] = com_bufrx[6];
        b[1] = com_bufrx[7];
        msg_to_ble->BDAddress[1] = (uint8_t)strtol((char*)b, 0, 16);
        b[0] = com_bufrx[8];
        b[1] = com_bufrx[9];
        msg_to_ble->BDAddress[2] = (uint8_t)strtol((char*)b, 0, 16);
        b[0] = com_bufrx[10];
        b[1] = com_bufrx[11];
        msg_to_ble->BDAddress[3] = (uint8_t)strtol((char*)b, 0, 16);
        b[0] = com_bufrx[12];
        b[1] = com_bufrx[13];
        msg_to_ble->BDAddress[4] = (uint8_t)strtol((char*)b, 0, 16);
        b[0] = com_bufrx[14];
        b[1] = com_bufrx[15];
        msg_to_ble->BDAddress[5] = (uint8_t)strtol((char*)b, 0, 16);
        
        // Command
        if( com_bufrx[16] != ';'  ) return 0;
        b[0] = com_bufrx[17];
        b[1] = com_bufrx[18];
        msg_to_ble->command      = (uint8_t)strtol((char*)b, 0, 16);
        
        // Contiene l'id di risposta ?
        if( com_bufrx[22] == ';'  ) 
        {
            ini = 23;
            b[0] = com_bufrx[20];
            b[1] = com_bufrx[21];
            msg_to_ble->command      = (uint8_t)strtol((char*)b, 0, 16);            
        }
        else                        
        {
            ini = 20;    
            msg_to_ble->id_risposta  = 0;
        }        
        
        p = 0;
        while (ini < com_lenrx)
        {
            b[0] = com_bufrx[ini++];
            b[1] = com_bufrx[ini++];
            msg_to_ble->message[p++] = (uint8_t)strtol((char*)b, 0, 16);
        }
        msg_to_ble->message_len = p;

        if (msg_to_ble->message_len % 16 != 0) return 0;

        if (verifica_bdaddress(msg_to_ble))
        {
            if (msg_to_ble->command == 0x03)
            {
                msg_to_ble->send = 2;
            }
            msg_to_ble->timeout = 30000;
            msg_to_ble->send = 1;
            return 1;
        }
    }
    else if (com_bufrx[1] == '0' && com_bufrx[2] == '3')
    {
        // Arrivato il CID
        return 3;
    }
    else if (com_bufrx[1] == '0' && com_bufrx[2] == '4')
    {
#if ENABLE_LOG        
        com_debug_PutString( "TASTO PREMUTO : \r\n" );
#endif        
        tasto_premuto = 15000;
        return 4;
    }
    else if (com_bufrx[1] == '0' && com_bufrx[2] == '5')
    {
        //V Arrivata la richiesta della release
        fl_send_release = 1;
        return 5;
    }
    
    return 0;
    
}

int verifica_bdaddress(message_s* msg)
{
    int r;
    for (r = 0; r < MAX_DEVICE; r++)
    {
        if (devices[r].timeout > 0)
        {
            if (memcmp(&devices[r].BDaddress[0], &msg->BDAddress[0], 6) == 0) return 1;
        }
    }
    return 0;
}
int dev = 0;
cy_stc_ble_gap_bd_addr_t remote_addr;
void BleProcessing()
{
    static uint8_t pacchetto = 0;
    static uint8_t pacchetti = 0;
    //static uint8_t command   = 0;

    int ini;
    int len;

    cy_en_ble_api_result_t  cyble_api_result;
    cy_en_ble_conn_state_t  cy_ble_conn_state;

    cy_ble_conn_state = Cy_BLE_GetConnectionState(conn_handle);
    
    if( fase_ble != fase_ble_old )
    {
        sprintf( (char*)buflog, "CAMBIO FASE IN : %d", fase_ble);
        com_debug_PutString(buflog);
        com_debug_PutString( "\r\n");
        fase_ble_old = fase_ble;
    } 
    
    switch (fase_ble)
    {
    case init:
        timeout               = 0;
        tentativi_connessione = 0;
        tentativi_invio       = 0;
        tentativi_falliti     = 0;
        tentativi_cccd        = 0;
        isConnected           = false;
        
        //if( ble_shutdown == false ) break;
        
        if( cy_ble_state == CY_BLE_STATE_STOPPED )
        {
            Cy_BLE_Start(StackEventHandler);
            fase_ble = start_ble;
        }
        if( cy_ble_state == CY_BLE_STATE_ON         ) fase_ble = start_ble;
        if( cy_ble_state == CY_BLE_STATE_CONNECTING ) Cy_BLE_GAPC_CancelDeviceConnection();
        break;

    case start_ble:
        {
            int r;
            
            if (cy_ble_state != CY_BLE_STATE_ON) break;

            if (timeout) break;

            sendCode = false;
            scanErr  = false;
                      
            if (cy_ble_scanState == CY_BLE_SCAN_STATE_STOPPED)
            {       
                if( Cy_BLE_GAPC_StartScan(CY_BLE_SCANNING_FAST, CY_BLE_CENTRAL_CONFIGURATION_0_INDEX) == CY_BLE_SUCCESS )
                {
                    for (r = 0; r < MAX_DEVICE; r++)
                    {
                        // azzero l'elenco degli advertising visti
                        devices[r].timeout      = 0;
                        devices[r].BDaddress[0] = 0;
                        devices[r].BDaddress[1] = 0;
                        devices[r].BDaddress[2] = 0;
                        devices[r].BDaddress[3] = 0;
                        devices[r].BDaddress[4] = 0;
                        devices[r].BDaddress[5] = 0;
                    }
                    timeout = 10000;
                    fase_ble = wait_start_scan;
                }
            }
            else
                fase_ble = scan;
            
            break;
        }

    case wait_start_scan:
        if( timeout == 0 )
        {
            fase_ble = start_ble;
        }
        if (cy_ble_scanState == CY_BLE_SCAN_STATE_SCANNING )
        {
            fase_ble = scan;
        }
    break;
        
    case scan:
        if( tasto_premuto )
        {
            for (ini = 0; ini < MAX_DEVICE; ini++)
            {
                // Vedo se qualche porta vuole iniziare la procedura di abbinamento
                if (devices[ini].timeout > 0 && devices[ini].c.type.abbina_bridge == 1 )
                {
                    memset(&message_to_ble, 0, sizeof(message_s));

                    message_to_ble.BDAddress[0] = devices[ini].BDaddress[0];
                    message_to_ble.BDAddress[1] = devices[ini].BDaddress[1];
                    message_to_ble.BDAddress[2] = devices[ini].BDaddress[2];
                    message_to_ble.BDAddress[3] = devices[ini].BDaddress[3];
                    message_to_ble.BDAddress[4] = devices[ini].BDaddress[4];
                    message_to_ble.BDAddress[5] = devices[ini].BDaddress[5];
                    message_to_ble.message_len  = 64;
                    message_to_ble.command      = 0x24;
                    message_to_ble.id_risposta  = 0;
                    message_to_ble.message[0]   = 2;

                    // Chiedo il CID al PIC
                    com_lentx = sprintf(com_buftx, "#03;\r");
                    com_PutArray(com_buftx, com_lentx);

                    // attendo la risposta
                    timeout = 1000;
                    while (timeout)
                    {
                        if (fl_rx_pic )
                        {
                            if (proc_rxcom() == 3)
                            {
                                message_to_ble.message[1] = com_lenrx - 4;
                                memcpy(&message_to_ble.message[2], &com_bufrx[4], com_lenrx - 4);

                                message_to_ble.message[60] = RELEASE_FW;
                                message_to_ble.message[61] = RELEASE_HW;
                                
                                // Aggiungo questo device al mio elenco ? ( per ora no )
                                sendCode = true;
                                ini = MAX_DEVICE;
                                timeout = 0;
                                tasto_premuto = 0;
                            }
                            fl_rx_pic = 0;
                        }
                    }
                }
            }
        }

        if (pSendToBle != pSendBle && timeout_rx_pic == 0 )
        {
            // sono arrivatio messaggi da destinare alla serratura
            if (messages_toble[pSendBle].timeout == 0)
            {
                messages_toble[pSendBle].send = 0;
                if (++pSendBle == 20) pSendBle = 0;
            }
            else
            {
                memcpy(&message_to_ble, &messages_toble[pSendBle], sizeof(message_s));
                sendCode = true;
            }
        }
        else if( sendCode == false && tasto_premuto == 0)
        {
            // Vedo se qualche porta ha degli eventi da consegnare ( log )
            if (devices[dev].timeout > 0 && devices[dev].c.type.nuovo_evento == 1)
            {
                memset(&message_to_ble, 0, sizeof(message_s));

                message_to_ble.BDAddress[0] = devices[dev].BDaddress[0];
                message_to_ble.BDAddress[1] = devices[dev].BDaddress[1];
                message_to_ble.BDAddress[2] = devices[dev].BDaddress[2];
                message_to_ble.BDAddress[3] = devices[dev].BDaddress[3];
                message_to_ble.BDAddress[4] = devices[dev].BDaddress[4];
                message_to_ble.BDAddress[5] = devices[dev].BDaddress[5];
                message_to_ble.message_len  = 16;
                message_to_ble.command      = 0x24;
                message_to_ble.id_risposta  = 0;
                message_to_ble.message[0]   = 1;

                devices[dev].c.type.nuovo_evento = 0;
                
                sendCode = true;
            }
            if( ++dev == MAX_DEVICE ) dev = 0;
        }
        if (sendCode == true)
        {
            Cy_BLE_GAPC_StopScan();
            {
                timeout = 10000;
                fase_ble = connect;
                tentativi_invio = 0;
            }
        }

        if (cy_ble_scanState == CY_BLE_SCAN_STATE_STOPPED)
        {
            fase_ble = start_ble;
            timeout = 5000;
        }
        break;

    case connect:
        {            
            cy_stc_ble_conn_handle_t _handle;
            cy_en_ble_conn_state_t   _state;
            
            if (timeout == 0)
            {
                fase_ble_error = connect;
                fase_ble = end_error;
                break;
            }
            if( cy_ble_scanState != CY_BLE_SCAN_STATE_STOPPED ) break;
            
            //if (cy_ble_scanState != CY_BLE_SCAN_STATE_STOPPED) break;

            par.remote_addr.type = 0;

            remote_addr.bdAddr[0] = message_to_ble.BDAddress[0];
            remote_addr.bdAddr[1] = message_to_ble.BDAddress[1];
            remote_addr.bdAddr[2] = message_to_ble.BDAddress[2];
            remote_addr.bdAddr[3] = message_to_ble.BDAddress[3];
            remote_addr.bdAddr[4] = message_to_ble.BDAddress[4];
            remote_addr.bdAddr[5] = message_to_ble.BDAddress[5];
            remote_addr.type = 0;

            print_log( "BLE CONNECT..... \r\n" );
            apiResult = Cy_BLE_GAPC_ConnectDevice(&remote_addr, CY_BLE_CENTRAL_CONFIGURATION_0_INDEX);

            if (apiResult == CY_BLE_SUCCESS)
            {
                //writeCCCD = false;
                //infoExchangeState = INFO_EXCHANGE_START;
                timeout = 6000;
                fase_ble = connecting;
            }
            else if( apiResult == CY_BLE_ERROR_INVALID_STATE )
            {
                print_log("BLE RESTART !\r\n");
                Cy_BLE_Stop();
                fase_ble = wait_stop;
                //Cy_BLE_Start(StackEventHandler);
                
                //Cy_BLE_SetConnectionState( conn_handle , CY_BLE_CONN_STATE_DISCONNECTED );
            }
            else
            {
                print_log( "BLE CONNECTED ERROR..... already connected ? \r\n");
                //apiResult = Cy_BLE_GAPC_CancelDeviceConnection();
                //Cy_BLE_GAPC_InitConnection
                fase_ble_error = connect;
                fase_ble = end_error;
            }
        }
        break;

    case connecting:
        
        if( tasto_premuto ) 
        {
            apiResult = Cy_BLE_GAPC_CancelDeviceConnection();
            fase_ble  = end;
            break;
        }
        
        if (timeout == 0)
        {
            apiResult = Cy_BLE_GAPC_CancelDeviceConnection();
            if (++tentativi_connessione == 5)
            {
                fase_ble_error = connecting;
                fase_ble = end_error;
            }
            else
            {
                // Tempo scaduto, provo a riconnettermi una seconda volta
                apiResult = Cy_BLE_GAPC_ConnectDevice(&remote_addr, CY_BLE_CENTRAL_CONFIGURATION_0_INDEX);
                if (apiResult == CY_BLE_SUCCESS)
                {
                    writeCCCD = false;
                    infoExchangeState = INFO_EXCHANGE_START;
                    timeout = 6000;
                }
                else
                {
                    apiResult = Cy_BLE_GAPC_CancelDeviceConnection();
                    fase_ble_error = connecting;
                    fase_ble = end_error;
                }
            }
            break;
        }
        
        if (cy_ble_conn_state != CY_BLE_CONN_STATE_CONNECTED) break;

        
//        CyDelay(250);
//        
//        if( rxCharHandle != 0 ) 
//        {
//            EnableNotification();
//        }
//        else
//        {
//            if ((INFO_EXCHANGE_COMPLETE != infoExchangeState))
//            {
//                attrHandleInit();
//                break;
//            }            
//        }
//        
                
        timeout = 10000;
        fase_ble = wait_cccd;

        break;
        
    case wait_cccd:

        // Aspetto che venga abilitata la notifica sulla caratteristica RX
        if (cy_ble_conn_state != CY_BLE_CONN_STATE_CONNECTED)
        {
            print_log( "BLE DISCONNESSO SU CCCD \r\n" );
            fase_ble_error = wait_cccd;
            fase_ble = end_error;
            break;            
        }
        
        if( timeout == 0 ) 
        {            
            if( ++tentativi_cccd == 3 ) 
            {
                fase_ble_error = wait_cccd;
                fase_ble = end_error;
                break;
            }
            if( txCharHandle == 0 ) 
            {
                infoExchangeState = INFO_EXCHANGE_START;
                attrHandleInit();
            }
            timeout = 4000;
            break;
        }
        
        if (writeCCCD == false) break;
        
        print_log( "NOTIFICHE ABILITATE\r\n" );
        
        if (sendCode == true)
        {
            pacchetto = 0;
            pacchetti = message_to_ble.message_len / 16;
        }
        ++logUfg.trasmissioni_ble;

        fase_ble = send;
        
        break;

    case send:
    {
        CyDelay(100);
        if (Cy_BLE_GATT_GetBusyStatus(conn_handle.attId) != CY_BLE_STACK_STATE_BUSY)
        {
            writeOk  = false;
            writeErr = false;
            if (sendCode == true)
            {
                ini = pacchetto * 16;
                ++pacchetto;

                buftx[0] = message_to_ble.command;
                buftx[1] = pacchetto;
                buftx[2] = pacchetti;
                buftx[3] = message_to_ble.id_risposta;

                memcpy(&buftx[4], &message_to_ble.message[ini], 16);

                conn_handle.attId     = 0;
                conn_handle.bdHandle  = 0;
                send_param.connHandle = conn_handle;
                send_param.handleValPair.attrHandle = rxCharHandle;
                //send_param.handleValPair.attrHandle = 0x31;
                send_param.handleValPair.value.len  = 20;
                send_param.handleValPair.value.val  = buftx;

                print_log( "BLE WRITE...... \r\n" );
                
                cyble_api_result = Cy_BLE_GATTC_WriteCharacteristicValue(&send_param);

                if (cyble_api_result == CY_BLE_SUCCESS)
                {
                    if (pacchetto == pacchetti)
                    {
                        timeout = 8000;
                        fase_ble = wait_code;
                    }
                    else{
                        timeout = 5000;   
                        fase_ble = wait;
                    }
                }
                else
                {
                    // If 'param' is NULL or 'connHandle' is invalid, or if 'attrHandle' is zero or if 'param->handleValPair.value.len' value is greater than (Effective GATT MTU-3).
                    // mi disconnetto e ci riprovo                    
                    if( ++tentativi_invio < 8 )
                    {
                        disconnect_info.reason   = 0x13;
                        disconnect_info.bdHandle = conn_handle.bdHandle;
                        cyble_api_result = Cy_BLE_GAP_Disconnect(&disconnect_info);
                        fase_ble = connect;
                    }
                    else 
                    {
                        print_log( "BLE WRITE ERROR \r\n" );
                        fase_ble = end_error;
                        fase_ble_error = send;
                    }
                    break;
                }
            }
        }
        else
        {
            print_log( "BLE GATT BUSY \r\n" );
        }
        break;
    }
    
    case wait:
        if (timeout == 0 || isConnected == false )
        {
            print_log( "BLE WAIT WRITE ERROR \r\n" );
            fase_ble = end_error;
            fase_ble_error = wait;
            break;
        }

        // Attendo la risposta al messaggio inviato
        if (writeOk || writeErr)
        {
            CyDelay(100);
            timeout  = 0;
            writeOk  = false;
            writeErr = false;
            // continuo ad inviare i pacchetti che seguono
            fase_ble = send;
        }

        break;

    case wait_code:
        if (isConnected == false) 
        {
            print_log( "BLE WAIT_END WRITE ERROR ( DISCONNECT )\r\n" );
            fase_ble = end_error;
            fase_ble_error = wait_code;
        }

        // Attendo la risposta al messaggio inviato
        if (writeOk == false && writeErr == false) break;

        // Prima di disconnettermi attendi 10 secondi nel caso devo consegnare altri messaggi
        if (pSendToBle != pSendBle) timeout = 0;
        if (timeout) break;

        // Devo inviare altri messaggi ?
        message_to_ble.send    = 0;
        message_to_ble.timeout = 0;
        if (message_to_ble.command != 0x24)
        {
            if (++pSendBle == 20) pSendBle = 0;

            if (pSendToBle != pSendBle)
            {
                memcpy(&message_to_ble, &messages_toble[pSendBle], sizeof(message_s));

                // Devo madare altri messaggi a queets serratura ?                        
                if (remote_addr.bdAddr[0] == message_to_ble.BDAddress[0] &&
                    remote_addr.bdAddr[1] == message_to_ble.BDAddress[1] &&
                    remote_addr.bdAddr[2] == message_to_ble.BDAddress[2] &&
                    remote_addr.bdAddr[3] == message_to_ble.BDAddress[3] &&
                    remote_addr.bdAddr[4] == message_to_ble.BDAddress[4] &&
                    remote_addr.bdAddr[5] == message_to_ble.BDAddress[5])
                {
                    pacchetto = 0;
                    pacchetti = message_to_ble.message_len / 16;
                    fase_ble = send;

                    CyDelay(100);
                    break;
                }
            }
        }

        timeout  = 5000; // Attendo prima di disconnettermi nel caso mi debbano arrivare messaggi dalla serratura
        fase_ble = disconnect;
        break;

    case disconnect:
        // Il timeout si ripristina a 5 secondi ogni volta che ricevo un messaggio dal bluetooth
        if (timeout) break;

        print_log( "BLE DISCONNECT...\r\n");
        disconnect_info.reason = 0x13;
        disconnect_info.bdHandle = conn_handle.bdHandle;
        cyble_api_result = Cy_BLE_GAP_Disconnect(&disconnect_info);
        if (cyble_api_result == CY_BLE_SUCCESS)
        {
            timeout = 5000;
            fase_ble = whait_disconnect;
        }
        else
            fase_ble = end;

        break;

    case whait_disconnect:

        if( timeout == 0 )
        {
            com_debug_PutString( "DISCONNECT ERROR \r\n" );
            fase_ble = end_error;
            fase_ble_error = whait_disconnect; 
            break;
        }
        
        if( isConnected == true ) break;
        
//        if (cy_ble_conn_state != CY_BLE_CONN_STATE_DISCONNECTED && 
//            cy_ble_conn_state != CY_BLE_CONN_STATE_CLIENT_DISCONNECTED_DISCOVERED ) break;

        fase_ble = end;
        break;

    case wait_stop:
        if( cy_ble_state != CY_BLE_STATE_STOPPED ) break;
        
        fase_ble = init;
    break;
        
    case end_error:

        print_log( "BLE ERROR \r\n" );
        if( ++tentativi_falliti == 4 ) 
        {
            while(1)
            {
                __NVIC_SystemReset();
            }
        }
        
        if ( Cy_BLE_IsPeerConnected( remote_addr.bdAddr ) )
        {
            fase_ble = disconnect;
            break;
        }

        fase_ble = start_ble;
        break;

    case end:

        if (isConnected) fase_ble = disconnect;
        else
        {
            fase_ble  = start_ble;
            getCrypto = false;
            sendCode  = false;
            timeout   = 0;
            tentativi_connessione = 0;
            tentativi_invio       = 0;
            tentativi_falliti     = 0;
            tentativi_cccd        = 0;
        }
        break;
    }
}

void procRxNotification(cy_stc_ble_gattc_handle_value_ntf_param_t* param)
{
    uint8_t command     = param->handleValPair.value.val[0];
    uint8_t pacchetto   = param->handleValPair.value.val[1];
    uint8_t pacchetti   = param->handleValPair.value.val[2];
    uint8_t id_risposta = param->handleValPair.value.val[3];
    
    uint8_t ini = (pacchetto - 1) * 16;

    uint8_t end;

    memcpy(&bufrx[ini], &param->handleValPair.value.val[4], 16);
    if (pacchetto == pacchetti)
    {
#if ENABLE_LOG    
    
    sprintf( (char*)buflog, "RICEVUTO DAL BLE : CMD %d", command);
    com_debug_PutString(buflog);
    com_debug_PutString( "\r\n");
#endif
        // Invio il tutto al Cloud
        end = ini + 16;
        ini = 0;

        if (command == 0x61)
        {
            // Invio la configurazione wireless
            com_lentx = sprintf(com_buftx, "#05;");
        }
        else
        {
            com_lentx = sprintf(com_buftx, "#02;%02X%02X%02X%02X%02X%02X;%02X;",
                remote_addr.bdAddr[0],
                remote_addr.bdAddr[1],
                remote_addr.bdAddr[2],
                remote_addr.bdAddr[3],
                remote_addr.bdAddr[4],
                remote_addr.bdAddr[5],
                command);

//            com_lentx = sprintf(com_buftx, "#02;%02X%02X%02X%02X%02X%02X;%02X;%02X;",
//                remote_addr.bdAddr[0],
//                remote_addr.bdAddr[1],
//                remote_addr.bdAddr[2],
//                remote_addr.bdAddr[3],
//                remote_addr.bdAddr[4],
//                remote_addr.bdAddr[5],
//                command,
//                id_risposta);
        }

        while (ini != end)
        {
            com_lentx += sprintf(&com_buftx[com_lentx], "%02X", bufrx[ini]);
            ++ini;
        }
        com_buftx[com_lentx] = 0x0d;
        ++com_lentx;

        memset(&messaggi[pMessaggi][0], 0, MAX_MESSAGE_SIZE);

        memcpy(&messaggi[pMessaggi][0], com_buftx, com_lentx);

        if (++pMessaggi == MAX_MESSAGGI) pMessaggi = 0;

        //com_PutArray(com_buftx,com_lentx);

        timeout = 5000;
        timeout_send_adv = 1000;

    }
}



void InitiateConnection(void)
{
    apiResult = Cy_BLE_GAPC_ConnectDevice(&par.remote_addr, CY_BLE_CENTRAL_CONFIGURATION_0_INDEX);
    if (apiResult != CY_BLE_SUCCESS)
    {
        //BleAssert();
    }
}

#define CY_BLE_SERVER_UART_TX_DATA_CLIENT_CHARACTERISTIC_CONFIGURATION_DESC_HANDLE   (0x002Fu) /* Handle of Client Characteristic Configuration descriptor */
void EnableNotification(void)
{
    cy_stc_ble_gattc_write_req_t req;
    cy_stc_ble_gatts_handle_value_ntf_t param;
    
    print_log( "BLE ABILITO NOTIFICHE \r\n" );
    
    desc_val[0] = 1;
    desc_val[1] = 0;
    req.connHandle               = conn_handle;
    req.handleValPair.attrHandle = charDiscDescrInfo.descrHandle; //CY_BLE_SERVER_UART_TX_DATA_CLIENT_CHARACTERISTIC_CONFIGURATION_DESC_HANDLE;
    req.handleValPair.value.val  = desc_val;
    req.handleValPair.value.len  = CY_BLE_CCCD_LEN;

    apiResult = Cy_BLE_GATTC_WriteCharacteristicDescriptors(&req);
}

void BleAssert(void)
{
    CY_ASSERT(0u != 0u);
}

uint8_t btest[50];

void StackEventHandler(uint32 event, void* eventParam)
{
    //cy_en_ble_api_result_t      apiResult;
    cy_stc_ble_gatts_write_cmd_req_param_t* wrReqParam; // = (cy_stc_ble_gatts_write_cmd_req_param_t *)eventParam;


    uint8_t scanState = Cy_BLE_GetScanState();
    switch (event)
    {
        /* This event is received when the BLE component is Started */
    case CY_BLE_EVT_STACK_ON:
    {
        print_log( "STACK BLE ON\r\n" );
        /* Stack initialized; ready for scan */
        break;
    }
    /* This event indicates get device address command completed successfully */
    case CY_BLE_EVT_GET_DEVICE_ADDR_COMPLETE:
    {
        break;
    }
    /* This event indicates set device address command completed. */
    case CY_BLE_EVT_SET_DEVICE_ADDR_COMPLETE:
    {
        break;
    }
    /* This event is received when there is a timeout. */
    case CY_BLE_EVT_TIMEOUT:
    {
        break;
    }
    /* This event indicates that some internal HW error has occurred. */
    case CY_BLE_EVT_HARDWARE_ERROR:
    {
        break;
    }
    /*  This event will be triggered by host stack if BLE stack is busy or has become free */
    case CY_BLE_EVT_STACK_BUSY_STATUS:
    {
        break;
    }
    /* This event indicates completion of Set LE event mask. */
    case CY_BLE_EVT_LE_SET_EVENT_MASK_COMPLETE:
    {
        break;
    }
    /* This event indicates set Tx Power command completed. */
    case CY_BLE_EVT_SET_TX_PWR_COMPLETE:
    {
        break;
    }

    /**********************************************************
    *                     GAP Central Events
    ***********************************************************/
    /* This event is triggered every time a device is discovered */
    case CY_BLE_EVT_GAPC_SCAN_PROGRESS_RESULT:
    {
        /* A new device listed in the scan report */
        adv_report = (cy_stc_ble_gapc_adv_report_param_t*)eventParam;

        /* Process only for Advertisement packets, not on scan response packets */
        if (adv_report->eventType != CY_BLE_GAPC_SCAN_RSP)
        {
            /* Set address 'found' status temporarily */

            if (memcmp(bleUartServiceUuid, &adv_report->data[5], 16) != 0)
            {
                break;
            }
            //if (adv_report->data[21] != 0x06) break;
            //if (adv_report->data[22] != 0x16) break;
            //if (adv_report->data[23] != 0x0a) break;
            //if (adv_report->data[24] != 0x18) break;

            targetAddrFound = true;

            refresh_timeout_device(adv_report);
        }
        break;
    }

    /* This event is triggered when the central device has started/stopped scanning */
    case CY_BLE_EVT_GAPC_SCAN_START_STOP:
    {
        cy_en_ble_api_result_t* result = (cy_en_ble_api_result_t*)eventParam;
        
        switch (scanState)
        {
        case CY_BLE_SCAN_STATE_STOPPED:
            print_log( "BLE SCAN STOP\r\n" );
            break;

        case CY_BLE_SCAN_STATE_SCAN_INITIATED:
            print_log( "BLE SCAN INITIATED\r\n" );
            break;

        case CY_BLE_SCAN_STATE_SCANNING:
            print_log( "BLE SCAN START\r\n" );
            break;

        case CY_BLE_SCAN_STATE_STOP_INITIATED:
            print_log( "BLE SCAN STOP INITIATED\r\n" );
            break;
        }
        break;
    }


    // This event is triggered when there is a change to either the maximum Payload 
    // length or the maximum transmission time of Data Channel PDUs in either direction
    case CY_BLE_EVT_DATA_LENGTH_CHANGE:
    {
        cy_stc_ble_set_phy_info_t phyParam;

        phyParam.bdHandle = conn_handle.bdHandle;
        phyParam.allPhyMask = CY_BLE_PHY_NO_PREF_MASK_NONE;
        phyParam.phyOption = 0;
        phyParam.rxPhyMask = CY_BLE_PHY_MASK_LE_1M;
        phyParam.txPhyMask = CY_BLE_PHY_MASK_LE_1M;

        Cy_BLE_EnablePhyUpdateFeature();
        apiResult = Cy_BLE_SetPhy(&phyParam);
        break;
    }
    /* This event indicates completion of the Cy_BLE_SetPhy API*/
    case CY_BLE_EVT_SET_PHY_COMPLETE:
    {
        cy_stc_ble_events_param_generic_t* param = (cy_stc_ble_events_param_generic_t*)eventParam;
        if (param->status == SUCCESS)
        {
            Cy_BLE_GetPhy(conn_handle.bdHandle);
        }
        else
        {
            // DEBUG_BLE("SET PHY Could not update to 2 Mbps\r\n");
            Cy_BLE_GetPhy(conn_handle.bdHandle);
        }
        break;
    }
    /* This event indicates completion of the Cy_BLE_GetPhy API*/
    case CY_BLE_EVT_GET_PHY_COMPLETE:
    {
        break;
    }
    /* This event indicates that the controller has changed the transmitter PHY or receiver PHY in use */
    case CY_BLE_EVT_PHY_UPDATE_COMPLETE:
    {
        break;
    }
    /**********************************************************
    *                       GATT Events
    ***********************************************************/
    /* This event is generated at the GAP Peripheral end after a connection is completed
      with a peer Central device. For a GAP Central device, this event is generated as
      in acknowledgment of receiving this event successfully by the BLE Controller.*/
    case CY_BLE_EVT_GATT_CONNECT_IND:
    {
        /* Connected as Central (master role) */
        conn_handle = *(cy_stc_ble_conn_handle_t*)eventParam;        
        isConnected = true;

        print_log( "BLE GATT CONNECTED\r\n" );
        
        
        writeCCCD = false; 
        infoExchangeState = INFO_EXCHANGE_START;
        attrHandleInit();

        //            DEBUG_BLE("CY_BLE_EVT_GATT_CONNECT_IND: attId=%X, bdHandle=%X \r\n", 
        //                        conn_handle.attId, 
        //                        conn_handle.bdHandle);
        //            printf("Connected to Device\r\n\n");

           /* Initiate an MTU exchange request */

           //cy_stc_ble_gatt_xchg_mtu_param_t mtuParam = {conn_handle, 128};
           //apiResult = Cy_BLE_GATTC_ExchangeMtuReq(&mtuParam);

           //if(apiResult != CY_BLE_SUCCESS)
           //{
              // DEBUG_BLE("Cy_BLE_GATTC_ExchangeMtuReq API Error: %xd \r\n", apiResult);
           //}

        break;
    }
    /* This event indicates GATT MTU Exchange response from the server device. */
    case CY_BLE_EVT_GATTC_XCHNG_MTU_RSP:
    {
        /*set the 'mtuSize' variable based on the minimum MTU supported by both devices */
        if (CY_BLE_GATT_MTU > ((cy_stc_ble_gatt_xchg_mtu_param_t*)eventParam)->mtu)
        {
            mtuSize = ((cy_stc_ble_gatt_xchg_mtu_param_t*)eventParam)->mtu;
        }
        else
        {
            mtuSize = CY_BLE_GATT_MTU;
        }

        print_log( "BLE MTU XCHNG_COMPLETE \r\n" );
        
        infoExchangeState |= MTU_XCHNG_COMPLETE;
        break;
    }
    /* This event is generated at the GAP Peripheral end after  disconnection. */
    case CY_BLE_EVT_GATT_DISCONNECT_IND:
    {
        if (conn_handle.bdHandle == (*(cy_stc_ble_conn_handle_t*)eventParam).bdHandle)
        {
//            conn_handle.bdHandle = CY_BLE_INVALID_CONN_HANDLE_VALUE;
//            conn_handle.attId    = CY_BLE_INVALID_CONN_HANDLE_VALUE;
//            isConnected = false;
            print_log( "BLE GATT DISCONNECTED\r\n" );
        }
        
        break;
    }
    case CY_BLE_EVT_GAP_DEVICE_DISCONNECTED:
    {
        cy_stc_ble_gap_disconnect_param_t* param = (cy_stc_ble_gap_disconnect_param_t*)eventParam;
        
        if( conn_handle.bdHandle == param->bdHandle )
        {
            conn_handle.bdHandle = CY_BLE_INVALID_CONN_HANDLE_VALUE;
            conn_handle.attId    = CY_BLE_INVALID_CONN_HANDLE_VALUE;
            print_log( "BLE DEVICE DISCONNECTED\r\n" );
            isConnected = false;
        }
    }
        break;
        /* This event is triggered when 'GATT MTU Exchange Request'  received from GATT client device. */
    case CY_BLE_EVT_GATTS_XCNHG_MTU_REQ:
    {
        break;
    }
    /* This event indicates a Write response from the server device. */
    case CY_BLE_EVT_GATTC_WRITE_RSP:
    {
        /* Write response for the CCCD write; this means that the
        * notifications are now enabled. */
        //            DEBUG_BLE("CY_BLE_EVT_GATTC_WRITE_RSP, bdHandle = %X\r\n",\ (*(cy_stc_ble_conn_handle_t *) eventParam).bdHandle);
        //            DEBUG_BLE("Notifications are enabled\r\n");
        print_log( "BLE GATTC WRITE OK\r\n" );
        writeCCCD = true;
        writeOk   = true;
        break;
    }
    /* This event is triggered when notification data is received from the  server device. */
    case CY_BLE_EVT_GATTC_HANDLE_VALUE_NTF:
    {
        ntf_param = (cy_stc_ble_gattc_handle_value_ntf_param_t*)eventParam;
        if( ntf_param->handleValPair.attrHandle == txCharHandle )
        {
            procRxNotification(ntf_param);
        }
        break;
    }
    break;
    case CY_BLE_EVT_GATTS_WRITE_REQ:
        // Write ok
        wrReqParam = (cy_stc_ble_gatts_write_cmd_req_param_t*)eventParam;

        // wrReqParam->connHandle <-- il device che mi sta parlando

        // Prelevo l'handle della caratteristica cy_ble_gatt_db_attr_handle_t
        if (wrReqParam->handleValPair.attrHandle == 1)
        {

        }
        print_log( "BLE GATTS WRITE OK\r\n" );
        writeOk = true;
        break;

    case CY_BLE_EVT_GATTS_READ_CHAR_VAL_ACCESS_REQ:
    {
        cy_stc_ble_gatts_char_val_read_req_t* mParam = (cy_stc_ble_gatts_char_val_read_req_t*)eventParam;

        cy_en_ble_gatt_err_code_t err = mParam->gattErrorCode;
    }
    break;

    case CY_BLE_EVT_GATTC_EXEC_WRITE_RSP:
    {
        //cy_stc_ble_gattc_exec_write_rsp_param_t *rsp = (cy_stc_ble_gattc_exec_write_rsp_param_t*)eventParam;
    }
    break;

    /* The event is received by the Client when the Server cannot perform  the requested operation and sends out an error response. */

    case CY_BLE_EVT_GATTC_READ_BY_GROUP_TYPE_RSP:
        {
            cy_stc_ble_gattc_read_by_grp_rsp_param_t* service_handle;
            
            service_handle = (cy_stc_ble_gattc_read_by_grp_rsp_param_t *)eventParam;
            cy_stc_ble_disc_srv_info_t ServiceInfo = { .connHandle = service_handle->connHandle };

            uint16_t attr_length;
            uint16_t length;
            uint32_t i;
            
            length=service_handle->attrData.length;
            if((length == CY_BLE_DISC_SRVC_INFO_LEN) || (length == CY_BLE_DISC_SRVC_INFO_128_LEN))
            {
                /*Store the Total Length of Attribute Data */  
                attr_length = service_handle->attrData.attrLen; 
      
                for(i = 0u; i < service_handle->attrData.attrLen; i += service_handle->attrData.length)   
                {        
                    ServiceInfo.srvcInfo = (cy_stc_ble_disc_srvc128_info_t *)(service_handle->attrData.attrValue + i);   
                    if(length == CY_BLE_DISC_SRVC_INFO_LEN)  
                    {
//                        ServiceInfo.uuidFormat=CY_BLE_GATT_16_BIT_UUID_FORMAT; 
//                        /*Print the service number */
//                        sprintf( buftx, "\r\nService No: %02d",(serviceNo));
//                        UART_PutString(buftx);
//                        start_handle_array[serviceNo-1] = ServiceInfo.srvcInfo->range.startHandle;
//                        end_handle_array[serviceNo-1] = ServiceInfo.srvcInfo->range.endHandle;
//                        /*Print the Start Handle */
//                        sprintf(buftx, "\r\nStart Handle: %04x",ServiceInfo.srvcInfo->range.startHandle);
//                        UART_PutString(buftx);                        
//                        /*Print the End Handle */
//                        sprintf(buftx, "\r\nEnd Handle: %04x",ServiceInfo.srvcInfo->range.endHandle);
//                        UART_PutString(buftx);
//                        /*Print the UUID Format */
//                        printf(buftx, "\r\nUUID Format: %02x",ServiceInfo.uuidFormat);
//                        UART_PutString(buftx);
//                        /*Print the UUID value */
//                        sprintf(buftx, "\r\nUUID value: %04x ",ServiceInfo.srvcInfo->uuid.uuid16); 
//                        UART_PutString(buftx);
//                        UART_PutString("\r\n");
                    } 
                    else   
                    { 
                        ServiceInfo.uuidFormat=CY_BLE_GATT_128_BIT_UUID_FORMAT;
                        
                        if( memcmp( ServiceInfo.srvcInfo->uuid.uuid128.value, bleUartServiceUuid, 16 ) == 0 )
                        {
                            // Trovato servizio UART
                            range_service_handle.startHandle = ServiceInfo.srvcInfo->range.startHandle;
                            range_service_handle.endHandle   = ServiceInfo.srvcInfo->range.endHandle;
                            //UartServiceInfo = ServiceInfo; //.connHandle = conn_handle;
                            infoExchangeState |= BLE_UART_SERVICE_HANDLE_FOUND;
                            attrHandleInit();
                            
                            print_log( "BLE UART_SERVICE_HANDLE_FOUND\r\n" );
                        }
//                        
//                        /*Print the service number */
//                        sprintf( buftx, "\r\nService No: %02d",(serviceNo));
//                        UART_PutString(buftx);
//                        start_handle_array[serviceNo-1] = ServiceInfo.srvcInfo->range.startHandle;
//                        end_handle_array[serviceNo-1] = ServiceInfo.srvcInfo->range.endHandle;
//                        /*Print the Start Handle */
//                        sprintf(buftx, "\r\nStart Handle: %04x",ServiceInfo.srvcInfo->range.startHandle);
//                        UART_PutString(buftx);
//                        /*Print the End Handle */
//                        sprintf(buftx,"\r\nEnd Handle: %04x",ServiceInfo.srvcInfo->range.endHandle);
//                        UART_PutString(buftx);
//                        /*Print the UUID Format */
//                        sprintf(buftx,"\r\nUUID Format: %02x",ServiceInfo.uuidFormat);
//                        UART_PutString(buftx);
//                        /*Print the UUID value */;
//                        UART_PutString("\r\nUUID value: ");
//                        for(i=0;i<16;i++)
//                        {
//                        sprintf(buftx,"%x",ServiceInfo.srvcInfo->uuid.uuid128.value[15-i]);
//                        UART_PutString(buftx);
//                        if(i==3 || i==5 || i==7 ||i==9)
//                        UART_PutString("-");
//                        }
//                        UART_PutString("\r\n");
                    }  
                }
            }            
        break;
        }
        
    case CY_BLE_EVT_GATTC_FIND_BY_TYPE_VALUE_RSP:
        // Trovato il servizio   UART
        find_param = (cy_stc_ble_gattc_find_by_type_rsp_param_t*)eventParam;

        range_service_handle.startHandle = find_param->range->startHandle;
        range_service_handle.endHandle = find_param->range->endHandle;

        print_log( "BLE UART_SERVICE_HANDLE_FOUND\r\n" );
        
        infoExchangeState |= BLE_UART_SERVICE_HANDLE_FOUND;
        attrHandleInit();
        break;

    case CY_BLE_EVT_GATTC_READ_BY_TYPE_RSP:
        {
            int t;
            // Risposta alla richiesta di ricezione delle caratteristiche         
            read_param = (cy_stc_ble_gattc_read_by_type_rsp_param_t*)eventParam;

            characteristic_handle=(cy_stc_ble_gattc_read_by_type_rsp_param_t *)eventParam;
                
            uint16_t chardataLength;
            uint16_t charattrLength;
           
            uint8_t *charattrVal;
            cy_stc_ble_disc_char_info_t discCharInfo;
             
            chardataLength=characteristic_handle->attrData.length;
            charattrLength=characteristic_handle->attrData.attrLen;
            charattrVal=characteristic_handle->attrData.attrValue;
                
            for(t = 0u; t < charattrLength; t += chardataLength)  
            {
                discCharInfo.charDeclHandle = Cy_BLE_Get16ByPtr(charattrVal);
                /*Print the Char declaration handle */
                //desc_array[Char_No-1]=discCharInfo.charDeclHandle;
                charattrVal += sizeof(discCharInfo.charDeclHandle);
                discCharInfo.properties = *charattrVal;
                /*Print the properties in a characteristic */
                //Print_Properties (discCharInfo.properties);
                charattrVal += sizeof(discCharInfo.properties);
                discCharInfo.valueHandle = Cy_BLE_Get16ByPtr(charattrVal);
                charattrVal += sizeof(discCharInfo.valueHandle);
                /* Characteristics discovery with 128-bit uuid */
                    
                if(chardataLength == CY_BLE_DISC_CHAR_INFO_128_LEN)   
                {
                    if( memcmp( charattrVal, uartRxAttrUuid, 16 ) == 0 )
                    {
                        discCharInfo.uuidFormat = CY_BLE_GATT_128_BIT_UUID_FORMAT;
                        (void)memcpy(&discCharInfo.uuid.uuid128, charattrVal, CY_BLE_GATT_128_BIT_UUID_SIZE);
                        charattrVal += CY_BLE_GATT_128_BIT_UUID_SIZE;  
                        
                        rxCharHandle = discCharInfo.valueHandle;
                        
                        infoExchangeState |= RX_ATTR_HANDLE_FOUND;
                        attrHandleInit();
                        print_log( "BLE RX_ATTR_HANDLE_FOUND \r\n" );
                    }
                    if( memcmp( charattrVal, uartTxAttrUuid, 16 ) == 0 )
                    {
                        discCharInfo.uuidFormat = CY_BLE_GATT_128_BIT_UUID_FORMAT;
                        (void)memcpy(&discCharInfo.uuid.uuid128, charattrVal, CY_BLE_GATT_128_BIT_UUID_SIZE);
                        charattrVal += CY_BLE_GATT_128_BIT_UUID_SIZE;  
                        
                        txCharHandle = discCharInfo.valueHandle;
                        
                        range_char_uartrx_handle.startHandle = discCharInfo.charDeclHandle;
                        range_char_uartrx_handle.endHandle   = discCharInfo.charDeclHandle + 50;
                        
                        infoExchangeState |= TX_ATTR_HANDLE_FOUND;
                        attrHandleInit();
                        print_log( "BLE TX_ATTR_HANDLE_FOUND \r\n" );
                    }
                }  
            }
        
//        if (0 == memcmp((uint8*)&(read_param->attrData.attrValue[5]), (uint8*)uartTxAttrUuid, 16))
//        {
//            txCharHandle = read_param->attrData.attrValue[3];
//            txCharHandle |= (read_param->attrData.attrValue[4] << 8);
//
//            infoExchangeState |= TX_ATTR_HANDLE_FOUND;
//            attrHandleInit();
//            print_log( "BLE TX_ATTR_HANDLE_FOUND \r\n" );
//        }
//        else if (0 == memcmp((uint8*)&(read_param->attrData.attrValue[5]), (uint8*)uartRxAttrUuid, 16))
//        {
//            rxCharHandle = read_param->attrData.attrValue[3];
//            rxCharHandle |= (read_param->attrData.attrValue[4] << 8);
//
//            memcpy(btest, read_param->attrData.attrValue, read_param->attrData.attrLen);
//
//            infoExchangeState |= RX_ATTR_HANDLE_FOUND;
//            attrHandleInit();
//            print_log( "BLE RX_ATTR_HANDLE_FOUND \r\n" );
//        }
//        else
//        {
//            print_log( "BLE SCONOSCIUTO_ATTR_HANDLE_FOUND \r\n" );
//        }
        break;
        }
             
        case CY_BLE_EVT_GATTC_FIND_INFO_RSP:
        {
            descriptor_handle=( cy_stc_ble_gattc_find_info_rsp_param_t *)eventParam;
       
            uint32_t locDataLength;
            uint32_t attrLength;
            uint32_t i;
            uint8_t *attrVal;
            
            /* Discovery descriptor information */
            cy_stc_ble_disc_descr_info_t locDiscDescrInfo;

            locDiscDescrInfo.descrHandle = CY_BLE_GATT_INVALID_ATTR_HANDLE_VALUE;
            {
                attrLength = descriptor_handle->handleValueList.byteCount;    /* Number of elements on list in bytes */
                locDiscDescrInfo.uuidFormat = descriptor_handle->uuidFormat;
                locDiscDescrInfo.connHandle = descriptor_handle->connHandle;
            
                if(locDiscDescrInfo.uuidFormat == CY_BLE_GATT_16_BIT_UUID_FORMAT)
                {
                    locDataLength = CY_BLE_DB_ATTR_HANDLE_LEN + CY_BLE_GATT_16_BIT_UUID_SIZE;
                }
                else
                {
                    locDataLength = CY_BLE_DB_ATTR_HANDLE_LEN + CY_BLE_GATT_128_BIT_UUID_SIZE;
                }
                attrVal = descriptor_handle->handleValueList.list;  
                for(i = 0u; i < attrLength; i += locDataLength)
                {
                    //desc_No++;
                    locDiscDescrInfo.descrHandle = Cy_BLE_Get16ByPtr(attrVal);

                    attrVal += CY_BLE_DB_ATTR_HANDLE_LEN;
                    if(locDiscDescrInfo.uuidFormat == CY_BLE_GATT_128_BIT_UUID_FORMAT)
                    {   
                        (void)memcpy(&locDiscDescrInfo.uuid.uuid128, attrVal, CY_BLE_GATT_128_BIT_UUID_SIZE);
                        attrVal += CY_BLE_GATT_128_BIT_UUID_SIZE;
                    }
                    else
                    {   
                        locDiscDescrInfo.uuid.uuid16 = Cy_BLE_Get16ByPtr(attrVal);
                        attrVal += CY_BLE_GATT_16_BIT_UUID_SIZE;
                        /*Print the Descriptor UUID value */
                        
                        if( locDiscDescrInfo.uuid.uuid16 == 0x2902 )
                        {
                            // Trovato descriptor per CCCD ( abilitazione notifiche )   
                            
                            charDiscDescrInfo.descrHandle = locDiscDescrInfo.descrHandle;
                            charDiscDescrInfo.uuidFormat  = 1;
                            charDiscDescrInfo.uuid.uuid16 = 0x2902;
                            
                            EnableNotification();
                        }                        
                    }
                }
            }
        }        
        break;
    
        case CY_BLE_EVT_GATTC_LONG_PROCEDURE_END:        
        {
            cy_stc_ble_gattc_long_procedure_end_param_t *param = (cy_stc_ble_gattc_long_procedure_end_param_t*)eventParam;
            //param->opcode
            print_log( "BLE GATTC_LONG_PROCEDURE_END\r\n" );            
            attrHandleInit();
        }
        
        break;
        case CY_BLE_EVT_GATTC_ERROR_RSP:
        {
            // Write error
            err_param = (cy_stc_ble_gatt_err_param_t*)eventParam;
            
            print_log( "BLE GATTC WRITE ERROR\r\n" );
            //scanErr  = true;
            //writeErr = true;
            
            //attrHandleInit();
            break;
        }
        case CY_BLE_EVT_STACK_SHUTDOWN_COMPLETE:
            ble_shutdown = true;
        break;
        /**********************************************************
        *                       Other Events
        ***********************************************************/
    default:
        break;
    }
}


void attrHandleInit()
{
    cy_stc_ble_gatt_xchg_mtu_param_t          mtuParam = { conn_handle, 23 };
    cy_stc_ble_gatt_value_t                   val;
    
    cy_stc_ble_gattc_read_by_type_req_t   param2;

    switch (infoExchangeState)
    {
    case INFO_EXCHANGE_START:

        ReadByGroupReq.connHandle.bdHandle = conn_handle.bdHandle;
        ReadByGroupReq.connHandle.attId     = conn_handle.attId;
        ReadByGroupReq.range.startHandle    = CY_BLE_GATT_ATTR_HANDLE_START_RANGE;
        ReadByGroupReq.range.endHandle      = CY_BLE_GATT_ATTR_HANDLE_END_RANGE;
        /*Start discovering of all the primary services*/
        apiResult = Cy_BLE_GATTC_DiscoverPrimaryServices(&ReadByGroupReq);

        break;
        
        val.val = bleUartServiceUuid;
        val.len = CY_BLE_GATT_128_BIT_UUID_SIZE;
        val.actualLen = CY_BLE_GATT_128_BIT_UUID_SIZE;
                    
        param.connHandle = conn_handle;
        param.value = val;
        
        Cy_BLE_GATTC_DiscoverPrimaryServiceByUuid(&param);
        break;

    case BLE_UART_SERVICE_HANDLE_FOUND:

        gattc_c.connHandle.bdHandle = conn_handle.bdHandle;
        gattc_c.connHandle.attId    = conn_handle.attId;
        gattc_c.range.startHandle   = range_service_handle.startHandle;
        gattc_c.range.endHandle     = range_service_handle.endHandle;
        gattc_c.uuidFormat          = CY_BLE_GATT_128_BIT_UUID_FORMAT;
        
        if( Cy_BLE_GATTC_DiscoverCharacteristics(&gattc_c) != CY_BLE_SUCCESS )
        {
            print_log( "ERRORE INIT DISCOVER");
        }
        
//        break;
//        param2.connHandle = conn_handle;
//        param2.range.startHandle = range_service_handle.startHandle;
//        param2.range.endHandle   = range_service_handle.endHandle;
//        param2.uuidFormat        = CY_BLE_GATT_128_BIT_UUID_FORMAT;
//        param2.uuid.uuid128.value[0] = uartTxAttrUuid[0];
//        param2.uuid.uuid128.value[1] = uartTxAttrUuid[1];
//        param2.uuid.uuid128.value[2] = uartTxAttrUuid[2];
//        param2.uuid.uuid128.value[3] = uartTxAttrUuid[3];
//        param2.uuid.uuid128.value[4] = uartTxAttrUuid[4];
//        param2.uuid.uuid128.value[5] = uartTxAttrUuid[5];
//        param2.uuid.uuid128.value[6] = uartTxAttrUuid[6];
//        param2.uuid.uuid128.value[7] = uartTxAttrUuid[7];
//        param2.uuid.uuid128.value[8] = uartTxAttrUuid[8];
//        param2.uuid.uuid128.value[9] = uartTxAttrUuid[9];
//        param2.uuid.uuid128.value[10] = uartTxAttrUuid[10];
//        param2.uuid.uuid128.value[11] = uartTxAttrUuid[11];
//        param2.uuid.uuid128.value[12] = uartTxAttrUuid[12];
//        param2.uuid.uuid128.value[13] = uartTxAttrUuid[13];
//        param2.uuid.uuid128.value[14] = uartTxAttrUuid[14];
//        param2.uuid.uuid128.value[15] = uartTxAttrUuid[15];
//        Cy_BLE_GATTC_DiscoverCharacteristicByUuid(&param2);
        
        break;
//          BLE_UART_SERVICE_HANDLE_FOUND   = 0x01,
//  TX_ATTR_HANDLE_FOUND            = 0x02,
//  RX_ATTR_HANDLE_FOUND
//        case SERVICE_AND_TXCHAR_HANDLES_FOUND:
//            param2.connHandle = conn_handle;
//            param2.range.startHandle = range_service_handle.startHandle;
//            param2.range.endHandle   = range_service_handle.endHandle;
//            param2.uuidFormat        = CY_BLE_GATT_128_BIT_UUID_FORMAT;
//            param2.uuid.uuid128.value[0] = uartRxAttrUuid[0];
//            param2.uuid.uuid128.value[1] = uartRxAttrUuid[1];
//            param2.uuid.uuid128.value[2] = uartRxAttrUuid[2];
//            param2.uuid.uuid128.value[3] = uartRxAttrUuid[3];
//            param2.uuid.uuid128.value[4] = uartRxAttrUuid[4];
//            param2.uuid.uuid128.value[5] = uartRxAttrUuid[5];
//            param2.uuid.uuid128.value[6] = uartRxAttrUuid[6];
//            param2.uuid.uuid128.value[7] = uartRxAttrUuid[7];
//            param2.uuid.uuid128.value[8] = uartRxAttrUuid[8];
//            param2.uuid.uuid128.value[9] = uartRxAttrUuid[9];
//            param2.uuid.uuid128.value[10] = uartRxAttrUuid[10];
//            param2.uuid.uuid128.value[11] = uartRxAttrUuid[11];
//            param2.uuid.uuid128.value[12] = uartRxAttrUuid[12];
//            param2.uuid.uuid128.value[13] = uartRxAttrUuid[13];
//            param2.uuid.uuid128.value[14] = uartRxAttrUuid[14];
//            param2.uuid.uuid128.value[15] = uartRxAttrUuid[15];
//            Cy_BLE_GATTC_DiscoverCharacteristicByUuid(&param2);
//        break;

    case (SERVICE_AND_CHAR_HANDLES_FOUND):
    
        FindInfoReq.connHandle.bdHandle = conn_handle.bdHandle;
        FindInfoReq.connHandle.attId    = conn_handle.attId;
        FindInfoReq.range.startHandle   = range_char_uartrx_handle.startHandle; // desc_array[char_number-1]+2;
        FindInfoReq.range.endHandle     = range_char_uartrx_handle.endHandle; //desc_array[char_number]-1;
        /*Start discovering of all the descriptors in a characteristics */
        apiResult = Cy_BLE_GATTC_DiscoverCharacteristicDescriptors(&FindInfoReq);

    
        //         charDescHandleRange.startHandle = txCharHandle + 1;
        //         charDescHandleRange.endHandle   = bleUartServiceEndHandle;
        //
        //         CyBle_GattcDiscoverAllCharacteristicDescriptors(cyBle_connHandle, &charDescHandleRange);

        // apiResult = Cy_BLE_GATTC_ExchangeMtuReq(&mtuParam);
        break;

    case (ALL_HANDLES_FOUND):
        //            CyBle_GattcExchangeMtuReq(cyBle_connHandle, CYBLE_GATT_MTU);
        apiResult = Cy_BLE_GATTC_ExchangeMtuReq(&mtuParam);
        break;

    default:
        break;
    }

    Cy_BLE_ProcessEvents();
}

int togle   = 0;
int secondi = 0;
int milli   = 0;
int max_secondi = 540; // 9 minuti
void tick_1ms(void)
{
    int r;
    if (timeout) --timeout;
    if (timeout_com) --timeout_com;
    if (timeout_send_adv) --timeout_send_adv;
    if (tasto_premuto) --tasto_premuto;
    if( timeout_rx_pic ) --timeout_rx_pic;
    
    for (r = 0; r < MAX_DEVICE; r++)
    {
        if (devices[r].timeout) --devices[r].timeout;        
    }

    for (r = 0; r < 20; r++)
    {
        // Un messaggio da consegnare alla serratura deve scadere se non consegnato in x Tempo
        if (messages_toble[r].timeout) --messages_toble[r].timeout;
    }

    ++milli;
    
    if( (milli % 1000 ) == 0 ) ++secondi; 
    
    Cy_GPIO_Write(led1_PORT,   led1_NUM,    togle );
    
//    if( secondi < max_secondi ) // la scheda resetta ogni 9 minuti
//        Cy_GPIO_Write(wd_ext_PORT, wd_ext_NUM , togle );
    
    togle ^= 1;
}

void ISR_UART(void)
{
    uint8_t read_data;
    timeout_rx_pic = 1000;
    
       
    /* Check for "RX fifo not empty interrupt" */
    if((com_HW->INTR_RX_MASKED & SCB_INTR_RX_MASKED_NOT_EMPTY_Msk ) != 0)
	{
        /* Clear UART "RX fifo not empty interrupt" */
		com_HW->INTR_RX = com_HW->INTR_RX & SCB_INTR_RX_NOT_EMPTY_Msk;        
            
        /* Get the character from terminal */
		read_data = Cy_SCB_UART_Get(com_HW);
        
        if (read_data == '#')
        {
            com_bufrx[0] = read_data;
            com_lenrx = 1;
        }
        else if (read_data == 0x0d)
        {
            com_bufrx[com_lenrx] = 0;
            fl_rx_pic = 1;
            
        }
        else if (com_lenrx < MAX_BUFF)
        {
            com_bufrx[com_lenrx] = read_data;
            ++com_lenrx;
        }
        
        
        /* Update data_received flag */
        //data_received = 1;        
	}   
    else
    {
        /* Error if any other interrupt occurs */
        // uart_error = 1;
    }
}

//void rxcom_isr()
//{
//    if( read_com() ) 
//    {
//        fl_rx_pic = 1;    
//    }
//}

void print_log(char* str )
{
#if ENABLE_LOG
        com_debug_PutString( str );
#endif    
}

/* [] END OF FILE */
