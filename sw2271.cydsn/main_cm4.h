/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"

#define RELEASE_FW 120
#define RELEASE_HW 100
#define RELEASE_PR 100

typedef struct
{
    unsigned nuova             :1;
    unsigned abbina_tastiera   :1;
    unsigned abbina_telefono   :1;
    unsigned disabilitata      :1;
    unsigned recovery          :1;
    unsigned abbina_bridge     :1;
    unsigned abbina_impronta   :1;
    unsigned nuovo_evento      :1;
}eAdv;

typedef union
{   
   eAdv    type;
   uint8_t val;
}uAdv;

typedef struct 
{
   unsigned mbr3         :1;
   unsigned fram         :1;
   unsigned magnetometro :1;
   unsigned ble_hw       :1;
   unsigned wco          :1;
   unsigned opt          :3;
}sErrFlag;

typedef union 
{
    uint8_t    val;
    sErrFlag   bit;    
}uErr;
extern uErr  error;

typedef enum
{
    inizio,
    sleep,
    leggo_batteria,
    run,
    attesa_ble,
    invia,
    battery_low,
    errore,
}eFaseApp;

typedef struct
{
    uint8_t  BDaddress[6];
    uint8_t  a;
    uint8_t  b;
    uAdv     c;
    uint16_t timeout;
}device_s;

typedef struct
{
    uint8_t  BDAddress[6];
    uint8_t  message[160];
    uint8_t  message_len;
    uint8_t  command;
    uint8_t  id_risposta;
    uint8_t  send;
    uint16_t timeout;
}message_s;

typedef struct
{
   uint8_t release;
   uint8_t accoppiato;
   cy_stc_ble_gap_bd_addr_t remote_addr;
   uint8_t aes[16];
   uint8_t id[8];
   uint8_t buzzer_en;
}sParametri;

typedef struct
{
   uint8_t  release;
   uint32_t accensioni;
   uint32_t false_accensioni;
   uint32_t trasmissioni_ble;
    uint32_t errori;
}sLogUfg;

typedef enum
{
    detecth_finger     = 0x01,
    get_image          = 0x02,
    gen_templet        = 0x03,
    move_templet       = 0x20,
    match_two_templet  = 0x04,
    search             = 0x05,
    merge_two_templet  = 0x06,
    store_templet      = 0x07,
    load_templet       = 0x08,
    up_templet         = 0x09,
    down_templet       = 0x0a,
    delete_one_templet = 0x0d,
    erase_all_templett = 0x0e,
    read_par_table     = 0x0f,
    set_secure_level   = 0x12,
    set_pwd            = 0x13,
    verify_pwd         = 0x14,
    sys_reset          = 0x15,
    flash_led          = 0x16,
    check_templet      = 0x28,
    check_dw_status    = 0x31,
    set_bps            = 0x33,
    up_image           = 0x38
    
}command_e;

typedef enum
{
    buffer_a     = 0x01,
    buffer_b     = 0x02,
    buffer_model = 0x03,
    buffer_c     = 0x04
}buffer_e;

typedef enum
{
    return_ok                                   = 0x00,
    return_packet_error                         = 0x01,
    return_no_finger                            = 0x02,
    return_failed_enter_finger                  = 0x03,
    return_image_is_too_dry                     = 0x04,
    return_image_is_too_wet                     = 0x05,
    return_image_is_too_messy                   = 0x06,
    return_image_ok_points_err                  = 0x07,
    retun_no_valid_image_in_buffer              = 0x15,
    return_fingerprint_mismatch                 = 0x08,
    return_no_fingerprint_found                 = 0x09,
    return_feature_merge_failed                 = 0x0a,
    return_address_serial_err                   = 0x0b,
    return_err_reading_template                 = 0x0c,
    return_upload_feature_failed                = 0x0d,
    return_module_no_accept_packets             = 0x0e,
    return_failed_upload_image                  = 0x0f,
    return_failed_delete_template               = 0x10,
    return_failed_to_empty_fingerprint_library  = 0x11,
    return_unable_to_enter_sleep                = 0x12,
    return_instruction_err                      = 0x13,
    return_reset_failed                         = 0x14,
    return_current_image_no_valid               = 0x15,
    return_operation_flash_err                  = 0x18,
    return_no_valid_template                    = 0x19,
    return_download_instruction_failed          = 0x24,
    return_setting_wave_rate_failed             = 0x33
}confirmation_code_e;

typedef enum
{
    init = 0,
    start_ble,
    wait_start_scan,
    scan,
    connect,
    connecting,
    wait_cccd,
    send,
    wait,
    wait_code,
    disconnect,
    whait_disconnect,
    wait_stop,
    end_error,
    end
}eBle;

typedef enum 
{
  INFO_EXCHANGE_START             = 0x00,
  BLE_UART_SERVICE_HANDLE_FOUND   = 0x01,
  TX_ATTR_HANDLE_FOUND            = 0x02,
  RX_ATTR_HANDLE_FOUND            = 0x04,
  TX_CCCD_HANDLE_FOUND            = 0x08,
  MTU_XCHNG_COMPLETE              = 0x10,
  
    
  SERVICE_AND_TXCHAR_HANDLES_FOUND = (BLE_UART_SERVICE_HANDLE_FOUND | TX_ATTR_HANDLE_FOUND),
  SERVICE_AND_CHAR_HANDLES_FOUND   = (BLE_UART_SERVICE_HANDLE_FOUND | TX_ATTR_HANDLE_FOUND | RX_ATTR_HANDLE_FOUND),
  ALL_HANDLES_FOUND                = (BLE_UART_SERVICE_HANDLE_FOUND | TX_ATTR_HANDLE_FOUND | RX_ATTR_HANDLE_FOUND | TX_CCCD_HANDLE_FOUND),
  INFO_EXCHANGE_COMPLETE           = (BLE_UART_SERVICE_HANDLE_FOUND | TX_ATTR_HANDLE_FOUND | RX_ATTR_HANDLE_FOUND | MTU_XCHNG_COMPLETE)
} INFO_EXCHANGE_STATE_T;

/* [] END OF FILE */
